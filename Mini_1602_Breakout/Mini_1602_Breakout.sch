EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "2020-08-28"
Rev "1.0"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 "Breakout board for mini 1602 display"
$EndDescr
$Comp
L Connector:Conn_01x16_Male J1
U 1 1 5F48A99E
P 3150 3700
F 0 "J1" H 3250 4550 50  0000 C CNN
F 1 "Conn_01x16_Male" V 3050 3700 50  0000 C CNN
F 2 "Mini_1602_Breakout:Mini_1602_Breakout" H 3150 3700 50  0001 C CNN
F 3 "~" H 3150 3700 50  0001 C CNN
	1    3150 3700
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x16_Male J2
U 1 1 5F48F17D
P 4750 3700
F 0 "J2" H 4950 4550 50  0000 R CNN
F 1 "Conn_01x16_Male" V 4650 4000 50  0000 R CNN
F 2 "Mini_1602_Breakout:TE_FPC_16p" H 4750 3700 50  0001 C CNN
F 3 "~" H 4750 3700 50  0001 C CNN
	1    4750 3700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3350 3000 4550 3000
Wire Wire Line
	4550 3100 3350 3100
Wire Wire Line
	3350 3200 4550 3200
Wire Wire Line
	4550 3300 3350 3300
Wire Wire Line
	3350 3400 4550 3400
Wire Wire Line
	4550 3500 3350 3500
Wire Wire Line
	3350 3600 4550 3600
Wire Wire Line
	4550 3700 3350 3700
Wire Wire Line
	3350 3800 4550 3800
Wire Wire Line
	4550 3900 3350 3900
Wire Wire Line
	3350 4000 4550 4000
Wire Wire Line
	3350 4100 4550 4100
Wire Wire Line
	4550 4200 3350 4200
Wire Wire Line
	3350 4300 4550 4300
Wire Wire Line
	4550 4400 3350 4400
Wire Wire Line
	3350 4500 4550 4500
$EndSCHEMATC
