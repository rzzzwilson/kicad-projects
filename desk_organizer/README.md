# Desk Organizer

I need a desk organizer to hold eight or so external hard drives in a more
convenient way and to reclaim some desk space.  The KiCad data and \*.DXF
files here are used to laser cut 2.5mm MDF sheet.
