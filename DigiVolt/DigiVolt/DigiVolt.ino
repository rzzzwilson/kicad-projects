//////////////////////////////////////////////////////////////////
// DigiVoltErrorMsg
//
// A two channel device to measure voltages.
//
// The "LO" input will measure up to about 2.5 volts.  The "HI"
// input will measure up to about 30 volts.
//
// NOTE:
// Must modify /Applications/Arduino.app/Contents/Java/hardware/arduino/avr/boards.txt
// to include this line "leonardo.build.usb_product="DigiVolt"" before uploading.
//////////////////////////////////////////////////////////////////

#include <SPI.h>
#include <EEPROM.h>

// WARNING - uncommenting this will initialize EEPROM values!
//#define INIT_EEPROM   1

// the SPI clock speed
const long SPIClock = 10000000;

// Voltage measurement program name & version
const char *ProgramName = "DigiVolt";
const char *Version = "1.1";

// set pin 17 as the slave select for the digital pot
// other SPI pins are as assumed on the Iota
const int SSPin = 17;     // B0 on the Iota
const int SCLKPin = 15;   // B1
const int MOSIPin = 16;   // B2

// channel activity pins (LED)
const int Act0 = 3;     // channel 0 activity LED - D0
const int Act1 = 2;     // channel 1 activity LED - D1

// value of internal reference
const float ARef = 2.56;

// command bytes for the digital pot
byte writePot0 = B00010001; // write to pot 0
byte writePot1 = B00010010; // write to pot 1

// number of channels for this device
const int NumChannels = 2;

// pin numbers for each voltage channel
int ChannelPin[NumChannels] = {23, 22};
//                             F0  F1 pins

// Scale for adjustment of "Aref" voltage
#define DefaultARefValue  2.56
float ARefScale = DefaultARefValue;

// Scale values for each channel
// Default value assumes R7/R8 "high" divider is 47K/10K (ditto R9/R10)d;
#define DefaultScale ((47.0+10.0)/10.0)
float Scales[NumChannels] = {DefaultScale, DefaultScale};

// Range values for each channel
// 0 means low scale (2.56v), 1 means high scale (~30v)
#define DefaultRange 'H'
char Ranges[NumChannels] = {DefaultRange, DefaultRange};

// values for max and min offset
const int MinOffset = 0;
const int MaxOffset = 255;

// buffer, etc, to gather external command strings
#define MAX_COMMAND_LEN   16
#define COMMAND_END_CHAR    ';'
char CommandBuffer[MAX_COMMAND_LEN + 1];
int CommandIndex = 0;

const char *ErrorMsg = "ERROR";
const char *OKMsg = "OK";

// Define the address in EEPROM of various things.
#define NEXT_SLOT(a)    ((a) + sizeof(a))

// two scale save slots in EEPROM
const int EepromARefScale = 0;
const int EepromScales0 = NEXT_SLOT(EepromARefScale);
const int EepromScales1 = NEXT_SLOT(EepromScales0);

/////////////////////////////////////////////////////////////
// Save/restore scale values in EEPROM.
/////////////////////////////////////////////////////////////

void get_scales(void)
{
  int scale_value;

  EEPROM.get(EepromARefScale, scale_value);
  ARefScale = ((float) scale_value) / 1000;
  EEPROM.get(EepromScales0, scale_value);
  Scales[0] = ((float) scale_value) / 1000;
  EEPROM.get(EepromScales1, scale_value);
  Scales[1] = ((float) scale_value) / 1000;
}

void put_scales(void)
{
  EEPROM.put(EepromARefScale, (int) (ARefScale * 1000));
  EEPROM.put(EepromScales0, (int) (Scales[0] * 1000));
  EEPROM.put(EepromScales1, (int) (Scales[1] * 1000));
}

/////////////////////////////////////////////////////////////
// Convert a string to an integer.
/////////////////////////////////////////////////////////////

int string_to_int(char *str)
{
  int result = 0;

  while (*str)
  {
    int digit = *str - '0';

    if ((digit < 0) || (digit > 9))
      break;
      
    result *= 10;
    result += *str - '0';
    ++str;
  }

  return result;
}

/////////////////////////////////////////////////////////////
// Find a character in a string.
// Returns pointer to first found char, else returns NULL.
/////////////////////////////////////////////////////////////

char * find_in_string(char *str, char ch)
{
  while (*str)
  {
    if (*str == ch)
      return str;
    ++str;
  }

  return NULL;
}

/////////////////////////////////////////////////////////////
// Convert a string to uppercase in situ.
/////////////////////////////////////////////////////////////

void str2upper(char *str)
{
  while (*str)
  {
    *str = toupper(*str);
    ++str;
  }
}

void setActivity(int channel, bool state)
{
  int pin = Act0;   // assume channel 0

  if (channel != 0)
    pin = Act1;

  if (state)
    digitalWrite(pin, HIGH);
  else
    digitalWrite(pin, LOW);
}

/////////////////////////////////////////////////////////////
// Set 'channel' pot to the given 'offset'.
/////////////////////////////////////////////////////////////

void digitalPotWrite(int channel, int offset)
{
  // handle multiple channels
  byte cmd = writePot0;

  if (channel != 0)
    cmd = writePot1;  // if it's not channel 0, assume channel 1

  setActivity(channel, true);

  // send value to the appropriate channel
  SPI.beginTransaction(SPISettings(10000000, MSBFIRST, SPI_MODE0));
  digitalWrite(SSPin, LOW);
  SPI.transfer(cmd);
  SPI.transfer(offset);
  digitalWrite(SSPin, HIGH);
  SPI.endTransaction();

  // possibly needed SETTLING TIME?
  delay(50);
  
  setActivity(channel, false);
}


/////////////////////////////////////////////////////////////
// Read the voltage on 'channel' pot.
/////////////////////////////////////////////////////////////

float voltageRead(int channel)
{
  // the working variables
  int pin = ChannelPin[channel];    // pin number for the channel
  int analog_value = 0;             // analog value read from channel
  
  // binary chop stuff
  int bottom = 0;                   // bottom of the wiper range
  int top = MaxOffset;              // top of the wiper range
  int wiper = MaxOffset / 2;        // wiper value we read "analog_value" at

  // binary chop the pot until we get max reading near full scale
  while (true)
  {
    digitalPotWrite(channel, wiper);
    analog_value = analogRead(pin);
    
#ifdef JUNK
    Serial.print("Chop: analog_value=");
    Serial.print(analog_value);
    Serial.print(",  bottom=");
    Serial.print(bottom);
    Serial.print(", wiper=");
    Serial.print(wiper);
    Serial.print(", top=");
    Serial.println(top);
#endif
    
    if (wiper == bottom + 1 || wiper == top - 1)
      break;
      
    if (analog_value >= 1023)
    {
      top = wiper;
      wiper = (wiper + bottom + 1) / 2;
    }
    else
    {
      bottom = wiper;
      wiper = (wiper + top + 1) / 2;
    }
  }
  
#ifdef DEBUG
  Serial.print("After: analog_value=");
  Serial.print(analog_value);
  Serial.print(", wiper=");
  Serial.println(wiper);
#endif

  // set wiper to 0 offset again (protect the pot chip)
  digitalPotWrite(channel, 0);

  return (((ARefScale * analog_value) / 1023) * 255) / wiper;
}


//##############################################################################
// External command routines.
//
// External commands are:
//     H;        send help text to console
//     ID;       get device identifier string
//     NC;       get number of channels
//     V?;       get voltage on channel '?'
//     R?x;      set scale on channel '?' - 'x' is L or H
//     R?;       get scale on channel '?' - returns L or H
//     CLxxxxx;  set common LOW calibration to xxxx
//     CL;       get common LOW calibration value
//     CH?xxxxx; set channel '?' HIGH calibration to xxxx
//     CH?;      get channel '?' HIGH calibration value
//     D;        debug command - does something
//##############################################################################

//----------------------------------------
// Get help:
//     H;
//----------------------------------------

void xcmd_help(char *cmd)
{
  // to turn off warning about "unused parameter"
  cmd[0] = '\0';

  Serial.println("-----------Interactive Commands-----------------");
  Serial.println("H;       send help text to console");
  Serial.println("ID;      get device identifier string");
  Serial.println("NC;      get number of channels");
  Serial.println("V?;      get voltage for channel '?'");
  Serial.println("R?;      get range on channel '?' - 'L' or 'H'");
  Serial.println("R?x;     set range on channel '?' to 'L' or 'H'");
  Serial.println("CH?;     get HIGH calibration on channel '?'");
  Serial.println("CH?xxxx; set HIGH calibration on channel '?' to 'xxxx'");
  Serial.println("CL;      get LOW calibration");
  Serial.println("CLxxxx;  set LOW calibration to 'xxxx'");
  Serial.println("D;       print some internal debug information");
  Serial.println("------------------------------------------------");
}

//----------------------------------------
// Get the identifier string:
//     ID
//----------------------------------------

void xcmd_id(char *cmd)
{
  // if not legal, complain
  if (strcmp(cmd, "ID"))
  {
    Serial.println(ErrorMsg);
    return;
  }

  // generate ID string and return
  Serial.print(ProgramName);
  Serial.print(" ");
  Serial.println(Version);
}

//----------------------------------------
// Get the number of channels:
//     NC
//----------------------------------------

void xcmd_numchan(char *cmd)
{
  // if not legal, complain
  if (strcmp(cmd, "NC"))
  {
    Serial.println(ErrorMsg);
    return;
  }

  // generate answer string and return
  Serial.println(NumChannels);
}

//----------------------------------------
// Get voltage for a channel:
//     V?
//----------------------------------------

void xcmd_getvolts(char *cmd)
{
  float voltage = 0;

  // if not legal, complain
  if (strlen(cmd) != 2)
  {
    Serial.println(ErrorMsg);
    return;
  }

  // get channel number, check legal
  int channel = cmd[1] - '0';
  if ((channel < 0) or (channel >= NumChannels))
  {
    Serial.println(ErrorMsg);
    return;
  }

  // get and return voltage
  voltage = voltageRead(channel);
  if (Ranges[channel] == 'H')
  {
    // high range, scale the voltage
    voltage *= Scales[channel];
  }
  
  Serial.println(voltage);
}

//----------------------------------------
// Set/get range for a channel:
//     R?x  set range for channel '?' to 'H' or 'L'
//     R?   get range for channel '?' - 'H' or 'L'
//----------------------------------------

void xcmd_range(char *cmd)
{
  // if not legal, complain
  if ((strlen(cmd) != 3) && (strlen(cmd) != 2))
  {
    Serial.println(ErrorMsg);
    return;
  }

  // get channel number, check legal
  int channel = cmd[1] - '0';
  if ((channel < 0) or (channel >= NumChannels))
  {
    Serial.println(ErrorMsg);
    return;
  }

  // if len() is 3 it's a "set range" command
  if (strlen(cmd) == 3)
  {
    // get range value, check legal
    if ((cmd[2] != 'L') && (cmd[2] != 'H'))
    {
      Serial.println(ErrorMsg);
      return;
    }

    // set range for the channel
    if (cmd[2] == 'L')
      Ranges[channel] = 'L';
    else
      Ranges[channel] = 'H';

    Serial.println(OKMsg);
    return;
  }
  else
  {
    // return range for the given channel range
    Serial.println(Ranges[channel]);
  }
}

//----------------------------------------
// Set/Get range for a channel:
//     CLx.xxx      set LOW calibration
//     CL           get LOW calibration
//     CH?x.xxx     set LOW calibration for channel ?
//     CH?          get HIGH calibration for channel ?
//----------------------------------------

void xcmd_calib(char *cmd)
{
  char discrim;

  // get LOW/HIGH discriminator
  if (strlen(cmd) < 2)
  {
    Serial.println(ErrorMsg);
    return;
  }

  discrim = cmd[1];
  if (discrim != 'H' and discrim != 'L')
  {
    Serial.println(ErrorMsg);
    return;
  }

  // do HIGH or LOW command
  if (discrim == 'L')
  {
    // LOW command, a GET if "CL"
    if (strlen(cmd) == 2)
    {
      Serial.println(ARefScale);
      return;
    }
    else
    {
      // Must be a SET command, check length
      if (strlen(cmd) < 4)
      {
        Serial.println(ErrorMsg);
        return;
      }

      // OK, get value and store it as a float
      ARefScale = atof(&cmd[2]);
      put_scales();
      Serial.println(OKMsg);
    }
  }
  else  // "discrim" must be 'H'
  {
    // get/set HIGH calibration...
    if (strlen(cmd) < 3)
    {
      Serial.println(ErrorMsg);
      return;
    }
      
    // next char MUST be channel number
    int channel = cmd[2] - '0';
    if ((channel < 0) or (channel >= NumChannels))
    {
      Serial.println(ErrorMsg);
      return;
    }

    // if 3 chars, it's a GET, else a SET
    if (strlen(cmd) == 3)
    {
      // GET command
      Serial.println(Scales[channel]);
      return;
    }
    else
    {
      // SET command
      Scales[channel] = atof(&cmd[3]);
      put_scales();
      Serial.println(OKMsg);
      return;
    }
  }
}

//----------------------------------------
// Perform debug:
//     D
//
// Put any debug code you want here.
// Could expand to "Dx" where the "x" char selects debug required.
//----------------------------------------

void xcmd_debug(char *cmd)
{
  // if not legal, complain
  if (strcmp(cmd, "D"))
  {
    Serial.println(ErrorMsg);
    return;
  }

  Serial.println("DigiVolt state:");

  // generate ID string and return
  Serial.print("Scales[0]=");
  Serial.print(Scales[0], 3);
  Serial.print(", Scales[1]=");
  Serial.println(Scales[1], 3);

  Serial.print("Ranges[0]=");
  Serial.print(Ranges[0]);
  Serial.print(", Ranges[1]=");
  Serial.println(Ranges[1]);

  Serial.print("ARefScale=");
  Serial.println(ARefScale);
}

//----------------------------------------
// Process an external command.
//     cmd     address of command string buffer
//     index   index of last char in string buffer
// 'cmd' is '\0' terminated.
//----------------------------------------
void do_external_cmd(char *cmd)
{
  // ensure everything is uppercase
  str2upper(cmd);

  // process the command
  switch (cmd[0])
  {
    case 'C':
      xcmd_calib(cmd);
      return;
    case 'D':
      xcmd_debug(cmd);
      return;
    case 'H':
      xcmd_help(cmd);
      return;
    case 'I':
      xcmd_id(cmd);
      return;
    case 'N':
      xcmd_numchan(cmd);
      return;
    case 'R':
      xcmd_range(cmd);
      return;
    case 'V':
      xcmd_getvolts(cmd);
      return;
  }

  Serial.println(ErrorMsg);
}

void do_external_commands(void)
{
  bool overflow = false;    // "true" if command buffer overflowed
  
  // gather any commands from the external controller
  while (Serial.available())
  {
    char ch = Serial.read();

    // ignore newlines
    if (ch == '\n')
      continue;

    if (CommandIndex < MAX_COMMAND_LEN)
    {
      // only add if command not over-length
      CommandBuffer[CommandIndex++] = ch;
    }
    else
      overflow = true;

    if (ch == COMMAND_END_CHAR)   // if end of command, execute it
    {
      if (overflow)
      {
        overflow = false;
        Serial.println("TOO LONG");
      }
      else
      {
        CommandBuffer[CommandIndex-1] = '\0'; // remove final ';'
//        unsigned long start = micros();
        do_external_cmd(CommandBuffer);
// print how long command took
//        unsigned long delta = micros() - start;
//        Serial.println(delta);
        CommandIndex = 0;
      }
    }
  }
}

void setup(void)
{
  // set up the serial comms
  Serial.begin(115200);

  // use 2.56 volt reference
  analogReference(INTERNAL);

  // set pin modes, etc
  pinMode(Act0, OUTPUT);
  pinMode(Act1, OUTPUT);
  pinMode(SSPin, OUTPUT);
  pinMode(SCLKPin, OUTPUT);
  pinMode(MOSIPin, OUTPUT);

  // start SPI
  digitalWrite(SSPin, HIGH);
  SPI.begin();

  // exercise the channel activity LEDs
  setActivity(0, true);
  setActivity(1, true);
  delay(300);
  setActivity(0, false);
  setActivity(1, false);

  // wait for the Serial port to become alive
  // needed for Leonardo only
  while (!Serial)
    ;

  // if necessary, initialize EEPROM values
#if INIT_EEPROM
  Serial.println("*********************************");
  Serial.println("* Erasing EVERYTHING in EEPROM! *");
  Serial.println("*********************************");
  put_scales();  // WARNING! Destroys the EEPROM stored values
#endif

  // get scale values from EEPROM
  get_scales();

  Serial.print(ProgramName);
  Serial.print(" ");
  Serial.print(Version);
  Serial.println(" ready.");
  Serial.println("Enter 'H;' and <enter> to get help.");
}

void loop()
{
  do_external_commands();    // do any external commands
}
