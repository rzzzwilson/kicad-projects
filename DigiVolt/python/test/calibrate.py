#!/usr/bin/env python3

"""
A program to lead someone through the process of calibrating a DigiVolt device.

Usage: calibrate.py [-h]

You will need to have on hand:

    * a DigiVolt device (of course!)
    * a low-voltage reference source <= 2.5 volts
    * a high-voltage reference source >= 10 volts

Automatically handles 1 or 2 channel devices (or 3, 4, ...).
"""

import os
import sys
from digithing import DigiThing, DigiThingError


# program version info
Version = 'calibrate 1.1'

LogFile = 'calibrate.log'

# amount we ignore in difference between measured volts and reference volts
VoltsEpsilon = 0.005


def log(msg):
    """Write 'msg' to the log file."""

    with open(LogFile, 'a') as f:
        f.write(msg+'\n')

def wait_attached(prompt):
    """Wait until a DigiVolt is connected."""

    # wait for a DigiVolt device to be attached
    count = 10000
    while True:
        error = None
        try:
            return DigiThing('DigiVolt')
        except DigiThingError as e:
            error = str(e)

        count += 1
        if count > 10000:
            if error:
                print('\r' + prompt + error + '       ', end='')
            count = 0

def get_calib(device, num_chans):
    """Get LOW and HIGH calibration figures from the connected device."""

    low_calib = float(device.do_cmd('cl;'))
    high_calib = []
    for chan in range(num_chans):
        calib = float(device.do_cmd(f'ch{chan};'))
        high_calib.append(calib)

    return (low_calib, high_calib)

def prepare_low(device):
    """Prepare the LOW voltage reference source.

    ser  the serial connection to the DigiVolt

    Returns the voltage for the connected LOW source.
    """

    print()
    print('Connect the low voltage source to the DigiVolt LOW channel 0 input.')
    input('Press ENTER when done: ')

    while True:
        voltage = input('What exact voltage is the LOW source? ')
        try:
            print()
            return float(voltage)
        except ValueError:
            print("Sorry, that's not a valid floating point number.")

def prepare_high(device):
    """Prepare the HIGH voltage reference source.

    ser  the serial connection to the DigiVolt

    Returns the voltage for the connected HIGH source.
    """

    while True:
        voltage = input('What exact voltage is the HIGH source? ')
        try:
            print()
            return float(voltage)
        except ValueError:
            print("Sorry, that's not a valid floating point number.")

def set_low_all(device, num_chans):
    """Ensure all channels are LOW input."""

    for chan in range(num_chans):
        device.do_cmd(f'r{chan}l;\n')

def check_voltage_source(device):
    """Make sure we see SOME voltage on channel 0."""

    count = 0
    while True:
        count += 1
        if count > 500:
            print('Not seeing any voltage on channel 0, make sure voltage source is ON!')
            count = 0
            continue

        volts = float(device.do_cmd('v0;'))
        if volts > 0.2:
            return

def low_calibrate(device, low_volts, low_calib):
    """Calibrate the LOW voltages on the device.

    device     the DigiVolt device to use
    low_volts  the actual voltage being applied
    low_calib  the LOW calibration figure being used

    Returns the new LOW calibaration figure.
    """

    last_calib = -1

    # set the LOW calibration to the default 2.56 volts
    old_calib = 2.56
    device.do_cmd(f'cl{old_calib:.3f};')

    while True:
        # get measured voltage, check against reference value
        volts = float(device.do_cmd('v0;'))

        if abs(low_volts - volts) < VoltsEpsilon:
            return low_calib

        # adjust the calibration before trying again
        delta = (low_volts - volts) / 4
        low_calib += delta

        if last_calib == low_calib:
            return low_calib

        last_calib = low_calib
        device.do_cmd(f'cl{low_calib:.3f};')

def high_calibrate(device, chan, high_volts):
    """Calibrate the HIGH voltages on the device.

    device      the DigiVolt device to use
    chan        the channel to calibrate
    high_volts  the HIGH voltage value

    Returns the new HIGH calibration for the channel.
    """

    # prepare for HIGH calibration of "chan" channel
    print(f'Connect the HIGH voltage source to the DigiVolt HIGH channel {chan} input.')
    input('Press ENTER when done: ')
    print()

    # turn channel to HIGH range
    device.do_cmd(f'r{chan}H;')

    # set the HIGH calibration to the schematic default (divider: 47K + 10K)
    old_calib = (47.0+10.0)/10.0
    device.do_cmd(f'ch{chan}{old_calib:.3f};')

    # now calibrate the "chan" channel
    last_calib = old_calib
    while True:
        # get measured voltage, check against reference value
        volts = float(device.do_cmd(f'v{chan};'))

        if abs(high_volts - volts) < VoltsEpsilon:
            print()
            return last_calib

        # adjust the calibration before trying again
        delta = (high_volts - volts) / 4
        new_calib = last_calib + delta
        log(f'high_calibrate: volts={volts}, high_volts={high_volts}, new_calib={new_calib}')

        if last_calib == new_calib:
            return new_calib

        last_calib = new_calib
        device.do_cmd(f'ch{chan}{new_calib:.3f};')

def usage(msg=None):
    """Print usage text and optional message to the screen."""

    if msg:
        print('*' * 60)
        print(msg)
        print('*' * 60)
    print(__doc__)

def main():
    # announce the program version
    print(Version)
    print()

    # wait_attached the program and prompt for DigiVolt devices to be inserted
    prompt = 'Searching for a DigiVolt ... '
    print(prompt, end='')
    sys.stdout.flush()
    device = wait_attached(prompt)
    print('\r' + prompt + 'done' + ' '*50)

    # get the number of channels on this device
    print('Getting the number of channels on the DigiVolt ... ', end='')
    sys.stdout.flush()
    num_chans = int(device.do_cmd('nc;'))
    log(f'Number of channels: {num_chans}')
    print(f'found {num_chans} channels.')

    # set the DigiVolt to use LOW input on all channels
    print('Preparing to calibrate the LOW voltage channel(s) ... ', end='')
    sys.stdout.flush()
    set_low_all(device, num_chans)
    log('All channels set to LOW input')
    print('done!')

    # read the current calibration values from the device
    (low_calib, high_calib) = get_calib(device, num_chans)
    log(f'low_calib={low_calib}')
    for chan in range(num_chans):
        log(f'high_calib[{chan}]={high_calib[chan]}')

    # prepare the LOW voltage source
    low_volts = prepare_low(device)

    # check that the voltage source is working
    check_voltage_source(device)

    # now calibrate the LOW voltage on channel 0
    low_calib = low_calibrate(device, low_volts, low_calib)
    log(f'low_calibrate returned: low_calib={low_calib}')

    # prepare the HIGH voltage source
    high_volts = prepare_high(device)

    # calibrate the HIGH voltage on all channels
    for chan in range(num_chans):
        high_calib[chan] = high_calibrate(device, chan, high_volts) 

    # print results and finish
    print('Final calibration:')
    print(f'LOW calibration: {low_calib:.3f}')
    print('HIGH calibration:')
    for chan in range(num_chans):
        print(f'    {chan}: {high_calib[chan]:.3f}')
    print('Finished!')


# check params, call main()
if len(sys.argv) != 1:
    usage()
    sys.exit(1)

try:
    os.remove(LogFile)
except FileNotFoundError:
    pass

main()

