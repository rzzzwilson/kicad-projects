What is here?
=============

This directory contains:

calibrate.py
------------

A CLI program to calibrate a DigiVolt device.

dump_debug.py
-------------

A small CLI program to dump the results of "d;" on a DigiVolt device.
