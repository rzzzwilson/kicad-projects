#!/usr/bin/env python3

"""
A short program to meauser voltage on a DigiVolt, high range, channel 0.`
"""

from digithing import DigiThing, DigiThingError


# look for a single DigiPot device
used_ports = []

print('******************** Looking for DigiVolt ********************')
try:
    dv = DigiThing('DigiVolt', ignore=used_ports)
except DigiThingError:
    print('No DigiVolt devices found')
    sys.exit(1)

print(f'Found: {dv}')
print(f"dv.do_cmd('id;') -> '{dv.do_cmd('id;').strip()}'")
dv.do_cmd('r0h;r1h;')
while True:
    print(dv.do_cmd('v0;'))
