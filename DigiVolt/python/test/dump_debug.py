#!/usr/bin/env python3

"""
A program to do a "d;" command on a DigiThing and print the results.
"""

import sys
from digithing import DigiThing, DigiThingError


# program version info
Version = 'dump_debug 1.0'


def main():
    # attach to a DigiThing
    try:
        dv = DigiThing('DigiVolt')
    except DigiThingError as e:
        print(e)
        sys.exit(1)

    # do 'd;' command and print the results
    print()
    print(dv.do_cmd('d;'))

    dv.close()


# check params, call main()
if len(sys.argv) != 1:
    print('Usage: dump_debug.py')
    sys.exit(1)

main()
