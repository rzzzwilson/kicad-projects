"""
Program to test the DigiVoltWidget widget.

Usage: test_channel_control.py [-h|--help]
"""


import os
import sys
import math
import getopt
import traceback

from PyQt5 import QtCore
from PyQt5.QtCore import QThread, QObject, pyqtSignal
from PyQt5.QtWidgets import (QApplication, QMainWindow, QWidget,
                             QGridLayout, QVBoxLayout, QHBoxLayout)

from digithing import DigiThing, DigiThingError
from channel_control import DigiVoltWidget

import logger
log = logger.Log('digivolt.log', logger.Log.INFO)


######
# Various demo constants
######

# demo name/version
DemoVersion = '1.0'
DemoName = f'Test DigiVolt widget {DemoVersion}'


################################################################################
# Thread that communicates with the DigiVolt device.
################################################################################

import time

class PollDigiVoltThread(QObject):

    VoltsSignal = pyqtSignal(float, float)

    def __init__(self, device):
        """Prepare the thread.

        device       the DigiVolt device to use

        Results sent back on a signal "VoltsSignal".
        """

        super().__init__()
#        QThread.__init__(self)
        self.device = device
        self.extra_cmd = None   # holds "extra" command string

    @QtCore.pyqtSlot()
    def run(self):
        while True:
            # do command queued by main thread
            if self.extra_cmd:
                log.debug(f"run: .extra_cmd='{self.extra_cmd}'")
                self.device.do_cmd(self.extra_cmd)
                self.extra_cmd = None

            # then get the voltages
            result0 = self.device.do_cmd('v0;').strip()
            result1 = self.device.do_cmd('v1;').strip()
            log.debug(f"run: result0='{result0}', result1='{result1}'")
            try:
                result0 = float(result0)
                result1 = float(result1)
            except ValueError:
                log.debug(f'ValueError: result0={result0}, result1={result1}')
                raise
            self.VoltsSignal.emit(result0, result1)


################################################################################
# The main application window.
################################################################################

class TestDigiVoltWidget(QMainWindow):

    def __init__(self, dv):
        """Create window to test the DigiVolt widget.

        dv  an open DigiVolt object
        """

        super().__init__()

        # build the GUI
        grid = QGridLayout()
        grid.setContentsMargins(2, 2, 2, 2)

        qwidget = QWidget(self)
        qwidget.setLayout(grid)
        self.setCentralWidget(qwidget)

        # build the 'controls' part of GUI
        self.dv_widget_0 = DigiVoltWidget('channel 0', DigiVoltWidget.RangeHigh)
        self.dv_widget_1 = DigiVoltWidget('channel 1', DigiVoltWidget.RangeHigh)

        grid.addWidget(self.dv_widget_0, 0, 0, 1, 1)
        grid.addWidget(self.dv_widget_1, 1, 0, 1, 1)

        # set the size of the demo window, etc
        self.setWindowTitle(DemoName)

        # establish connection to a DigiVolt device and set state
        self.device = dv
        dv.do_cmd('R0H;R1H;')       # set to HIGH range

        # connect DigiVolt device events to handlers
        self.dv_widget_0.Range.connect(self.range_change_0)
        self.dv_widget_1.Range.connect(self.range_change_1)

        # prepare the device polling thread
        self.thread = PollDigiVoltThread(self.device)
        self.workerThread = QtCore.QThread()
        self.workerThread.started.connect(self.thread.run)
        self.thread.moveToThread(self.workerThread)

        # connect local thread event to handler
        self.thread.VoltsSignal.connect(self.volts_update)

        self.show()

        # OK, start the thread
        self.workerThread.start()

    @QtCore.pyqtSlot(float, float)
    def volts_update(self, v0, v1):
        log.debug(f'volts_update: voltage0={v0}, voltage1={v1}')
        self.dv_widget_0.set_voltage(v0)
        self.dv_widget_1.set_voltage(v1)

    @QtCore.pyqtSlot(int)
    def range_change_0(self, hilo):
        """Handle a "range change" event for channel 0."""

        log.debug(f"Change range on channel 0 to '{DigiVoltWidget.RangeCBValues[hilo]}'")
        state = DigiVoltWidget.RangeCBValues[hilo][0]
        self.thread.extra_cmd = f'R0{state};'

    @QtCore.pyqtSlot(int)
    def range_change_1(self, hilo):
        """Handle a "range change" event for channel 1."""

        log.debug(f"Change range on channel 1 to '{DigiVoltWidget.RangeCBValues[hilo]}'")
        state = DigiVoltWidget.RangeCBValues[hilo][0]
        self.thread.extra_cmd = f'R1{state};'

###############################################################################

# our own handler for uncaught exceptions
def excepthook(type, value, tb):
    msg = '\n' + '=' * 80
    msg += '\nUncaught exception:\n'
    msg += ''.join(traceback.format_exception(type, value, tb))
    msg += '=' * 80 + '\n'
    print(msg)


def usage(msg=None):
    if msg:
        print(('*'*80 + '\n%s\n' + '*'*80) % msg)
    print(__doc__)


# plug our handler into the python system
sys.excepthook = excepthook

# analyse the command line args
argv = sys.argv[1:]

try:
    (opts, args) = getopt.getopt(argv, 'h', ['help'])
except getopt.error:
    usage()
    sys.exit(1)

for (opt, param) in opts:
    if opt in ['-h', '--help']:
        usage()
        sys.exit(0)

# get a DigiVolt device
dv = DigiThing('DigiVolt')

# start the app
app = QApplication(args)
ex = TestDigiVoltWidget(dv)
sys.exit(app.exec_())
