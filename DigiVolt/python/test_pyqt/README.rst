This program is an initial test using PyQt to interface with DigiVolt.

Design
------

The main window will contain two custom widgets, each controlling one
DigiVolt channel:

>>>
    +---------------------------------------------------------------------------+
    | +------+               +--------+          +----------------------------+ |
    | | OFF  | Channel X     |  XX.XX | volts    |                            | |
    | +------+               +--------+          |                            | |
    |                                            |                            | |
    |     +-+          +-------------------+     |                            | |
    |     |X| save to: | .../.../.../......|     |                            | |
    |     +-+          +-------------------+     |       matplotlib graph     | |
    |                                            |       showing voltage      | |
    |                                            |         versus time        | |
    |     +-+          +---------------+---+     |                            | |
    |     |X| plotting | update period | V |     |                            | |
    |     +-+          +---------------+---+     |                            | |
    |                                            |                            | |
    |                                            +----------------------------+ |
    +---------------------------------------------------------------------------+
>>>
