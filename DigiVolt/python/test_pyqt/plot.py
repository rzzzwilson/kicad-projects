import matplotlib.pyplot as plt

x_data = []
y_data = []
with open('data.out', 'r') as fd:
    for line in fd:
        (y, x) = map(float, line.split())
        x_data.append(x)
        y_data.append(y)

plt.plot(x_data, y_data, label='channel 0')
#plt.axis([0, 260, 0, 5])
plt.xlabel('Multimeter Voltage')
plt.ylabel('DigiVolt Voltage')
plt.title('DigiVolt Channel 0 Tracking')
plt.legend(bbox_to_anchor=(0.025, 0.975), loc='upper left', borderaxespad=0.)
plt.savefig('track_plot.png')
plt.show()



#x = np.linspace(0, 10, 500)
#dashes = [10, 5, 100, 5]  # 10 points on, 5 off, 100 on, 5 off
#
#fig, ax = plt.subplots()
#line1, = ax.plot(x, np.sin(x), '--', linewidth=2,
#                 label='Dashes set retroactively')
#line1.set_dashes(dashes)
#
#line2, = ax.plot(x, -1 * np.sin(x), dashes=[30, 5, 10, 5],
#                 label='Dashes set proactively')
#
#ax.legend(loc='lower right')
#plt.show()
