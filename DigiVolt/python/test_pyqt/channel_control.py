"""
The PyQt custom control for one DigiVolt channel control.

Constructor:

    dv = DigiVoltWidget('title', hilo)

Methods & Attributes:

    dv.set_voltage(value)       # sets displayed voltage to 'value'

Events:
    dv.Range.emit(value)

"""

import os
import sys
import time
import random

from PyQt5.QtCore import Qt, pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import (QWidget, QGridLayout, QHBoxLayout, QGroupBox,
                             QPushButton, QLabel, QComboBox, QLineEdit,
                             QSizePolicy, QFileDialog, QCheckBox)
from PyQt5.QtGui import QColor


################################################################################
# The custom widget to display DigiVolt data for one channel.
################################################################################

class DigiVoltWidget(QWidget):
    """Custom DigiVolt display widget."""

    # values in the Range combobox
    RangeCBValues = ('LOW', 'HIGH')
    RangeLow = RangeCBValues.index('LOW')
    RangeHigh = RangeCBValues.index('HIGH')

    # signals raised by this widget
    Range = pyqtSignal(int)     # argument is RangeHigh or RangeLow

    # some stylesheets
    LabelStyle = 'QLabel { background-color : #f0f0f0; border: 1px solid gray; border-radius: 3px; }'
    GroupStyle = ('QGroupBox { background-color: rgb(230, 230, 230); }'
                  'QGroupBox::title { subcontrol-origin: margin; '
                                 '    background-color: rgb(215, 215, 215); '
                                 '    border-radius: 3px; '
                                 '    padding: 2 2px; '
                                 '    color: black; }')


    def __init__(self, title, hilo=RangeHigh):
        """Initialise a DigiVoltWidget instance.

        title    title to give the custom widget
        hilo    the initial device range to use
        """


        super().__init__()

        # start the layout
        group_box = QGroupBox(title)
        group_box.setStyleSheet(DigiVoltWidget.GroupStyle)

        grid_layout = QGridLayout()
        grid_layout.setContentsMargins(5, 5, 5, 5)
#        grid_layout.setHorizontalSpacing(1)
#        grid_layout.setColumnStretch(0, 1)

        # create and position the sub-widgets
        row = 0

        self.cb_hilo = QComboBox(self)
        for r in DigiVoltWidget.RangeCBValues:
            self.cb_hilo.addItem(r)
        self.cb_hilo.setCurrentIndex(hilo)
        # set display to required range
        grid_layout.addWidget(self.cb_hilo, row, 0)

        self.lb_voltage = QLabel('xx.xx volts', self)
        self.lb_voltage.setAlignment(Qt.AlignLeft)
        grid_layout.addWidget(self.lb_voltage, row, 1)

        group_box.setLayout(grid_layout)

        layout = QHBoxLayout()
        layout.setContentsMargins(1, 1, 1, 1)
        layout.addWidget(group_box)

        self.setLayout(layout)

        # set size hints
#        self.setMinimumSize(350, 200)
        size_policy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.setSizePolicy(size_policy)

        # connect internal widget events to handlers
        self.cb_hilo.currentIndexChanged.connect(self.hilo_change)

    def set_voltage(self, value):
        """Set widget displayed voltage to 'value'."""

        self.lb_voltage.setText(f'{value:.2f} volts')

#    @pyqtSlot()
    def hilo_change(self):
        index = self.cb_hilo.currentIndex()
        self.Range.emit(index)
