#!/usr/bin/env python3

"""
A program to test the tracking of two DigiVolt channels using a DigiPot device.
"""

import sys
import time
import traceback
import digithing as dgt


def wait_attached(prompt, thing):
    """Wait until a DigiThing device is connected.

    prompt  prompt to print
    thing   string identifying DigiThing device

    Return the DigiThing object reference.
    """

    propellor_seq = '|/-\\'
    count = 0

    # wait for a DigiThing device to be attached
    while True:
        error = None
        try:
            dv = dgt.DigiThing(thing)
            print(f"\r{prompt}: {thing} found{' '*40}")
            return dv
        except dgt.Error as e:
            error = str(e)
        except dgt.NotFound:
            pass

        propellor = propellor_seq[count]
        count += 1
        if count >= len(propellor_seq):
            count = 0

        if error:
            print(f"\r{prompt}: {error}       ", end='')
        else:
            print(f"\r{prompt}: {propellor}       ", end='')
        time.sleep(0.5)

def main():
    # look for the DigiVolt device
    digivolt = wait_attached('Looking for a DigiVolt device', 'DigiVolt')

    # now look for a DigiPot device
    digipot = wait_attached('Looking for a DigiPot device', 'DigiPot')

    # set digivolt to high range
    digivolt.do_cmd('R0H;R1H;')

    # set applied voltage to minimum
    digipot.do_cmd('w00;w10;')

    for offset in range(0, 256, 5):
        # set voltage to next step
        digipot.do_cmd(f'w0{offset};w1{offset};')

        # get measured voltage for both channels
        (v0, v1) = digivolt.do_cmd('v0;v1;').split('\n')
#        v0 = digivolt.do_cmd('v0;')
#        v1 = digivolt.do_cmd('v1;')

        # display results
        print(f'{offset},{v0},{v1}')


# capture any not-handled exceptions
def excepthook(type, value, tback):
    msg = '\n' + '=' * 80
    msg += '\nUncaught exception:\n'
    msg += ''.join(traceback.format_exception(type, value, tback))
    msg += '=' * 80 + '\n'
    log(msg)
    print(msg)

# plug our handler into the python system
sys.excepthook = excepthook

main()
