What is this?
=============

*DigiVolt* is a small two channel voltage measuring device.  It is designed
to measure voltage up to 50 volts and to report to a controlling PC through
the USB serial port.

Design
------

The AtMega32U4 microcontroller can measure up to 2.56 volts using an internal
voltage reference.  The maximum voltage measurable can be increased by using
a potential divider.  In that case, the voltage measured by the microcontroller
is a fixed fraction of the actual voltage, at the expense of reduced resolution
for lower voltages.

In an attempt to get maximum precision in voltage measurements and to also add
an "auto-range" feature, the external voltage divider function will be handled
by an MCP42xxx digitial potentiometer, controlled by the microcontroller.  The
voltage to be measured is applied to the "top" of the potentiometer, the wiper
feeds the divided voltage to the microcontroller to be measured and the other
digipot pin goes to ground.  The measurement algorithm then goes something like
this:

1. set digital potentiometer to minimum voltage + 1 step (minimum measurement voltage)
2. measure the divided voltage digital value (0 to 1023)
3. if measured voltage >= 1023, goto step 7
4. remember divided voltage value
5. increase wiper setting up by 1 (or 2, 4, etc)
6. goto step 2
7. use remembered voltage value and divider setting to calculate input voltage

In addition, the input voltage can be applied to an external fixed divider before
being measured by the MCP42xxx/AtMega pair.  This increases the voltage range that
can be measured.  The input through the fixed divider will be the "HI" range.
Input direct to the digipot will be the "LO" range".

The MCP42xxx devices have two controllable potentiometers, so there will be two
input voltages to the device.

The microcontroller will accept commands through the USB serial connection.
This will allow the device to be controlled by a program on a laptop.

Calibration
-----------

There are two uncertain areas in the design that must be calibrated:

* microcontroller internal reference value
* external fixed divider uncertain resistance values

The microcontroller internal reference can be calibrated by setting the MCP42xxx
digipot to "maximum" and applying a know voltage <= 2.56 volts to the "LO" input.
Once the actual value for the internal reference is known this can be saved in
EEPROM.

The fixed divider ratio can be determined in a similar manner to the internal
reference (above) and this should be done *after* the internal reference is
calibrated.

Command Language
----------------

The commands for the serial USB connection are:

+------------+----------------------------------------------+
| Command    | Meaning                                      |
+============+==============================================+
| H;         | write help text to the console               |
+------------+----------------------------------------------+
| ID;        | return a device identifier string            |
+------------+----------------------------------------------+
| NG;        | get number of channels                       |
+------------+----------------------------------------------+
| V?G;       | get voltage for channel '?'                  |
+------------+----------------------------------------------+
| S?x;       | set scale on channel '?' to 'L' or 'H'       |
+------------+----------------------------------------------+
| C?Lxxxx;   | set channel '?' LO calibration to 'xxxx'     |
+------------+----------------------------------------------+
| C?Hxxxx;   | set channel '?' HI calibration to 'xxxx'     |
+------------+----------------------------------------------+
| D;         | return some internal debug information       |
+------------+----------------------------------------------+

Schematic
---------

.. image:: DigiVolt_1.0_Schematic.png

PCB
---

.. image:: PCB_1.0a.png

Software
--------

Python software to interface with DigiVolt will be held in the *python*
subdirectory.
