The hardware design here is a breakout board for the ESP32-PICO-MINI-02
module.  The basic idea is to make something usable on a breadboard
and including a regulator to convert the USB 5 volts to 3.3 volts for
the module.  Apart from the regulator, minimal circuitry.
