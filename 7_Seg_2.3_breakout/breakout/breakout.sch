EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "7 Segment 2.3\" Breakout"
Date "2022-04-18"
Rev "1.0"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L breakout:7digit U0
U 1 1 625D7317
P 2400 3750
F 0 "U0" H 1400 5050 50  0000 L CNN
F 1 "7digit" H 1450 4950 50  0000 L CNN
F 2 "breakout:breakout" H 2375 3875 50  0001 C CNN
F 3 "" H 2375 3875 50  0001 C CNN
	1    2400 3750
	1    0    0    -1  
$EndComp
Text GLabel 2300 5150 3    50   Input ~ 0
E0
Text GLabel 2400 5150 3    50   Input ~ 0
D0
Text GLabel 2500 5150 3    50   Input ~ 0
C0
Wire Wire Line
	2300 5150 2300 5100
Wire Wire Line
	2400 5100 2400 5150
Wire Wire Line
	2500 5100 2500 5150
Wire Wire Line
	2200 5100 2200 5400
Wire Wire Line
	2200 5400 2600 5400
Wire Wire Line
	2600 5400 2600 5100
Text GLabel 2200 5450 3    50   Input ~ 0
CC0
Wire Wire Line
	2200 5450 2200 5400
Connection ~ 2200 5400
Text GLabel 2200 2300 1    50   Input ~ 0
G0
Wire Wire Line
	2200 2300 2200 2400
Text GLabel 2300 2300 1    50   Input ~ 0
F0
Text GLabel 2400 2300 1    50   Input ~ 0
DP0
Text GLabel 2500 2300 1    50   Input ~ 0
A0
Text GLabel 2600 2300 1    50   Input ~ 0
B0
Wire Wire Line
	2300 2300 2300 2400
Wire Wire Line
	2400 2300 2400 2400
Wire Wire Line
	2500 2300 2500 2400
Wire Wire Line
	2600 2300 2600 2400
$Comp
L breakout:7digit U1
U 1 1 625E1FF3
P 4750 3750
F 0 "U1" H 3750 5050 50  0000 L CNN
F 1 "7digit" H 3800 4950 50  0000 L CNN
F 2 "breakout:breakout" H 4725 3875 50  0001 C CNN
F 3 "" H 4725 3875 50  0001 C CNN
	1    4750 3750
	1    0    0    -1  
$EndComp
Text GLabel 4650 5150 3    50   Input ~ 0
E1
Text GLabel 4750 5150 3    50   Input ~ 0
D1
Text GLabel 4850 5150 3    50   Input ~ 0
C1
Wire Wire Line
	4650 5150 4650 5100
Wire Wire Line
	4750 5100 4750 5150
Wire Wire Line
	4850 5100 4850 5150
Wire Wire Line
	4550 5100 4550 5400
Wire Wire Line
	4550 5400 4950 5400
Wire Wire Line
	4950 5400 4950 5100
Text GLabel 4550 5450 3    50   Input ~ 0
CC1
Wire Wire Line
	4550 5450 4550 5400
Connection ~ 4550 5400
Text GLabel 4550 2300 1    50   Input ~ 0
G1
Wire Wire Line
	4550 2300 4550 2400
Text GLabel 4650 2300 1    50   Input ~ 0
F1
Text GLabel 4750 2300 1    50   Input ~ 0
DP1
Text GLabel 4850 2300 1    50   Input ~ 0
A1
Text GLabel 4950 2300 1    50   Input ~ 0
B1
Wire Wire Line
	4650 2300 4650 2400
Wire Wire Line
	4750 2300 4750 2400
Wire Wire Line
	4850 2300 4850 2400
Wire Wire Line
	4950 2300 4950 2400
$Comp
L breakout:7digit U2
U 1 1 625E35C4
P 7100 3750
F 0 "U2" H 6100 5050 50  0000 L CNN
F 1 "7digit" H 6150 4950 50  0000 L CNN
F 2 "breakout:breakout" H 7075 3875 50  0001 C CNN
F 3 "" H 7075 3875 50  0001 C CNN
	1    7100 3750
	1    0    0    -1  
$EndComp
Text GLabel 7000 5150 3    50   Input ~ 0
E2
Text GLabel 7100 5150 3    50   Input ~ 0
D2
Text GLabel 7200 5150 3    50   Input ~ 0
C2
Wire Wire Line
	7000 5150 7000 5100
Wire Wire Line
	7100 5100 7100 5150
Wire Wire Line
	7200 5100 7200 5150
Wire Wire Line
	6900 5100 6900 5400
Wire Wire Line
	6900 5400 7300 5400
Wire Wire Line
	7300 5400 7300 5100
Text GLabel 6900 5450 3    50   Input ~ 0
CC2
Wire Wire Line
	6900 5450 6900 5400
Connection ~ 6900 5400
Text GLabel 6900 2300 1    50   Input ~ 0
G2
Wire Wire Line
	6900 2300 6900 2400
Text GLabel 7000 2300 1    50   Input ~ 0
F2
Text GLabel 7100 2300 1    50   Input ~ 0
DP2
Text GLabel 7200 2300 1    50   Input ~ 0
A2
Text GLabel 7300 2300 1    50   Input ~ 0
B2
Wire Wire Line
	7000 2300 7000 2400
Wire Wire Line
	7100 2300 7100 2400
Wire Wire Line
	7200 2300 7200 2400
Wire Wire Line
	7300 2300 7300 2400
$Comp
L breakout:7digit U3
U 1 1 625E5C48
P 9450 3750
F 0 "U3" H 8450 5050 50  0000 L CNN
F 1 "7digit" H 8500 4950 50  0000 L CNN
F 2 "breakout:breakout" H 9425 3875 50  0001 C CNN
F 3 "" H 9425 3875 50  0001 C CNN
	1    9450 3750
	1    0    0    -1  
$EndComp
Text GLabel 9350 5150 3    50   Input ~ 0
E3
Text GLabel 9450 5150 3    50   Input ~ 0
D3
Text GLabel 9550 5150 3    50   Input ~ 0
C3
Wire Wire Line
	9350 5150 9350 5100
Wire Wire Line
	9450 5100 9450 5150
Wire Wire Line
	9550 5100 9550 5150
Wire Wire Line
	9250 5100 9250 5400
Wire Wire Line
	9250 5400 9650 5400
Wire Wire Line
	9650 5400 9650 5100
Text GLabel 9250 5450 3    50   Input ~ 0
CC3
Wire Wire Line
	9250 5450 9250 5400
Connection ~ 9250 5400
Text GLabel 9250 2300 1    50   Input ~ 0
G3
Wire Wire Line
	9250 2300 9250 2400
Text GLabel 9350 2300 1    50   Input ~ 0
F3
Text GLabel 9450 2300 1    50   Input ~ 0
DP3
Text GLabel 9550 2300 1    50   Input ~ 0
A3
Text GLabel 9650 2300 1    50   Input ~ 0
B3
Wire Wire Line
	9350 2300 9350 2400
Wire Wire Line
	9450 2300 9450 2400
Wire Wire Line
	9550 2300 9550 2400
Wire Wire Line
	9650 2300 9650 2400
$Comp
L Connector_Generic:Conn_01x04 J2
U 1 1 625EE6FD
P 6000 6200
F 0 "J2" V 5872 6380 50  0000 L CNN
F 1 "Digits" V 5963 6380 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 6000 6200 50  0001 C CNN
F 3 "~" H 6000 6200 50  0001 C CNN
	1    6000 6200
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J1
U 1 1 625EF899
P 5900 1500
F 0 "J1" V 5864 1012 50  0000 R CNN
F 1 "Segments" V 5773 1012 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 5900 1500 50  0001 C CNN
F 3 "~" H 5900 1500 50  0001 C CNN
	1    5900 1500
	0    -1   -1   0   
$EndComp
Text GLabel 5800 5900 1    50   Input ~ 0
CC3
Text GLabel 5900 5900 1    50   Input ~ 0
CC2
Text GLabel 6000 5900 1    50   Input ~ 0
CC1
Text GLabel 6100 5900 1    50   Input ~ 0
CC0
Wire Wire Line
	5800 5900 5800 6000
Wire Wire Line
	5900 5900 5900 6000
Wire Wire Line
	6000 5900 6000 6000
Wire Wire Line
	6100 5900 6100 6000
Text GLabel 2900 1250 0    50   Input ~ 0
DP0
Text GLabel 2900 1350 0    50   Input ~ 0
DP1
Text GLabel 2900 1450 0    50   Input ~ 0
DP2
Text GLabel 2900 1550 0    50   Input ~ 0
DP3
Text GLabel 3200 1250 2    50   Input ~ 0
DP
Wire Wire Line
	2900 1250 3050 1250
Wire Wire Line
	2900 1350 3050 1350
Wire Wire Line
	3050 1350 3050 1250
Connection ~ 3050 1250
Wire Wire Line
	3050 1250 3200 1250
Wire Wire Line
	2900 1450 3050 1450
Wire Wire Line
	3050 1450 3050 1350
Connection ~ 3050 1350
Wire Wire Line
	2900 1550 3050 1550
Wire Wire Line
	3050 1550 3050 1450
Connection ~ 3050 1450
Text GLabel 2200 1250 0    50   Input ~ 0
G0
Text GLabel 2200 1350 0    50   Input ~ 0
G1
Text GLabel 2200 1450 0    50   Input ~ 0
G2
Text GLabel 2200 1550 0    50   Input ~ 0
G3
Text GLabel 2500 1250 2    50   Input ~ 0
G
Wire Wire Line
	2200 1250 2350 1250
Wire Wire Line
	2200 1350 2350 1350
Wire Wire Line
	2350 1350 2350 1250
Connection ~ 2350 1250
Wire Wire Line
	2350 1250 2500 1250
Wire Wire Line
	2200 1450 2350 1450
Wire Wire Line
	2350 1450 2350 1350
Connection ~ 2350 1350
Wire Wire Line
	2200 1550 2350 1550
Wire Wire Line
	2350 1550 2350 1450
Connection ~ 2350 1450
Text GLabel 2900 700  0    50   Input ~ 0
D0
Text GLabel 2900 800  0    50   Input ~ 0
D1
Text GLabel 2900 900  0    50   Input ~ 0
D2
Text GLabel 2900 1000 0    50   Input ~ 0
D3
Text GLabel 3200 700  2    50   Input ~ 0
D
Wire Wire Line
	2900 700  3050 700 
Wire Wire Line
	2900 800  3050 800 
Wire Wire Line
	3050 800  3050 700 
Connection ~ 3050 700 
Wire Wire Line
	3050 700  3200 700 
Wire Wire Line
	2900 900  3050 900 
Wire Wire Line
	3050 900  3050 800 
Connection ~ 3050 800 
Wire Wire Line
	2900 1000 3050 1000
Wire Wire Line
	3050 1000 3050 900 
Connection ~ 3050 900 
Text GLabel 2200 700  0    50   Input ~ 0
C0
Text GLabel 2200 800  0    50   Input ~ 0
C1
Text GLabel 2200 900  0    50   Input ~ 0
C2
Text GLabel 2200 1000 0    50   Input ~ 0
C3
Text GLabel 2500 700  2    50   Input ~ 0
C
Wire Wire Line
	2200 700  2350 700 
Wire Wire Line
	2200 800  2350 800 
Wire Wire Line
	2350 800  2350 700 
Connection ~ 2350 700 
Wire Wire Line
	2350 700  2500 700 
Wire Wire Line
	2200 900  2350 900 
Wire Wire Line
	2350 900  2350 800 
Connection ~ 2350 800 
Wire Wire Line
	2200 1000 2350 1000
Wire Wire Line
	2350 1000 2350 900 
Connection ~ 2350 900 
Text GLabel 1500 700  0    50   Input ~ 0
B0
Text GLabel 1500 800  0    50   Input ~ 0
B1
Text GLabel 1500 900  0    50   Input ~ 0
B2
Text GLabel 1500 1000 0    50   Input ~ 0
B3
Text GLabel 1800 700  2    50   Input ~ 0
B
Wire Wire Line
	1500 700  1650 700 
Wire Wire Line
	1500 800  1650 800 
Wire Wire Line
	1650 800  1650 700 
Connection ~ 1650 700 
Wire Wire Line
	1650 700  1800 700 
Wire Wire Line
	1500 900  1650 900 
Wire Wire Line
	1650 900  1650 800 
Connection ~ 1650 800 
Wire Wire Line
	1500 1000 1650 1000
Wire Wire Line
	1650 1000 1650 900 
Connection ~ 1650 900 
Text GLabel 800  700  0    50   Input ~ 0
A0
Text GLabel 800  800  0    50   Input ~ 0
A1
Text GLabel 800  900  0    50   Input ~ 0
A2
Text GLabel 800  1000 0    50   Input ~ 0
A3
Text GLabel 1100 700  2    50   Input ~ 0
A
Wire Wire Line
	800  700  950  700 
Wire Wire Line
	800  800  950  800 
Wire Wire Line
	950  800  950  700 
Connection ~ 950  700 
Wire Wire Line
	950  700  1100 700 
Wire Wire Line
	800  900  950  900 
Wire Wire Line
	950  900  950  800 
Connection ~ 950  800 
Wire Wire Line
	800  1000 950  1000
Wire Wire Line
	950  1000 950  900 
Connection ~ 950  900 
Text GLabel 1500 1250 0    50   Input ~ 0
F0
Text GLabel 1500 1350 0    50   Input ~ 0
F1
Text GLabel 1500 1450 0    50   Input ~ 0
F2
Text GLabel 1500 1550 0    50   Input ~ 0
F3
Text GLabel 1800 1250 2    50   Input ~ 0
F
Wire Wire Line
	1500 1250 1650 1250
Wire Wire Line
	1500 1350 1650 1350
Wire Wire Line
	1650 1350 1650 1250
Connection ~ 1650 1250
Wire Wire Line
	1650 1250 1800 1250
Wire Wire Line
	1500 1450 1650 1450
Wire Wire Line
	1650 1450 1650 1350
Connection ~ 1650 1350
Wire Wire Line
	1500 1550 1650 1550
Wire Wire Line
	1650 1550 1650 1450
Connection ~ 1650 1450
Text GLabel 800  1250 0    50   Input ~ 0
E0
Text GLabel 800  1350 0    50   Input ~ 0
E1
Text GLabel 800  1450 0    50   Input ~ 0
E2
Text GLabel 800  1550 0    50   Input ~ 0
E3
Text GLabel 1100 1250 2    50   Input ~ 0
E
Wire Wire Line
	800  1250 950  1250
Wire Wire Line
	800  1350 950  1350
Wire Wire Line
	950  1350 950  1250
Connection ~ 950  1250
Wire Wire Line
	950  1250 1100 1250
Wire Wire Line
	800  1450 950  1450
Wire Wire Line
	950  1450 950  1350
Connection ~ 950  1350
Wire Wire Line
	800  1550 950  1550
Wire Wire Line
	950  1550 950  1450
Connection ~ 950  1450
Text GLabel 5600 1800 3    50   Input ~ 0
A
Text GLabel 5700 1800 3    50   Input ~ 0
B
Text GLabel 5800 1800 3    50   Input ~ 0
C
Text GLabel 5900 1800 3    50   Input ~ 0
D
Text GLabel 6000 1800 3    50   Input ~ 0
E
Text GLabel 6100 1800 3    50   Input ~ 0
F
Text GLabel 6200 1800 3    50   Input ~ 0
G
Text GLabel 6300 1800 3    50   Input ~ 0
DP
Wire Wire Line
	5600 1700 5600 1800
Wire Wire Line
	5700 1700 5700 1800
Wire Wire Line
	5800 1800 5800 1700
Wire Wire Line
	5900 1700 5900 1800
Wire Wire Line
	6000 1800 6000 1700
Wire Wire Line
	6100 1700 6100 1800
Wire Wire Line
	6200 1800 6200 1700
Wire Wire Line
	6300 1700 6300 1800
$EndSCHEMATC
