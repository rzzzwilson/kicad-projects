# SoftSwitch 2

Another "soft switch" similar to the original SoftSwitch.  This one is
taken from the QMX+ transceiver by Hans G0UPL.

The QMX+ schematic is in the *resources* directory.
