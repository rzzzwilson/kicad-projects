//
// Test code for SoftSwitch 2.
//
// Connect a SPST between ON and GND. 
//
//   Uno -> SoftSwitch
//    7  ->   HOLD
//    6  ->   ON (button)
//
// The sketch will latch the power until the button
// is long pressed

#include <ezButton.h>

const byte HoldPin = 7;
const byte SensePin = 6;

unsigned long next_change = 0;
unsigned long interval = 500;

ezButton button(SensePin);

void flash_led(int n)
{
  for (int i = 0; i < n; ++i)
  {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(50);
    digitalWrite(LED_BUILTIN, LOW);
    delay(50);
  }
}

void setup()
{
  // set the power HOLD pin high immediately
  pinMode(HoldPin, OUTPUT);
  digitalWrite(HoldPin, HIGH);

  // turn on LED_BUILTIN
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);

  // wait until SENSE button not pressed
  button.setDebounceTime(50);
  delay(20);
  button.loop();
  delay(20);
  while (button.getState() == LOW)
//  while (digitalRead(SensePin) == LOW)
  {
    button.loop();
    delay(10);
  }

  // flash the LED - we are ready
  flash_led(5);
}

void loop()
{
  button.loop();

  if (button.isPressed())
  {
    delay(250);
    flash_led(5);
    digitalWrite(HoldPin, LOW);
    while (1)
      ;
  }

  unsigned long now = millis();
  
  if (now - next_change >= interval)
  {
    next_change = now;
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  }
}
