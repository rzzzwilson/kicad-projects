EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Frequency Counter for AVR"
Date "2021-07-26"
Rev "1.0"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 "From \"Arduino Projects for Amateur Radio\", chapter 15"
$EndDescr
$Comp
L Transistor_BJT:2N3904 Q1
U 1 1 60FE6C4E
P 2000 2050
F 0 "Q1" H 2191 2096 50  0000 L CNN
F 1 "2N2222A" H 2191 2005 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 2200 1975 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/2N3903-D.PDF" H 2000 2050 50  0001 L CNN
	1    2000 2050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 60FE78AE
P 2100 2600
F 0 "#PWR0101" H 2100 2350 50  0001 C CNN
F 1 "GND" H 2105 2427 50  0000 C CNN
F 2 "" H 2100 2600 50  0001 C CNN
F 3 "" H 2100 2600 50  0001 C CNN
	1    2100 2600
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J1
U 1 1 60FE8260
P 1050 2050
F 0 "J1" H 1200 2200 50  0000 C CNN
F 1 "Signal In" H 1100 2300 50  0000 C CNN
F 2 "" H 1050 2050 50  0001 C CNN
F 3 " ~" H 1050 2050 50  0001 C CNN
	1    1050 2050
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 60FE9BA9
P 1050 2600
F 0 "#PWR0102" H 1050 2350 50  0001 C CNN
F 1 "GND" H 1055 2427 50  0000 C CNN
F 2 "" H 1050 2600 50  0001 C CNN
F 3 "" H 1050 2600 50  0001 C CNN
	1    1050 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 2250 1050 2450
Wire Wire Line
	2100 2250 2100 2450
$Comp
L Device:R_Small R1
U 1 1 60FEAD40
P 1750 1850
F 0 "R1" H 1809 1896 50  0000 L CNN
F 1 "470K" H 1809 1805 50  0000 L CNN
F 2 "" H 1750 1850 50  0001 C CNN
F 3 "~" H 1750 1850 50  0001 C CNN
	1    1750 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 1950 1750 2050
Wire Wire Line
	1750 2050 1800 2050
Wire Wire Line
	2100 1500 2100 1700
Wire Wire Line
	1750 1750 1750 1700
Wire Wire Line
	1750 1700 2100 1700
Connection ~ 2100 1700
Wire Wire Line
	2100 1700 2100 1850
$Comp
L Device:L_Small L1
U 1 1 60FEC641
P 2100 950
F 0 "L1" H 2056 904 50  0000 R CNN
F 1 "33uH" H 2056 995 50  0000 R CNN
F 2 "" H 2100 950 50  0001 C CNN
F 3 "~" H 2100 950 50  0001 C CNN
	1    2100 950 
	-1   0    0    1   
$EndComp
Wire Wire Line
	2100 1050 2100 1150
$Comp
L Device:C_Small C2
U 1 1 60FEECD1
P 1750 1300
F 0 "C2" H 1600 1400 50  0000 L CNN
F 1 "0.1uF" H 1450 1300 50  0000 L CNN
F 2 "" H 1750 1300 50  0001 C CNN
F 3 "~" H 1750 1300 50  0001 C CNN
	1    1750 1300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 60FEF6F6
P 1750 1400
F 0 "#PWR0103" H 1750 1150 50  0001 C CNN
F 1 "GND" H 1600 1300 50  0000 C CNN
F 2 "" H 1750 1400 50  0001 C CNN
F 3 "" H 1750 1400 50  0001 C CNN
	1    1750 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 1200 1750 1150
Wire Wire Line
	1750 1150 2100 1150
Connection ~ 2100 1150
Wire Wire Line
	2100 1150 2100 1300
$Comp
L Device:C_Small C3
U 1 1 60FF00B0
P 2600 950
F 0 "C3" H 2692 996 50  0000 L CNN
F 1 "0.1uF" H 2692 905 50  0000 L CNN
F 2 "" H 2600 950 50  0001 C CNN
F 3 "~" H 2600 950 50  0001 C CNN
	1    2600 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 850  2100 750 
Wire Wire Line
	2100 750  2600 750 
Wire Wire Line
	2600 750  2600 850 
Connection ~ 2600 750 
$Comp
L power:GND #PWR0104
U 1 1 60FF0CF8
P 2600 1100
F 0 "#PWR0104" H 2600 850 50  0001 C CNN
F 1 "GND" H 2450 1000 50  0000 C CNN
F 2 "" H 2600 1100 50  0001 C CNN
F 3 "" H 2600 1100 50  0001 C CNN
	1    2600 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 1050 2600 1100
$Comp
L Transistor_BJT:2N3904 Q2
U 1 1 60FF39B1
P 3500 2050
F 0 "Q2" H 3691 2096 50  0000 L CNN
F 1 "2N2222A" H 3691 2005 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 3700 1975 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/2N3903-D.PDF" H 3500 2050 50  0001 L CNN
	1    3500 2050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 60FF39BB
P 3600 2600
F 0 "#PWR0105" H 3600 2350 50  0001 C CNN
F 1 "GND" H 3605 2427 50  0000 C CNN
F 2 "" H 3600 2600 50  0001 C CNN
F 3 "" H 3600 2600 50  0001 C CNN
	1    3600 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 2250 3600 2450
$Comp
L Device:R_Small R3
U 1 1 60FF39C6
P 3250 1850
F 0 "R3" H 3309 1896 50  0000 L CNN
F 1 "470K" H 3309 1805 50  0000 L CNN
F 2 "" H 3250 1850 50  0001 C CNN
F 3 "~" H 3250 1850 50  0001 C CNN
	1    3250 1850
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R4
U 1 1 60FF39D0
P 3600 1400
F 0 "R4" H 3659 1446 50  0000 L CNN
F 1 "1K0" H 3659 1355 50  0000 L CNN
F 2 "" H 3600 1400 50  0001 C CNN
F 3 "~" H 3600 1400 50  0001 C CNN
	1    3600 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 1950 3250 2050
Wire Wire Line
	3250 2050 3300 2050
Wire Wire Line
	3600 1500 3600 1700
Wire Wire Line
	3250 1750 3250 1700
Wire Wire Line
	3250 1700 3600 1700
Connection ~ 3600 1700
Wire Wire Line
	3600 1700 3600 1850
$Comp
L Device:L_Small L2
U 1 1 60FF39E1
P 3600 950
F 0 "L2" H 3556 904 50  0000 R CNN
F 1 "33uH" H 3556 995 50  0000 R CNN
F 2 "" H 3600 950 50  0001 C CNN
F 3 "~" H 3600 950 50  0001 C CNN
	1    3600 950 
	-1   0    0    1   
$EndComp
Wire Wire Line
	3600 1050 3600 1150
$Comp
L Device:C_Small C5
U 1 1 60FF39EC
P 3250 1300
F 0 "C5" H 3100 1400 50  0000 L CNN
F 1 "0.1uF" H 2950 1300 50  0000 L CNN
F 2 "" H 3250 1300 50  0001 C CNN
F 3 "~" H 3250 1300 50  0001 C CNN
	1    3250 1300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 60FF39F6
P 3250 1400
F 0 "#PWR0106" H 3250 1150 50  0001 C CNN
F 1 "GND" H 3100 1300 50  0000 C CNN
F 2 "" H 3250 1400 50  0001 C CNN
F 3 "" H 3250 1400 50  0001 C CNN
	1    3250 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 1200 3250 1150
Wire Wire Line
	3250 1150 3600 1150
Connection ~ 3600 1150
Wire Wire Line
	3600 1150 3600 1300
$Comp
L Device:C_Small C6
U 1 1 60FF3A04
P 4100 950
F 0 "C6" H 4192 996 50  0000 L CNN
F 1 "0.1uF" H 4192 905 50  0000 L CNN
F 2 "" H 4100 950 50  0001 C CNN
F 3 "~" H 4100 950 50  0001 C CNN
	1    4100 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 850  3600 750 
Wire Wire Line
	3600 750  4100 750 
Wire Wire Line
	4100 750  4100 850 
Connection ~ 4100 750 
$Comp
L power:GND #PWR0107
U 1 1 60FF3A13
P 4100 1100
F 0 "#PWR0107" H 4100 850 50  0001 C CNN
F 1 "GND" H 3950 1000 50  0000 C CNN
F 2 "" H 4100 1100 50  0001 C CNN
F 3 "" H 4100 1100 50  0001 C CNN
	1    4100 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 1050 4100 1100
$Comp
L Device:C_Small C4
U 1 1 60FF63BE
P 2850 2050
F 0 "C4" V 2621 2050 50  0000 C CNN
F 1 ".001uF" V 2712 2050 50  0000 C CNN
F 2 "" H 2850 2050 50  0001 C CNN
F 3 "~" H 2850 2050 50  0001 C CNN
	1    2850 2050
	0    1    1    0   
$EndComp
Wire Wire Line
	2100 1700 2600 1700
Wire Wire Line
	2600 1700 2600 2050
Wire Wire Line
	2600 2050 2750 2050
Wire Wire Line
	2950 2050 3250 2050
Connection ~ 3250 2050
$Comp
L Device:C_Small C1
U 1 1 60FF7BD6
P 1450 2050
F 0 "C1" V 1300 2050 50  0000 C CNN
F 1 ".001uF" V 1600 2050 50  0000 C CNN
F 2 "" H 1450 2050 50  0001 C CNN
F 3 "~" H 1450 2050 50  0001 C CNN
	1    1450 2050
	0    1    1    0   
$EndComp
Wire Wire Line
	1250 2050 1350 2050
Wire Wire Line
	1550 2050 1750 2050
Connection ~ 1750 2050
Wire Wire Line
	2600 750  3600 750 
Connection ~ 3600 750 
Wire Wire Line
	1050 2450 2100 2450
Connection ~ 1050 2450
Wire Wire Line
	1050 2450 1050 2600
Connection ~ 2100 2450
Wire Wire Line
	2100 2450 2100 2600
Wire Wire Line
	2100 2450 3600 2450
Connection ~ 3600 2450
Wire Wire Line
	3600 2450 3600 2600
$Comp
L Device:R_Small R2
U 1 1 60FEB4BF
P 2100 1400
F 0 "R2" H 2159 1446 50  0000 L CNN
F 1 "1K0" H 2159 1355 50  0000 L CNN
F 2 "" H 2100 1400 50  0001 C CNN
F 3 "~" H 2100 1400 50  0001 C CNN
	1    2100 1400
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J2
U 1 1 60FFDD5B
P 4800 2450
F 0 "J2" H 4772 2332 50  0000 R CNN
F 1 "Signal Out" H 4772 2423 50  0000 R CNN
F 2 "" H 4800 2450 50  0001 C CNN
F 3 "~" H 4800 2450 50  0001 C CNN
	1    4800 2450
	-1   0    0    1   
$EndComp
Wire Wire Line
	3600 1700 4200 1700
$Comp
L Connector:Conn_01x02_Male J3
U 1 1 61000A0E
P 4800 850
F 0 "J3" H 4772 732 50  0000 R CNN
F 1 "Power" H 4772 823 50  0000 R CNN
F 2 "" H 4800 850 50  0001 C CNN
F 3 "~" H 4800 850 50  0001 C CNN
	1    4800 850 
	-1   0    0    1   
$EndComp
Wire Wire Line
	4100 750  4600 750 
$Comp
L power:GND #PWR0108
U 1 1 610051F4
P 4550 950
F 0 "#PWR0108" H 4550 700 50  0001 C CNN
F 1 "GND" H 4700 850 50  0000 C CNN
F 2 "" H 4550 950 50  0001 C CNN
F 3 "" H 4550 950 50  0001 C CNN
	1    4550 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 850  4550 850 
Wire Wire Line
	4550 850  4550 950 
Text Notes 4450 750  0    50   ~ 0
+5v
Text Notes 700  700  0    50   ~ 0
Preamplifier
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 6100B0E4
P 950 7400
F 0 "#FLG0101" H 950 7475 50  0001 C CNN
F 1 "PWR_FLAG" H 950 7573 50  0000 C CNN
F 2 "" H 950 7400 50  0001 C CNN
F 3 "~" H 950 7400 50  0001 C CNN
	1    950  7400
	1    0    0    -1  
$EndComp
Wire Wire Line
	950  7400 950  7500
$Comp
L power:GND #PWR0109
U 1 1 6100C842
P 950 7500
F 0 "#PWR0109" H 950 7250 50  0001 C CNN
F 1 "GND" H 955 7327 50  0000 C CNN
F 2 "" H 950 7500 50  0001 C CNN
F 3 "" H 950 7500 50  0001 C CNN
	1    950  7500
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 2350 4600 2350
Wire Wire Line
	4200 1700 4200 2350
Wire Wire Line
	3600 2450 4600 2450
Wire Notes Line
	650  600  5350 600 
Wire Notes Line
	650  2900 5350 2900
Wire Notes Line
	650  600  650  2900
Wire Notes Line
	5350 600  5350 2900
$Comp
L 74xx:74HC74 U1
U 1 1 610347ED
P 2150 3750
F 0 "U1" H 1950 4050 50  0000 C CNN
F 1 "74HC74" H 2150 4300 50  0000 C CNN
F 2 "" H 2150 3750 50  0001 C CNN
F 3 "74xx/74hc_hct74.pdf" H 2150 3750 50  0001 C CNN
	1    2150 3750
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC74 U1
U 2 1 61035B59
P 3200 3750
F 0 "U1" H 3000 4050 50  0000 C CNN
F 1 "74HC74" H 3200 4300 50  0000 C CNN
F 2 "" H 3200 3750 50  0001 C CNN
F 3 "74xx/74hc_hct74.pdf" H 3200 3750 50  0001 C CNN
	2    3200 3750
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC74 U1
U 3 1 610375D3
P 2150 4900
F 0 "U1" H 1900 5350 50  0000 L CNN
F 1 "74HC74" H 2000 4900 50  0000 L CNN
F 2 "" H 2150 4900 50  0001 C CNN
F 3 "74xx/74hc_hct74.pdf" H 2150 4900 50  0001 C CNN
	3    2150 4900
	1    0    0    -1  
$EndComp
NoConn ~ 2150 3450
NoConn ~ 3200 3450
NoConn ~ 2150 4050
NoConn ~ 3200 4050
Wire Wire Line
	1850 3650 1800 3650
Wire Wire Line
	1800 3650 1800 3300
Wire Wire Line
	1800 3300 2500 3300
Wire Wire Line
	2500 3300 2500 3850
Wire Wire Line
	2500 3850 2450 3850
Wire Wire Line
	2900 3650 2850 3650
Wire Wire Line
	2850 3650 2850 3300
Wire Wire Line
	2850 3300 3550 3300
Wire Wire Line
	3550 3300 3550 3850
Wire Wire Line
	3550 3850 3500 3850
Wire Wire Line
	2450 3650 2700 3650
Wire Wire Line
	2700 3650 2700 3750
Wire Wire Line
	2700 3750 2900 3750
$Comp
L power:+5V #PWR0110
U 1 1 6103F8AA
P 2150 4450
F 0 "#PWR0110" H 2150 4300 50  0001 C CNN
F 1 "+5V" H 2165 4623 50  0000 C CNN
F 2 "" H 2150 4450 50  0001 C CNN
F 3 "" H 2150 4450 50  0001 C CNN
	1    2150 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 61041581
P 2150 5350
F 0 "#PWR0111" H 2150 5100 50  0001 C CNN
F 1 "GND" H 2155 5177 50  0000 C CNN
F 2 "" H 2150 5350 50  0001 C CNN
F 3 "" H 2150 5350 50  0001 C CNN
	1    2150 5350
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male J5
U 1 1 61045059
P 4600 3750
F 0 "J5" H 4750 3350 50  0000 R CNN
F 1 "Signal Out" H 4750 3450 50  0000 R CNN
F 2 "" H 4600 3750 50  0001 C CNN
F 3 "~" H 4600 3750 50  0001 C CNN
	1    4600 3750
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Male J4
U 1 1 6104E1C3
P 1250 3750
F 0 "J4" H 1150 3850 50  0000 C CNN
F 1 "Signal In" H 1000 3750 50  0000 C CNN
F 2 "" H 1250 3750 50  0001 C CNN
F 3 "~" H 1250 3750 50  0001 C CNN
	1    1250 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 3750 1850 3750
$Comp
L power:GND #PWR0112
U 1 1 6105215B
P 1550 3950
F 0 "#PWR0112" H 1550 3700 50  0001 C CNN
F 1 "GND" H 1555 3777 50  0000 C CNN
F 2 "" H 1550 3950 50  0001 C CNN
F 3 "" H 1550 3950 50  0001 C CNN
	1    1550 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 3850 1550 3850
Wire Wire Line
	1550 3850 1550 3950
$Comp
L power:GND #PWR0113
U 1 1 61053F43
P 4200 3750
F 0 "#PWR0113" H 4200 3500 50  0001 C CNN
F 1 "GND" V 4205 3622 50  0000 R CNN
F 2 "" H 4200 3750 50  0001 C CNN
F 3 "" H 4200 3750 50  0001 C CNN
	1    4200 3750
	0    1    1    0   
$EndComp
Wire Wire Line
	4200 3750 4400 3750
Wire Wire Line
	3500 3650 4400 3650
Wire Wire Line
	2700 3750 2700 4250
Wire Wire Line
	2700 4250 4250 4250
Wire Wire Line
	4250 4250 4250 3850
Wire Wire Line
	4250 3850 4400 3850
Connection ~ 2700 3750
Wire Wire Line
	2150 4450 2150 4500
Wire Wire Line
	2150 5300 2150 5350
Wire Notes Line
	650  3050 5350 3050
Wire Notes Line
	5350 3050 5350 5650
Wire Notes Line
	5350 5650 650  5650
Wire Notes Line
	650  5650 650  3050
Text Notes 700  3150 0    50   ~ 0
Scaler
Text Notes 4700 3650 0    50   ~ 0
divide by 4
Text Notes 4700 3900 0    50   ~ 0
divide by 2
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 6106E01C
P 1350 7500
F 0 "#FLG0102" H 1350 7575 50  0001 C CNN
F 1 "PWR_FLAG" H 1350 7673 50  0000 C CNN
F 2 "" H 1350 7500 50  0001 C CNN
F 3 "~" H 1350 7500 50  0001 C CNN
	1    1350 7500
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0114
U 1 1 6106E813
P 1350 7400
F 0 "#PWR0114" H 1350 7250 50  0001 C CNN
F 1 "+5V" H 1365 7573 50  0000 C CNN
F 2 "" H 1350 7400 50  0001 C CNN
F 3 "" H 1350 7400 50  0001 C CNN
	1    1350 7400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 7400 1350 7500
$EndSCHEMATC
