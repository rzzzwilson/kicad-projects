# USB-C Breakout

A simple little board to test using the 6pin USB-C SMD sockets I bought.  Once
I'm happy that the footprint is OK and I can use the socket(s), I'll start
using them in projects.
