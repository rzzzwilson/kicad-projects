EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "LQFP48 Breakout Board"
Date "2020-02-22"
Rev "1.0"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LQFP48:LQFP48 U1
U 1 1 5E51C197
P 6050 3900
F 0 "U1" H 6000 3850 50  0000 L CNN
F 1 "LQFP48" H 5900 3950 50  0000 L CNN
F 2 "Housings_QFP:TQFP-48_7x7mm_Pitch0.5mm" H 6100 3900 50  0001 C CNN
F 3 "" H 6100 3900 50  0001 C CNN
	1    6050 3900
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x24_Male J1
U 1 1 5E5283C9
P 3300 3850
F 0 "J1" V 3135 3778 50  0000 C CNN
F 1 "Conn_01x24_Male" V 3226 3778 50  0000 C CNN
F 2 "LQFP48_Breakout:Header_1x24" H 3300 3850 50  0001 C CNN
F 3 "~" H 3300 3850 50  0001 C CNN
	1    3300 3850
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x24_Male J2
U 1 1 5E5302A3
P 8550 3950
F 0 "J2" V 8385 3878 50  0000 C CNN
F 1 "Conn_01x24_Male" V 8476 3878 50  0000 C CNN
F 2 "LQFP48_Breakout:Header_1x24" H 8550 3950 50  0001 C CNN
F 3 "~" H 8550 3950 50  0001 C CNN
	1    8550 3950
	-1   0    0    1   
$EndComp
Text GLabel 5150 3350 0    50   Input ~ 0
P1
Text GLabel 5150 3450 0    50   Input ~ 0
P2
Text GLabel 5150 3550 0    50   Input ~ 0
P3
Text GLabel 5150 3650 0    50   Input ~ 0
P4
Text GLabel 5150 3750 0    50   Input ~ 0
P5
Text GLabel 5150 3850 0    50   Input ~ 0
P6
Text GLabel 5150 3950 0    50   Input ~ 0
P7
Text GLabel 5150 4050 0    50   Input ~ 0
P8
Text GLabel 5150 4150 0    50   Input ~ 0
P9
Text GLabel 5150 4250 0    50   Input ~ 0
P10
Text GLabel 5150 4350 0    50   Input ~ 0
P11
Text GLabel 5150 4450 0    50   Input ~ 0
P12
Text GLabel 5500 4800 3    50   Input ~ 0
P13
Text GLabel 5600 4800 3    50   Input ~ 0
P14
Text GLabel 5700 4800 3    50   Input ~ 0
P15
Text GLabel 5800 4800 3    50   Input ~ 0
P16
Text GLabel 5900 4800 3    50   Input ~ 0
P17
Text GLabel 6000 4800 3    50   Input ~ 0
P18
Text GLabel 6100 4800 3    50   Input ~ 0
P19
Text GLabel 6200 4800 3    50   Input ~ 0
P20
Text GLabel 6300 4800 3    50   Input ~ 0
P21
Text GLabel 6400 4800 3    50   Input ~ 0
P22
Text GLabel 6500 4800 3    50   Input ~ 0
P23
Text GLabel 6600 4800 3    50   Input ~ 0
P24
Text GLabel 6950 4450 2    50   Input ~ 0
P25
Text GLabel 6950 4350 2    50   Input ~ 0
P26
Text GLabel 6950 4250 2    50   Input ~ 0
P27
Text GLabel 6950 4150 2    50   Input ~ 0
P28
Text GLabel 6950 4050 2    50   Input ~ 0
P29
Text GLabel 6950 3950 2    50   Input ~ 0
P30
Text GLabel 6950 3850 2    50   Input ~ 0
P31
Text GLabel 6950 3750 2    50   Input ~ 0
P32
Text GLabel 6950 3650 2    50   Input ~ 0
P33
Text GLabel 6950 3550 2    50   Input ~ 0
P34
Text GLabel 6950 3450 2    50   Input ~ 0
P35
Text GLabel 6950 3350 2    50   Input ~ 0
P36
Text GLabel 6600 3000 1    50   Input ~ 0
P37
Text GLabel 6500 3000 1    50   Input ~ 0
P38
Text GLabel 6400 3000 1    50   Input ~ 0
P39
Text GLabel 6300 3000 1    50   Input ~ 0
P40
Text GLabel 6200 3000 1    50   Input ~ 0
P41
Text GLabel 6100 3000 1    50   Input ~ 0
P42
Text GLabel 6000 3000 1    50   Input ~ 0
P43
Text GLabel 5900 3000 1    50   Input ~ 0
P44
Text GLabel 5800 3000 1    50   Input ~ 0
P45
Text GLabel 5700 3000 1    50   Input ~ 0
P46
Text GLabel 5600 3000 1    50   Input ~ 0
471
Text GLabel 5500 3000 1    50   Input ~ 0
P48
Text GLabel 3600 2750 2    50   Input ~ 0
P1
Text GLabel 3600 2850 2    50   Input ~ 0
P2
Text GLabel 3600 2950 2    50   Input ~ 0
P3
Text GLabel 3600 3050 2    50   Input ~ 0
P4
Text GLabel 3600 3150 2    50   Input ~ 0
P5
Text GLabel 3600 3250 2    50   Input ~ 0
P6
Text GLabel 3600 3350 2    50   Input ~ 0
P7
Text GLabel 3600 3450 2    50   Input ~ 0
P8
Text GLabel 3600 3550 2    50   Input ~ 0
P9
Text GLabel 3600 3650 2    50   Input ~ 0
P10
Text GLabel 3600 3750 2    50   Input ~ 0
P11
Text GLabel 3600 3850 2    50   Input ~ 0
P12
Text GLabel 3600 3950 2    50   Input ~ 0
P13
Text GLabel 3600 4050 2    50   Input ~ 0
P14
Text GLabel 3600 4150 2    50   Input ~ 0
P15
Text GLabel 3600 4250 2    50   Input ~ 0
P16
Text GLabel 3600 4350 2    50   Input ~ 0
P17
Text GLabel 3600 4450 2    50   Input ~ 0
P18
Text GLabel 3600 4550 2    50   Input ~ 0
P19
Text GLabel 3600 4650 2    50   Input ~ 0
P20
Text GLabel 3600 4750 2    50   Input ~ 0
P21
Text GLabel 3600 4850 2    50   Input ~ 0
P22
Text GLabel 3600 4950 2    50   Input ~ 0
P23
Text GLabel 3600 5050 2    50   Input ~ 0
P24
Wire Wire Line
	3500 2750 3600 2750
Wire Wire Line
	3500 2850 3600 2850
Wire Wire Line
	3500 2950 3600 2950
Wire Wire Line
	3500 3050 3600 3050
Wire Wire Line
	3500 3150 3600 3150
Wire Wire Line
	3500 3250 3600 3250
Wire Wire Line
	3500 3350 3600 3350
Wire Wire Line
	3500 3450 3600 3450
Wire Wire Line
	3500 3550 3600 3550
Wire Wire Line
	3500 3650 3600 3650
Wire Wire Line
	3500 3750 3600 3750
Wire Wire Line
	3500 3850 3600 3850
Wire Wire Line
	3500 3950 3600 3950
Wire Wire Line
	3500 4050 3600 4050
Wire Wire Line
	3500 4150 3600 4150
Wire Wire Line
	3500 4250 3600 4250
Wire Wire Line
	3500 4350 3600 4350
Wire Wire Line
	3500 4450 3600 4450
Wire Wire Line
	3500 4550 3600 4550
Wire Wire Line
	3500 4650 3600 4650
Wire Wire Line
	3500 4750 3600 4750
Wire Wire Line
	3500 4850 3600 4850
Wire Wire Line
	3500 4950 3600 4950
Wire Wire Line
	3500 5050 3600 5050
Wire Wire Line
	5150 3350 5250 3350
Wire Wire Line
	5150 3450 5250 3450
Wire Wire Line
	5150 3550 5250 3550
Wire Wire Line
	5150 3650 5250 3650
Wire Wire Line
	5150 3750 5250 3750
Wire Wire Line
	5150 3850 5250 3850
Wire Wire Line
	5150 3950 5250 3950
Wire Wire Line
	5150 4050 5250 4050
Wire Wire Line
	5150 4150 5250 4150
Wire Wire Line
	5150 4250 5250 4250
Wire Wire Line
	5150 4350 5250 4350
Wire Wire Line
	5150 4450 5250 4450
Wire Wire Line
	5500 4700 5500 4800
Wire Wire Line
	5600 4800 5600 4700
Wire Wire Line
	5700 4700 5700 4800
Wire Wire Line
	5800 4800 5800 4700
Wire Wire Line
	5900 4700 5900 4800
Wire Wire Line
	6000 4800 6000 4700
Wire Wire Line
	6100 4700 6100 4800
Wire Wire Line
	6200 4800 6200 4700
Wire Wire Line
	6300 4700 6300 4800
Wire Wire Line
	6400 4800 6400 4700
Wire Wire Line
	6500 4700 6500 4800
Wire Wire Line
	6600 4800 6600 4700
Text GLabel 8250 5050 0    50   Input ~ 0
P25
Text GLabel 8250 4950 0    50   Input ~ 0
P26
Text GLabel 8250 4850 0    50   Input ~ 0
P27
Text GLabel 8250 4750 0    50   Input ~ 0
P28
Text GLabel 8250 4650 0    50   Input ~ 0
P29
Text GLabel 8250 4550 0    50   Input ~ 0
P30
Text GLabel 8250 4450 0    50   Input ~ 0
P31
Text GLabel 8250 4350 0    50   Input ~ 0
P32
Text GLabel 8250 4250 0    50   Input ~ 0
P33
Text GLabel 8250 4150 0    50   Input ~ 0
P34
Text GLabel 8250 4050 0    50   Input ~ 0
P35
Text GLabel 8250 3950 0    50   Input ~ 0
P36
Text GLabel 8250 3850 0    50   Input ~ 0
P37
Text GLabel 8250 3750 0    50   Input ~ 0
P38
Text GLabel 8250 3650 0    50   Input ~ 0
P39
Text GLabel 8250 3550 0    50   Input ~ 0
P40
Text GLabel 8250 3450 0    50   Input ~ 0
P41
Text GLabel 8250 3350 0    50   Input ~ 0
P42
Text GLabel 8250 3250 0    50   Input ~ 0
P43
Text GLabel 8250 3150 0    50   Input ~ 0
P44
Text GLabel 8250 3050 0    50   Input ~ 0
P45
Text GLabel 8250 2950 0    50   Input ~ 0
P46
Text GLabel 8250 2850 0    50   Input ~ 0
471
Text GLabel 8250 2750 0    50   Input ~ 0
P48
Wire Wire Line
	8250 2750 8350 2750
Wire Wire Line
	8350 2850 8250 2850
Wire Wire Line
	8250 2950 8350 2950
Wire Wire Line
	8350 3050 8250 3050
Wire Wire Line
	8250 3150 8350 3150
Wire Wire Line
	8350 3250 8250 3250
Wire Wire Line
	8250 3350 8350 3350
Wire Wire Line
	8350 3450 8250 3450
Wire Wire Line
	8250 3550 8350 3550
Wire Wire Line
	8350 3650 8250 3650
Wire Wire Line
	8250 3750 8350 3750
Wire Wire Line
	8350 3850 8250 3850
Wire Wire Line
	8250 3950 8350 3950
Wire Wire Line
	8350 4050 8250 4050
Wire Wire Line
	8250 4150 8350 4150
Wire Wire Line
	8350 4250 8250 4250
Wire Wire Line
	8250 4350 8350 4350
Wire Wire Line
	8350 4450 8250 4450
Wire Wire Line
	8250 4550 8350 4550
Wire Wire Line
	8350 4650 8250 4650
Wire Wire Line
	8250 4750 8350 4750
Wire Wire Line
	8350 4850 8250 4850
Wire Wire Line
	8250 4950 8350 4950
Wire Wire Line
	8350 5050 8250 5050
Wire Wire Line
	6850 4450 6950 4450
Wire Wire Line
	6950 4350 6850 4350
Wire Wire Line
	6850 4250 6950 4250
Wire Wire Line
	6950 4150 6850 4150
Wire Wire Line
	6850 4050 6950 4050
Wire Wire Line
	6950 3950 6850 3950
Wire Wire Line
	6850 3850 6950 3850
Wire Wire Line
	6950 3750 6850 3750
Wire Wire Line
	6850 3650 6950 3650
Wire Wire Line
	6950 3550 6850 3550
Wire Wire Line
	6850 3450 6950 3450
Wire Wire Line
	6950 3350 6850 3350
Wire Wire Line
	6600 3100 6600 3000
Wire Wire Line
	6500 3000 6500 3100
Wire Wire Line
	6400 3100 6400 3000
Wire Wire Line
	6300 3000 6300 3100
Wire Wire Line
	6200 3100 6200 3000
Wire Wire Line
	6100 3000 6100 3100
Wire Wire Line
	6000 3100 6000 3000
Wire Wire Line
	5900 3000 5900 3100
Wire Wire Line
	5800 3100 5800 3000
Wire Wire Line
	5700 3000 5700 3100
Wire Wire Line
	5600 3100 5600 3000
Wire Wire Line
	5500 3000 5500 3100
$EndSCHEMATC
