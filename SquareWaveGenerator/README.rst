SquareGen
=========

This project is a repackaging of the fast risetime Square Wave generator
that I built from the 
`video by W2AEW on YouTube <https://www.youtube.com/watch?v=9cP6w2odGUc>`_.
I originally put this circuit into an enclosure using the "dead bug" style
and it worked pretty well.  Here's that built circuit:

.. image:: IMG_5211.JPG

And here's the circuit running on the 'scope:

.. image:: DS1Z_QuickPrint3.png

Not bad for a cheap little circuit.  It draws less than 3mA at 5volts.

I have been building circuits on PCB lately using the PCB shops in China with
through-hole components, but now I want to get into surface-mounted components
so I'm using this circuit as a guinea-pig.  Using KiCAD here's the schematic:

.. image:: schematic.png

and the PCB itself.  The board is designed to go into an enclosure the same
as the previous "dead bug" version, but the board goes in diagonally so the
power and signal out connectors have very short runs:

.. image:: Version3.1PCB.png

This is a later version of the PCB than shown below.  This version has a 10uF
bypass capacitor on the Vcc 5volt circuit, which eliminates low-frequency
ringing on the signal.  I just bodged an electrolytic onto the original board
for testing.

I'll put the PCB into the same 25x25x50mm enclosure as the first circuit, so the
PCB measures about 44x23mm.  I got the enclosures 
`from here <https://www.aliexpress.com/wholesale?catId=0&initiative_id=SB_20180825211723&SearchText=50x25x25mm+Extruded+Aluminum>`_.


Status
======

The board has been built and installed into its case.  This is before I added
a 10uF bypass capacitor to the Vcc line:

.. image:: IMG_5487.JPG

When powered and feeding into a 50 ohm load, the output is:

.. image:: DS1Z_QuickPrint1.png

A closer look at the rising edge shows:

.. image:: DS1Z_QuickPrint4.png

This has a risetime of about 1.6ns, which is pretty good for such a simple
circuit.  There is some well-damped ringing after the overshoot.  I do wonder
just what I am measuring though, since the scope/probes should have a risetime
somewhere near 3ns.  Still, I'm happy!
