Bootloader:

    https://www.youtube.com/watch?v=wGbiT6IxGP0
    https://www.youtube.com/watch?v=fD1Bzf2iP_E

Programming blue pill:

    https://github.com/rogerclarkmelbourne/Arduino_STM32/wiki/Programming-an-STM32F103XXX-with-a-generic-%22ST-Link-V2%22-programmer-from-Linux

Installing on Linux:

    https://arduino.stackexchange.com/questions/77426/stm32-st-link-not-working-on-linux