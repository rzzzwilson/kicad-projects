// Code to exercise 1 bit of the core rope memory.

const int excite = 7;
const int sense = 6;

void setup()
{
  Serial.begin(115200);
  delay(2500);
  Serial.println("READY");
  
  pinMode(excite, OUTPUT);
  pinMode(sense, INPUT);
  int b1 = digitalRead(sense);
  Serial.print("Start, b1=");
  Serial.println(b1 ? "HIGH" : "LOW");
}

void loop()
{
  for (int i = 0; i < 100; ++i)
  {
    digitalWrite(excite, HIGH);
//    delay(1);
    digitalWrite(excite, LOW);
//    delay(1);
  }

//  int b1 = digitalRead(sense);
//  Serial.print("b1=");
//  Serial.println(b1 ? "HIGH" : "LOW");
//  delay(500);
}
