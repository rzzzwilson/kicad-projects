Micro Micro Clock
=================

This project arose from a need to create some sort of configurable, flexible
square-wave clock source to make investigating what a Z-80 microcontroller
does with its address/data/control lines easier.  This is something I need to
do to get the Pico-80 project started.

The basic idea is from Ben Eater's series on building your own computer.
I took his ideas for the 8-bit clock:

    https://eater.net/8bit/clock

This design will use a microcontroller to generate the clock signal(s) and
will have:

* control via the rotary encoder (RE)
* a display to show status, frequency, etc
* continuous square-wave output as one option
* a one-shot facility, triggered by the RE pushbutton
* variable frequency control, sub 1Hz up to about 1KHz
* ability to be externally controlled (UART, Serial, ???)

The external control will be able to fully control the clock.  It should also
allow things like sending 6 cycles of a set clock signal and then stopping.
This might be part of a larger testing suite involving a PC and logic analyzer.
