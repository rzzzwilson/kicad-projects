Micro Clock
===========

This project arose from a need to create some sort of configurable, flexible
square-wave clock source to make investigating what a Z-80 microcontroller
does with its address/data/control lines easier.  This is something I need to
do to get the Pico-80 project started.

The basic idea is from Ben Eater's series on building your own computer.
I took his ideas for the 8-bit clock:

    https://eater.net/8bit/clock

Ben's idea will be faithfully recreated but on a PCB.  Later I might use
a microcontroller, rotary encoder and a display to make a smaller version
allowing external control.

Version 2
---------

The later, microcontroller, version will have:

* control via the rotary encoder
* a display to show status
* continuous square-wave output
* a one-shot facility, triggered by the RE pushbutton
* variable frequency control, sub 1Hz up to about 1KHz
* ability to be externally controlled (UART, Serial, ???)

The external control will be able to fully control the clock.  It should also
allow things like sending 6 cycles of a set clock signal and then stopping.
This might be part of a larger testing suite involving a PC and logic analyzer.

The design for this version will  be in subdirectory *micro_micro_clock*.

micro micro clock commands
--------------------------

The clock will have two states, a "configure" state and a "running" state.

| Command         | In state  | Meaning                             |
|-----------------|-----------|-------------------------------------|
| Fxxxx;          | configure | Set clock frequency to xxxx         |
| F;              | configure | Display the current clock frequency |
| Nxx;            | configure | Emit xx number of clock cycles, 0 means continue forever  |
| N;              | configure | Display the configured number of clock cycles   |
| X;              | configure | start executing the configuration   |
| Q;              | running   | Quit operation, return to configure state |
