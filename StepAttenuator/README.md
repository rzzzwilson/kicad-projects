# A Step Attenuator

A simple step attenuator as described in:

https://dl6gl.de/stufenabschwaecher-bis-34-db-fuer-10-euro.html

A copy of that page is in the *resources* directory.


## Basic Design

The circuit was transcribed into KiCAD.  The schematic is:

![schematic](Schematic.png)

Note that some 1206 resistor positions are not populated.  That is shown
as a resistor outline with no resistor value.

The PCB was designed to fit inside a 40x50x200mmm aluminium enclosure:

https://www.aliexpress.com/item/32764606551.html


## Shielding

The basic board has shielding pads designed in.  Thin copper foil will be used
to construct a 2mm high enclosure around each attenuator section.  A jig
will be constructed from 2mm thick laser-cut acrylic to cut and fold the foil
into the correct shape.  The jig is documented in the *shielding/cutting_jig* directory.

## Faceplate

The enclosure has a 39mm wide recess on the front that will be filled with
a PCB, maybe 0.8mm thick, that will have markings in the silkscreen.  That
design will be in the *faceplate* directory.
