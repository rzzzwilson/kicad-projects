# SoftSwitch

This is an experimental little "softswitch" that can turn a microcontroller
on/off.

The idea is that pressing the switch allows power to be connected to the
microcontroller.  When the microcontroller starts the first thing it does
is to set the pin connected to the HOLD pad HIGH, thereby latching the power
on.  Another pin connected to the SENSE pad allows the microcontroller
to tell when the button is pushed again.  The microcontroller can then 
set the HOLD pin LOW which turns off the power.

When the microcontroller sees a button push it might save some state to
EEPROM before turning the power off which allows the microcontroller
to restore that state on next power up.  Alternatively, it's possible
the controller may decide that now is a bad time to turn the power
off and decline to immediately turn the power off.

Unfortunately I can't find the original source of this circuit.

## Connections

The "Vin 7-12v" and GND pads is where external power is connected.

The HOLD and SENSE pads are connected to the microcontroller GPIO pins.

The "switched Vout 7-12v" and GND pads feed the switched power to 
external circuitry.

The "switched Vout 5v" and GND pads feed a switched 5 volt power to
external circuitry.  This 5 volts is regulated from the Vin voltage, so
that voltage must be a couple of volts over 5 volts.

## More development

This works reliably, as far as I can tell.

Try to find a circuit that uses only 1 pin for HOLD and SENSE.
