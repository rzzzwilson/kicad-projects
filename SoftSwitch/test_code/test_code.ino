//
// Exercise the SoftSwitch board.
// Connect these before testing:
//   Uno  -> SoftSwitch
//   ------------------
//   GND     GND
//   5v      Vout
//   pin6    SENSE
//   pin7    HOLD
//
// and power:
//   PS   -> SoftSwitch
//   ------------------
//   8volt   Vin
//   GND     GND
//

#include <ezButton.h>


const byte HoldPin = 7;
const byte SensePin = 6;

unsigned long next_flash = 0;
unsigned long flash_interval = 500;

ezButton button(SensePin);


void flash_led(int n)
{
  for (int i = 0; i < n; ++i)
  {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(75);
    digitalWrite(LED_BUILTIN, LOW);
    delay(75);
  }
}

//int read_button(int pin)
//{
//  uint16_t states = 0;
//  
//  while (true)
//  {
//    states = (states << 1) + (digitalRead(pin) & 1);
//
//    if (states == 0xffff || states == 0x0000)
//    {
//      return states == 0xffff ? HIGH : LOW;
//    }
//
//    delay(10);
//  }
//}

void shutdown()
{
  flash_led(5);
  digitalWrite(HoldPin, LOW);
  while (1)   // wait until power is removed
  {
  }
}

void setup()
{
  // set the power HOLD pin high immediately
  // this maintains power from SoftSwitch
  pinMode(HoldPin, OUTPUT);
  digitalWrite(HoldPin, HIGH);
  
  pinMode(LED_BUILTIN, OUTPUT);

  // prepare button and wait until button NOT down
  button.setDebounceTime(50);
  delay(20);
  button.loop();
  delay(20);
//  while (button.getState() == LOW)
  while (digitalRead(SensePin) == LOW)
  {
    button.loop();
    delay(10);
  }

//  // set SENSE pin as INPUT_PULLUP
//  // wait until the SENSE button is released
//  pinMode(SensePin, INPUT_PULLUP);
//  while (read_button(SensePin) == LOW)
//  {
//  }

  // show we have power on
  flash_led(5);
}

void loop()
{
  button.loop();
  
  // poll the button, shutdown if pushed
//  int state = read_button(SensePin);
//  if (state == LOW)
  if (button.isPressed())
  {
    shutdown();
  }
  
  // flash the builtin LED once every whenever
  if (millis() - next_flash >= flash_interval)
  {
    next_flash += flash_interval;
    flash_led(1);
  }
}
