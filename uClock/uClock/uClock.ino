int OUT_PIN = 9;
const long PERIOD = 1;
unsigned long previousMillis = 0;
bool pin_state = false;


void setup()
{
  Serial.begin(115200);
  delay(2500);
  Serial.println("READY");
  
  pinMode(OUT_PIN, OUTPUT);
}

void loop()
{
  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= PERIOD)
  {
    pin_state = !pin_state;
    digitalWrite(OUT_PIN, pin_state);
    previousMillis = currentMillis;
  }

}
