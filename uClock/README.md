# uClock

A microcontroller-based experimental clock to be used to
evaluate the pico80 and similar systems.

## Requirements

* Microcontroller based, AtMega32U4 because of USB connection
* Clock frequency somewhere in the range of sub 1Hz up to 10KHz
* Command driven from Serial, see below
* Hardware button to start/stop execution of programmed parameters

## Serial Commands

| Command | Meaning                                              |
|---------|------------------------------------------------------|
| Fnnn;   | Frequency of clock, nnn in range [1,10000]           |
| Cnn;    | Number of clock cycles to emit, 0 means run forever  |
| X;      | Execute the programmed parameters                    |

If the uClock is executing, any serial input terminates the execution.
