Kappa
=====

An AtMega32U4 microcontroller using the QFN-44 package and running
at 3.3 volts, with provision for a LiPo 1S battery.

Inspired, a bit, by the "Adafruit Feather 32u4".  The pinout follows
Arduino Micro pinout, mostly.

The name of the board will be:

    κappa

The production schematic is *Kappa/Kappa_1.1_Schematic.pdf*:

.. image:: Kappa_1.1_Schematic.png

Status
======

2022 Feb 15 - boards out for manufacture.
