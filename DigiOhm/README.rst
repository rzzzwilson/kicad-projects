What is this?
=============

*DigiOhm* is a small device used to measure resistance.  It is an implementation
of an idea described in the web pages in the *misc* directory.

Design
------


Calibration
-----------

There are ?? uncertain areas in the design that must be calibrated:

Command Language
----------------

The commands for the serial USB connection are:

+------------+----------------------------------------------+
| Command    | Meaning                                      |
+============+==============================================+
| H;         | write help text to the console               |
+------------+----------------------------------------------+
| ID;        | return a device identifier string            |
+------------+----------------------------------------------+
| C?;        | get calibration for resistor ?               |
+------------+----------------------------------------------+
| C?xxxx;    | set calibration for resistor ?               |
+------------+----------------------------------------------+
| R;         | get measured resistance                      |
+------------+----------------------------------------------+

Schematic
---------

.. image:: DigiOhm_1.0_Schematic.png

PCB
---

.. image:: DigiOhm_1.0_PCB.png

Software
--------

Arduino code to run the DigiOhm device is in the *DigiOhm* directory.

Python software to interface with DigiVolt will be held in the *python*
subdirectory.
