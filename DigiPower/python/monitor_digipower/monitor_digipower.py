#!/usr/bin/env python3

"""
A program to measure values from a DigiPower device.

Usage:  monitor_digipower [-h] [-i <id>] <data file>

where -h           prints this help, and
      -i           sets the documentation line to <id>
      <data file>  is the name of the data file to be written
"""

import os
import sys
import time
import datetime
import getopt
import serial
import usb
import traceback

import digithing
import logger
log = logger.Log('measure_digipower.log', logger.Log.DEBUG)


# delay time (seconds) between samples
SampleTime = 1.0

NumSamples = 10


def measure_DigiPower(path, dpow_id, dpw):
    """Make measurements on the DigiPower device.

    path    path to data file to write
    dpow_id  string identifying the test
    dpw     the DigiPower device to use
    """

    log(f"measure_DigiPower: opening file '{path}' for writing")
    with open(path, 'w') as fd:
        fd.write(f'{dpow_id}\n')
        print('Monitoring DigiPower, press ^C to exit.')

#        latest = []

        while True:
            try:
                # get current timestamp
                timestamp = datetime.datetime.utcnow().isoformat()
    
                # read the power value from the DigiPower device
                power = dpw.do_cmd('p;').strip()
                power = float(power)

#                # perform running average
#                latest.append(power)
#                latest = latest[-NumSamples:]
#                power = sum(latest) / len(latest)
    
                fd.write(f'{timestamp},{power:.2f}\n')
                fd.flush()
    
                time.sleep(SampleTime)
            except KeyboardInterrupt:
                print('\nTerminated')
                return

def find_required_devices():
    """Find and return the required devices."""

    print('Looking for DigiPower device ... ', end='')
    sys.stdout.flush()
    dpw = None
    try:
        dpw = digithing.DigiThing('DigiPower', ignore=[])
    except digithing.DigiThingError:
        print('No DigiPower devices found')
    else:
        print(f"found: {dpw.do_cmd('id;').strip()}")
    
    if dpw is None:
        raise Exception("Can't find a DigiPower device!?")

    return dpw

def usage(msg=None):
    """Print the module docstring plus the optional message."""

    if msg:
        print('-' * 60)
        print(msg)
        print('-' * 60, '\n')
    print(__doc__)

def excepthook(type, value, tback):
    """Capture any unhandled exceptions."""

    msg = '\n' + '=' * 80
    msg += '\nUncaught exception:\n'
    msg += ''.join(traceback.format_exception(type, value, tback))
    msg += '=' * 80 + '\n'
    log(msg)
    print(msg)

# plug our handler into the python system
sys.excepthook = excepthook

# check the command line args
try:
    opts, args = getopt.gnu_getopt(sys.argv, "hi:", ["help", "id="])
except getopt.GetoptError:
    usage('Bad option')
    sys.exit(10)

dpow_id = 'DigiPower'

for opt, arg in opts:
    if opt in ("-h", "--help"):
        usage()
        sys.exit(0)
    elif opt in ("-i", "--id"):
        dpow_id = arg

if len(args) != 2:
    usage()
    sys.exit(10)
    
filename = args[1]
if os.path.exists(filename):
    print(f"Sorry, file '{filename}' exists - won't overwrite")
    sys.exit(10)

# check we have the required devices online
dpw = find_required_devices()

# start the measurements, save data to given file
measure_DigiPower(filename, dpow_id, dpw)
