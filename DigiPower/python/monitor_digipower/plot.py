"""
Usage: plot.py <datafile>
"""

import sys
import matplotlib.pyplot as plt


def usage(msg=None):
    if msg:
        print('*' * 60)
        print(msg)
        print('*' * 60)
    print(__doc__)

# get name of the data file to plot
if len(sys.argv) != 2:
    usage()
    sys.exit(10)

data_file = sys.argv[1]

# plot data in the file
x_data = []
y_data = []
with open(data_file, 'r') as fd:
    id_str = fd.readline()   # skip ID line
    for (index, line) in enumerate(fd):
        (dtime, power) = line.split(',')
        power = float(power)
        x_data.append(index)
        y_data.append(power)

plt.plot(x_data, y_data, label='power')
plt.ylim(-65.0, +25.0)
plt.xlabel('time')
plt.ylabel('DigiPower dBm')
plt.title(f'dBm/time - {id_str}')
plt.savefig(f'{data_file}.png')
plt.show()
