The **monitor_digipower** directory holds code to monitor and plot the 
values read from the DigiPower instrument.
