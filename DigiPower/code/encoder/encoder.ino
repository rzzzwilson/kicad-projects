/*
 * Code to test operation of the rotary encoder.
 * 
 * Both the quadrature switches and the pushbutton switch.
 */


// ACTIVE_LOW is non-zero if external pins pulled down to be active
#define ACTIVE_LOW    1

// the builtin LED
const int led = 12;         // D6 - builtin LED

// define microcontroller pins connected to the rotary encoder
const int re_pinPush = 0;   // D2 - encoder pushbutton pin - 0 means DOWN
const int re_pinA = 3;      // D0 - encoder A pin
const int re_pinB = 2;      // D1 - encoder B pin

// define the VFOevents
enum Event
{
  vfo_None,       // no event, used sometimes
  vfo_RLeft,      // left rotate
  vfo_RRight,     // right rotate
  vfo_DnRLeft,    // left rotate, depressed
  vfo_DnRRight,   // right rotate, depressed
  vfo_Click,      // short click
  vfo_LongClick,  // long click
  vfo_DClick,     // double short click
};

// define the length of the event queue
const int EventQueueLength = 10;

// time when click becomes a "long click" (milliseconds)
// the delay is configurable in the UI
const int MinLongClickTime = 100;     // min configurable time
const int MaxLongClickTime = 1000;    // max configurable time
const int DefaultLongClickTime = 500; // default time

// time when click becomes a "double click" (milliseconds)
// the delay is configurable in the UI
const int MinDClickTime = 100;        // min configurable time
const int MaxDClickTime = 1000;       // max configurable time
const int DefaultDClickTime = 300;    // default time

typedef byte VFOEvent;

//----------------------------------------
// Flash the builtin LED.
//----------------------------------------

void flash(void)
{
  digitalWrite(led, HIGH);
  delay(2);
  digitalWrite(led, LOW);
  delay(5);
}

//----------------------------------------
// Convert an event number to a display string.
//----------------------------------------

const char *event2display(VFOEvent event)
{
  switch (event)
  {
    case vfo_None:      return "vfo_None";
    case vfo_RLeft:     return "vfo_RLeft";
    case vfo_RRight:    return "vfo_RRight";
    case vfo_DnRLeft:   return "vfo_DnRLeft";
    case vfo_DnRRight:  return "vfo_DnRRight";
    case vfo_Click:     return "vfo_Click";
    case vfo_LongClick: return "vfo_LongClick";
    case vfo_DClick:    return "vfo_DClick";
  }
  
  return "UNKNOWN EVENT";
}

//----------------------------------------
// Abort the program.
// Tries to tell the world what went wrong, then just loops.
//     msg  address of error string
// Only first NumRows*NumCols chars of message is displayed on LCD.
//----------------------------------------

void DV_abort(const char *msg)
{
  // print error on console (maybe)
  Serial.print("message=");
  Serial.println(msg);
  Serial.println("Microcontroller is paused!");

  // wait here for a bit
  while (1)
    ;
}

//##############################################################################
// The system event queue.
// Implemented as a circular buffer.
// Since the RE code that pushes to the queue is event-driven, we must be
// careful to disable/enable interrupts at the appropriate places.
//##############################################################################

// the queue itself
VFOEvent event_queue[EventQueueLength];

// queue pointers
int queue_fore = 0;   // points at next event to be popped
int queue_aft = 0;    // points at next free slot for a pushed event

//----------------------------------------
// Push an event onto the event queue.
//     event  number of the event to push
// If queue is full, abort!
//
// This routine is called only from interrupt code, so needs no protection.
//----------------------------------------

void event_push(VFOEvent event)
{
  // put new event into next empty slot
  event_queue[queue_fore] = event;

  // move fore ptr one slot up, wraparound if necessary
  ++queue_fore;
  if (queue_fore >= EventQueueLength)
    queue_fore = 0;

  // if queue full, abort
  if (queue_aft == queue_fore)
  {
      event_dump_queue("ERROR: event queue full!");
      DV_abort("Event queue full");
  }
}

//----------------------------------------
// Pop next event from the queue.
//
// Returns vfo_None if queue is empty.
//----------------------------------------

VFOEvent event_pop(void)
{
  // Must protect from RE code while fiddling with queue
  noInterrupts();

  // if queue empty, return None event
  if (queue_fore == queue_aft)
  {
    interrupts();
    return vfo_None;
  }

  // get next event
  VFOEvent event = event_queue[queue_aft];

  // move aft pointer up one slot, wrap if necessary
  ++queue_aft;
  if (queue_aft >= EventQueueLength)
    queue_aft = 0;

  interrupts();

  return event;
}

//----------------------------------------
// Returns the number of events in the queue.
//----------------------------------------

int event_pending(void)
{
  // Must protect from RE code fiddling with queue
  noInterrupts();

  // get distance between fore and aft pointers
  int result = queue_fore - queue_aft;

  // handle case when events wrap around
  if (result < 0)
    result += EventQueueLength;

  interrupts();

  return result;
}

//----------------------------------------
// Clear out any events in the queue.
//----------------------------------------

void event_flush(void)
{
  // Must protect from RE code fiddling with queue
  noInterrupts();

  queue_fore = 0;
  queue_aft = 0;

  interrupts();
}

//----------------------------------------
// Dump the queue contents to the console.
//     msg  address of message to show
// Debug code.
//----------------------------------------

void event_dump_queue(const char *msg)
{
  // Must protect from RE code fiddling with queue
  noInterrupts();

  Serial.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
  Serial.print("Queue: ");
  Serial.println(msg);
  for (int i = 0; i < EventQueueLength; ++i)
  {
    VFOEvent event = event_queue[i];

    Serial.print("  ");
    Serial.print(i);
    Serial.print(" -> ");
    Serial.println(event2display(event));
  }
  if (event_pending() == 0)
  {
    Serial.print("Queue length=0 (or ");
    Serial.print(EventQueueLength);
    Serial.println(")");
  }
  else
  {
    Serial.print("Queue length=");
    Serial.println(EventQueueLength);
  }
  
  Serial.print("queue_aft=");
  Serial.print(queue_aft);
  Serial.print(", queue_fore=");
  Serial.println(queue_fore);
  Serial.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

  interrupts();
}


//##############################################################################
// Interrupt driven rotary encoder interface.  From code by Simon Merrett.
// Based on insight from Oleg Mazurov, Nick Gammon, rt, Steve Spence.
//##############################################################################

// internal variables
unsigned int ReLongClickTime = DefaultLongClickTime;
unsigned int ReDClickTime = DefaultDClickTime;

bool re_rotation = false;            // true if rotation occurred while knob down
bool re_down = 0;                    // non-zero while knob is down
unsigned long re_down_time = 0;      // milliseconds when knob is pressed down
unsigned long last_click = 0;        // time when last single click was found

// expecting rising edge on pinA - at detent
volatile byte aFlag = 0;

// expecting rising edge on pinA - at detent
volatile byte bFlag = 0;

//----------------------------------------
// Initialize the rotary encoder stuff.
//
// Return 'true' if button was down during setup.
//----------------------------------------

bool re_setup(void)
{
  // set RE data pins as inputs
  pinMode(re_pinA, INPUT_PULLUP);
  pinMode(re_pinB, INPUT_PULLUP);
  pinMode(re_pinPush, INPUT_PULLUP);

  // read the pushbutton here, return later
#if ACTIVE_LOW
  int pb_down = ! digitalRead(re_pinPush);
#else
  int pb_down = digitalRead(re_pinPush);
#endif

  // attach pins to ISR on falling edge only on RE pins
#if ACTIVE_LOW
  attachInterrupt(digitalPinToInterrupt(re_pinA), pinA_isr, FALLING);
  attachInterrupt(digitalPinToInterrupt(re_pinB), pinB_isr, FALLING);
#else
  attachInterrupt(digitalPinToInterrupt(re_pinA), pinA_isr, RISING);
  attachInterrupt(digitalPinToInterrupt(re_pinB), pinB_isr, RISING);
#endif
  
  // detect any change on pushbutton pin
  attachInterrupt(digitalPinToInterrupt(re_pinPush), pinPush_isr, CHANGE);

  // flush any events so far
  event_flush();
  
  // look at RE push button, if DOWN this function returns 'true'
  return pb_down;
}

//----------------------------------------
// Handler for pushbutton interrupts (UP or DOWN).
//----------------------------------------

void pinPush_isr(void)
{
  // sample the pin value
#if ACTIVE_LOW
  re_down = ! digitalRead(re_pinPush);
#else
  re_down = digitalRead(re_pinPush);
#endif

  if (re_down)
  {
    // button pushed down
    re_rotation = false;      // no rotation while down so far
    re_down_time = millis();  // note time we went down
  }
  else
  {
    // button released, check if rotation, UP event if not
    if (! re_rotation)
    {
      unsigned long last_up_time = millis();
      unsigned int push_time = last_up_time - re_down_time;

      if (push_time < ReLongClickTime)
      {
        // check to see if we have a single click very recently
        if (last_click != 0)
        {
          // yes, did have single click before this release
          unsigned long dclick_delta = last_up_time - last_click;

          // if short time since last click, issue double-click event
          if (dclick_delta <= ReDClickTime)
          {
            event_push(vfo_DClick);
            last_click = 0;
          }
          else
          {
            event_push(vfo_Click);
            last_click = last_up_time;    // single-click, prepare for possible double
          }
        }
        else
        {
          // no, this is an isolated release
          event_push(vfo_Click);
          last_click = last_up_time;    // single-click, prepare for possible double
        }
      }
      else
      {
        event_push(vfo_LongClick);
      }
    }
  }
}

//----------------------------------------
// Handler for pinA interrupts.
//----------------------------------------

void pinA_isr(void)
{
  // sample the pin values
#if ACTIVE_LOW
  byte pin_A = digitalRead(re_pinA);
  byte pin_B = digitalRead(re_pinB);
#else
  byte pin_A = ! digitalRead(re_pinA);
  byte pin_B = ! digitalRead(re_pinB);
#endif

  if (! pin_A && ! pin_B && aFlag)
  { // check that we have both pins at detent (LOW)
    // and that we are expecting detent on this pin's rising edge
    if (re_down)
    {
      event_push(vfo_DnRLeft);
      re_rotation = true;
    }
    else
    {
      event_push(vfo_RLeft);
    }
    bFlag = 0; //reset flags for the next turn
    aFlag = 0; //reset flags for the next turn
  }
  else if (! pin_A && pin_B)
  {
    // show we're expecting pinB to signal the transition to detent from free rotation
    bFlag = 1;
  }
}

//----------------------------------------
// Handler for pinB interrupts.
//----------------------------------------

void pinB_isr(void)
{
  // sample the pin values
#if ACTIVE_LOW
  byte pin_A = digitalRead(re_pinA);
  byte pin_B = digitalRead(re_pinB);
#else
  byte pin_A = ! digitalRead(re_pinA);
  byte pin_B = ! digitalRead(re_pinB);
#endif

  if (! pin_A && ! pin_B && bFlag)
  { // check that we have both pins at detent (ACTIVE) and 
    // that we are expecting detent on this pin's rising edge
    if (re_down)
    {
      event_push(vfo_DnRRight);
      re_rotation = true;
    }
    else
    {
      event_push(vfo_RRight);
    }
    bFlag = 0; //reset flags for the next turn
    aFlag = 0; //reset flags for the next turn
  }
  else if (pin_A && ! pin_B)
  {
    // show we're expecting pinA to signal the transition to detent from free rotation
    aFlag = 1;
  }
}


void setup(void)
{
  // initialize the serial console
  Serial.begin(115200);
  while (! Serial) // wait for Serial to go live (Leonardo only)
    ;

  // initialize the LED pin
  pinMode(led, OUTPUT);
  
  // set up the rotary encoder
  // reset a few things if RE button down during powerup
  if (re_setup())
  {
    Serial.println("Resetting various things");
    Serial.println("Click the RE button to continue...");

    // flush any events in the queue, then wait for vfo_Click
    event_flush();
    while (event_pop() != vfo_Click)
      ;
  }
  
  Serial.println("Ready!");
}

//----------------------------------------
// Handle events from the rotary encoder.
//----------------------------------------

void handle_RE_events(void)
{
  while (event_pending() > 0)
  {
    // get next event and handle it
    VFOEvent event = event_pop();

    Serial.print(millis());
    Serial.print(": handle_RE_events: handling ");
    Serial.println(event2display(event));
  }
}

//----------------------------------------
// Standard Arduino loop() function.
//----------------------------------------

void loop(void)
{
  // handle all events in the queue
//  handle_RE_events();
  
  while (event_pending() > 0)
  {
    VFOEvent event = event_pop();

    Serial.print(millis());
    Serial.print(": handle_RE_events: handling ");
    Serial.println(event2display(event));

    flash();
  }
}
