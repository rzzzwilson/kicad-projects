/*
 * Firmware for the DigiPower hardware.
 * https://gitlab.com/rzzzwilson/kicad-projects/-/tree/master/DigiPower
 *
 * Must modify /Applications/Arduino.app/Contents/Java/hardware/arduino/avr/boards.txt
 * to include this line "leonardo.build.usb_product="DigiPower" before uploading.  This
 * is so the enumeration of USB devices will see "DigiPower".
 */

#include <LiquidCrystal.h>
#include <EEPROM.h>
#include <math.h>
#include <avr/wdt.h>   // for the soft-boot code

// set values for whatever voltage reference we are using
#define VREF_INTERNAL_2_56    0
#define VREF_EXTERNAL_2_56    1
#define VREF_EXTERNAL_4_096   2

// REFERENCE: the voltage reference we are using
#define REFERENCE   VREF_EXTERNAL_2_56     // 2.560v external reference

// program name
#define PRODUCT_NAME  "DigiPower"

// version of the software
#define VERSION       "1.14"

// callsign of author
#define CALLSIGN      "ac3dn"

//-----
// Some convenience typedefs
//-----

typedef unsigned long ULONG;
typedef unsigned int UINT;

//-----
// Hardware configuration
//-----

// define the pins connecting to the 1602 LCD
const int LCD_RS = 8;       // B4 on Iota
const int LCD_RW = 9;       // B5 on Iota
const int LCD_E = 10;       // B6 on Iota
const int LCD_D4 = 14;      // B3 on Iota
const int LCD_D5 = 17;      // B0 on Iota
const int LCD_D6 = 15;      // B1 on Iota
const int LCD_D7 = 16;      // B2 on Iota
const int LCD_V0 = 5;       // C6 on Iota
const int LCD_K = 13;       // C7 on Iota

// define microcontroller pins connected to the rotary encoder
// just recode this section after connecting RE any old way
// may need to switch "re_pinA" and "re_pinB" to get correct operation
const int re_pinBtn = 0;    // D2 on Iota - encoder pushbutton pin
const int re_pinA = 3;      // D0 on Iota - encoder A pin
const int re_pinB = 2;      // D1 on Iota - encoder B pin

// define the ADC pin that reads the voltage from the AD8307
const int AD8307_pin = 23;  // F0 on Iota - voltage from AD8307

//-----
// 1602 characters and definitions
//-----

// LCD display constants
const int NumCols = 16;
const int NumRows = 2;
const char * BlankRow = "                ";

// data for CGRAM definable characters
const uint8_t in_use_char[8] = {0x10,0x18,0x1c,0x1e,0x1c,0x18,0x10,0x00};
const uint8_t bar_1_char[8] = {0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10};
const uint8_t bar_2_char[8] = {0x14,0x14,0x14,0x14,0x14,0x14,0x14,0x14};
const uint8_t bar_3_char[8] = {0x15,0x15,0x15,0x15,0x15,0x15,0x15,0x15};

// names for CGRAM chars
const int InUseChar = 0;    // a right-facing arrow
const int Bar1Char = 1;     // 1 vertical bar
const int Bar2Char = 2;     // 2 vertical bars
const int Bar3Char = 3;     // 3 vertical bars

//-----
// Definitions for voltage measurement
//-----

// notional voltage reference value
#if REFERENCE == VREF_EXTERNAL_2_56 || REFERENCE == VREF_INTERNAL_2_56
    const float DefaultVoltsReference = 2.560;
#else
    const float DefaultVoltsReference = 4.096;
#endif

// limits of calibration adjustment
#if REFERENCE == VREF_EXTERNAL_2_56 || REFERENCE == VREF_INTERNAL_2_56
    const float MinVoltsReference = 2.0;
    const float MaxVoltsReference = 3.0;
#else
    const float MinVoltsReference = 3.5;
    const float MaxVoltsReference = 4.5;
#endif

const float DeltaVoltsReference = 0.001;

//-----
// Maximum & minimum power limits (dbM)
//-----

const float MaxPowerLimit = +15.0;
const float MinPowerLimit = -70.0;

//-----
// Size of the power "average" buffer
//-----

const int AvgBufferLen = 50;

//-----
// Buffer for external command gathering
//-----

// buffer, etc, to gather external command strings
#define MAX_COMMAND_LEN   16
#define COMMAND_END_CHAR    ';'
char CommandBuffer[MAX_COMMAND_LEN + 1];
int CommandIndex = 0;

// messages for command code
const char *ErrorMsg = "ERROR";
const char *OKMsg = "OK";

//-----
// Definitions for logical events from the RE
//-----

// typedef for Rotary Encoder events
typedef byte REEvent;

// define the logical RE events
enum Event
{
  re_None,       // no event, used sometimes
  re_RLeft,      // left rotate
  re_RRight,     // right rotate
  re_DnRLeft,    // left rotate, depressed
  re_DnRRight,   // right rotate, depressed
  re_Click,      // short click
  re_LongClick,  // long click
  re_DClick,     // double short click
};

// define the length of the event queue
// can get "event queue full" abort if too small
const int EventQueueLength = 10;

// default values for RE timouts
const int DefaultLongClickTime = 500;
const int DefaultDoubleClickTime = 300;

// time when click becomes a "long click" (milliseconds)
// the delay is configurable in the UI, this defines the limits
const int MinLongClickTime = 100;   // min configurable time
const int MaxLongClickTime = 1000;  // max configurable time

// time when click becomes a "double click" (milliseconds)
// the delay is configurable in the UI, this defines the limits
const int MinDClickTime = 100;      // min configurable time
const int MaxDClickTime = 1000;     // max configurable time

//-----
// Values for brightness/contrast things
//-----

// Maximum, minimum and step values for changing brightness/contrast
const int BrightMaxValue = 255;
const int BrightMinValue = 8;
const int BrightStep = 4;

const int ContrastMaxValue = 128;
const int ContrastMinValue = 0;
const int ContrastStep = 4;

// default brightness/contrast values
const int DefaultContrast   = ContrastMinValue;
const int DefaultBrightness = BrightMaxValue;

//-----
// EEPROM constants
//-----

// Define the address in EEPROM of various things.
#define NEXT_SLOT(a)    ((a) + sizeof(a))

// define the addresses of things saved in EEPROM
const int EepromBright = 0;
const int EepromContrast = NEXT_SLOT(EepromBright);
const int EepromLongClickTime = NEXT_SLOT(EepromContrast);
const int EepromDClickTime = NEXT_SLOT(EepromLongClickTime);
const int EepromVoltsRef = NEXT_SLOT(EepromDClickTime);

//-----
// Miscellaneous
//-----

// macro to turn off warnings about "unused parameter" in functions
#define UNUSED(v) (void) (v)

// macro to get number of elements in an array
#define ALEN(a)    (sizeof(a)/sizeof((a)[0]))

//-----
// Global variables
//-----

// LCD 1602 display object
LiquidCrystal lcd(LCD_RS, LCD_RW, LCD_E, LCD_D4, LCD_D5, LCD_D6, LCD_D7);

// values used to control contrast/brightness, etc, and default values
int lcd_contrast = DefaultContrast;
int lcd_brightness = DefaultBrightness;

// flag, "true" if DigiPower firmware has aborted
bool Aborted = false;

// actual voltage reference calibration value
float volts_ref = DefaultVoltsReference;

// Measured power
float Power_dBm = 0.0;


//##############################################################################
// Utility routines.
//##############################################################################

void reboot(void)
{
  // [...]
  // Set the magic bits to get a Caterina-based device
  // to reboot into the bootloader and stay there, rather
  // than run move onward
  //
  // These values are the same as those defined in
  // Caterina.c

  uint16_t bootKey = 0x7777;
  uint16_t *const bootKeyPtr = (uint16_t *)0x0800;

  // Stash the magic key
  *bootKeyPtr = bootKey;

  // Set a watchdog timer
  wdt_enable(WDTO_120MS);

  while(1) {} // This infinite loop ensures nothing else
              // happens before the watchdog reboots us
}

//----------------------------------------
// Get free memory in bytes.
//----------------------------------------

int free_ram(void)
{
    extern unsigned int __heap_start;
    extern void *__brkval;
    int v;

    return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval);
}


//----------------------------------------
// Convert a string to uppercase, in situ.
//----------------------------------------

void str2upper(char *str)
{
  while (*str)
  {
    *str = toupper(*str);
    ++str;
  }
}

//----------------------------------------
// Abort the program.
// Tries to tell the world what went wrong.  Sets the "Aborted" flag to "true".
//     msg  address of error string
// Only first one row of chars of message (NumCols) is displayed on LCD.
//----------------------------------------

void abort(const char *msg)
{
  // display on Serial output
  Serial.println(F("DigiPower has aborted:"));
  Serial.println(msg);

  // and on LCD
  lcd.clear();
  lcd.print(msg);
  lcd.setCursor(0, 1);
  lcd.print(" ... reset power");

  // turn up brightness/contrast in case they are too low
  analogWrite(LCD_K, DefaultBrightness);
  analogWrite(LCD_V0, DefaultContrast);

  // we used to loop forever here but that made reprogramming tricky,
  // so now we set an "aborted" flag and we allow loop() to run
  Aborted = true;
}

//----------------------------------------
// Routines to fade the screen in/out.
//----------------------------------------

void fade_in(int period)
{
  for (int b = 0; b < lcd_brightness; ++b)
  {
    analogWrite(LCD_K, b);
    delay(period);
  }
}

void fade_out(int period)
{
  for (int b = lcd_brightness; b > 0; --b)
  {
    analogWrite(LCD_K, b);
    delay(period);
  }
}

//----------------------------------------
// Show the credits.
//----------------------------------------

void show_credits(void)
{
  // display credits text
  lcd.clear();
  lcd.print(PRODUCT_NAME);
  lcd.print(" ");
  lcd.print(VERSION);
  lcd.setCursor(NumCols - strlen(CALLSIGN), 1);
  lcd.print(CALLSIGN);
}

//----------------------------------------
// Show the splash screen.  A small vanity.
//----------------------------------------

void show_splash(void)
{
  // show the credits
  show_credits();

  // pause, then fade display
  delay(1500);
  fade_out(3);
  lcd.clear();
  fade_in(1);
}

//##############################################################################
// Code to read voltage on AD8307 and calculate power.
//##############################################################################

#ifdef JUNK
//-----------------------------------------------------------------------------------------
//       Convert Voltage Reading into Power
//-----------------------------------------------------------------------------------------
//
void measure_Power(void)
{
  double  delta_db;
  int16_t delta_ad;
  double  delta_ad1db;

  // Calculate the slope gradient between the two calibration points:
  //
  // (dB_Cal1 - dB_Cal2)/(V_Cal1 - V_Cal2) = slope_gradient
  //
  delta_db = (double)((R.calibrate[1].db10m - R.calibrate[0].db10m)/10.0);
  delta_ad = R.calibrate[1].ad - R.calibrate[0].ad;
  delta_ad1db = delta_db/delta_ad;

  //
  // measured current dB value is then: (V - V_Cal1) * slope_gradient + dB_Cal1
  //
  ad8307_real = (ad8307_ad - R.calibrate[0].ad) * delta_ad1db + R.calibrate[0].db10m/10.0;
}
#endif

//##############################################################################
// The system event queue handler code.
//##############################################################################

// the event queue - circular buffer
REEvent event_queue[EventQueueLength];

// event queue pointers, "volatile" because accessed in non-interrupt code
volatile int queue_fore = 0;   // points at next event to be popped
volatile int queue_aft = 0;    // points at next free slot for a pushed event

//----------------------------------------
// Push an event onto the event queue.
//     event  number of the event to push
//
// If queue is full, abort!
// This routine is called only from interrupt code, so needs no protection.
//----------------------------------------

void event_push(REEvent event)
{
  // put new event into next empty slot
  event_queue[queue_fore] = event;

  // move fore ptr one slot up, wraparound if necessary
  ++queue_fore;
  if (queue_fore >= EventQueueLength)
    queue_fore = 0;

  // if queue full, abort
  if (queue_aft == queue_fore)
  {
    // dump what's in the queue
    event_dump_queue("ERROR: event queue full!");

    // and abort, there's no return from abort()!
    abort("Event queue full");
  }
}

//----------------------------------------
// Pop next event from the queue.
//
// Returns re_None if queue is empty.
// Called from non-interrupt code, so interrupts must be turned OFF.
//----------------------------------------

REEvent event_pop(void)
{
  // if queue empty, return None event
  if (queue_fore == queue_aft)
    return re_None;

  // Must protect from RE code while fiddling with queue
  noInterrupts();

  // get next event
  REEvent event = event_queue[queue_aft];

  // move aft pointer up one slot, wrap if necessary
  ++queue_aft;
  if (queue_aft >= EventQueueLength)
    queue_aft = 0;

  interrupts();

  return event;
}

//----------------------------------------
// Returns the number of events in the queue.
//
// Can be called from non-interrupt code
//----------------------------------------

int event_pending(void)
{
  // Must protect from RE code fiddling with queue
  noInterrupts();

  // get distance between fore and aft pointers
  int result = queue_fore - queue_aft;

  // handle case when events wrap around
  if (result < 0)
    result += EventQueueLength;

  interrupts();

  return result;
}

//----------------------------------------
// Convert an event number to a display string.
//----------------------------------------

const char *event2display(REEvent event)
{
  switch (event)
  {
    case re_None:      return "re_None";
    case re_RLeft:     return "re_RLeft";
    case re_RRight:    return "re_RRight";
    case re_DnRLeft:   return "re_DnRLeft";
    case re_DnRRight:  return "re_DnRRight";
    case re_Click:     return "re_Click";
    case re_LongClick: return "re_LongClick";
    case re_DClick:    return "re_DClick";
  }

  return "UNKNOWN EVENT";
}

//----------------------------------------
// Clear out any events in the queue.
//----------------------------------------

void event_flush(void)
{
  // Must protect from RE code fiddling with queue
  noInterrupts();

  queue_fore = 0;
  queue_aft = 0;

  interrupts();
}

//----------------------------------------
// Dump the queue contents to the console.
//     msg  address of message to show
// Debug code.
// Called only from code that has interrupts off.
//----------------------------------------

void event_dump_queue(const char *msg)
{
  Serial.println(F("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"));
  Serial.print(F("Queue: "));
  Serial.println(msg);
  for (int i = 0; i < EventQueueLength; ++i)
  {
    REEvent event = event_queue[i];

    Serial.print(F("  "));
    Serial.print(i);
    Serial.print(F(" -> "));
    Serial.println(event2display(event));
  }
  if (event_pending() == 0)
  {
    Serial.print(F("Queue length=0 (or "));
    Serial.print(EventQueueLength);
    Serial.println(F(")"));
  }
  else
  {
    Serial.print(F("Queue length="));
    Serial.println(EventQueueLength);
  }

  Serial.print(F("queue_aft="));
  Serial.print(queue_aft);
  Serial.print(F(", queue_fore="));
  Serial.println(queue_fore);
  Serial.println(F("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"));
}

//##############################################################################
// Interrupt driven rotary encoder interface.  From code by Simon Merrett.
// Based on insight from Oleg Mazurov, Nick Gammon, rt, Steve Spence.
//##############################################################################

// internal variables
UINT re_long_click_time = DefaultLongClickTime;
UINT re_double_click_time = DefaultLongClickTime;

volatile bool re_rotation = false;  // true if rotation occurred while knob down
volatile bool re_down = 0;          // non-zero while knob is down
volatile ULONG re_down_time = 0;    // milliseconds when knob is pressed down
volatile ULONG re_last_click = 0;   // time when last single click was found

// expecting rising edge on pinA or pinB - at detent
volatile byte aFlag = 0;
volatile byte bFlag = 0;

//----------------------------------------
// Initialize the rotary encoder stuff.
//
// Return 'true' if button was down during setup.
//----------------------------------------

bool re_setup(void)
{
  // make input pins HIGH for a moment
  // problems with pins being LOW on first read!?
  pinMode(re_pinBtn, OUTPUT);
  digitalWrite(re_pinBtn, HIGH);
  pinMode(re_pinA, OUTPUT);
  digitalWrite(re_pinA, HIGH);
  pinMode(re_pinB, OUTPUT);
  digitalWrite(re_pinB, HIGH);

  // set RE data pins as inputs
  pinMode(re_pinBtn, INPUT_PULLUP);
  pinMode(re_pinA, INPUT_PULLUP);
  pinMode(re_pinB, INPUT_PULLUP);

  // attach pins to ISR on falling edge only on RE pins
  attachInterrupt(digitalPinToInterrupt(re_pinA), pinA_isr, FALLING);
  attachInterrupt(digitalPinToInterrupt(re_pinB), pinB_isr, FALLING);

  // detect ANY change on pushbutton pin
  attachInterrupt(digitalPinToInterrupt(re_pinBtn), pinPush_isr, CHANGE);

  // read the pushbutton here, return value later
  int pb_down = ! digitalRead(re_pinBtn);

  // flush any logical events so far
  event_flush();

  // return state of RE push button, if DOWN this function returns 'true'
  return pb_down;
}

//----------------------------------------
// Handler for pushbutton interrupts (UP or DOWN).
//----------------------------------------

void pinPush_isr(void)
{
  // sample the pin value
  re_down = ! digitalRead(re_pinBtn);

  if (re_down)
  {
    // button pushed down
    re_rotation = false;      // no rotation while down so far
    re_down_time = millis();  // note time we went down
  }
  else
  {
    // button released, check if rotation, UP event if not
    if (! re_rotation)
    {
      ULONG last_up_time = millis();
      UINT push_time = last_up_time - re_down_time;

      if (push_time < re_long_click_time)
      {
        // check to see if we have a single click very recently
        if (re_last_click != 0)
        {
          // yes, did have single click before this release
          ULONG dclick_delta = last_up_time - re_last_click;

          // if short time since last click, issue double-click event
          if (dclick_delta <= re_double_click_time)
          {
            event_push(re_DClick);
            re_last_click = 0;
          }
          else
          {
            event_push(re_Click);
            re_last_click = last_up_time;    // single-click, prepare for possible double
          }
        }
        else
        {
          // no, this is an isolated release
          event_push(re_Click);
          re_last_click = last_up_time;    // single-click, prepare for possible double
        }
      }
      else
      {
        event_push(re_LongClick);
      }
    }
  }
}

//----------------------------------------
// Handler for pinA interrupts.
//----------------------------------------

void pinA_isr(void)
{
  // sample the pin values
  byte pin_A = digitalRead(re_pinA);
  byte pin_B = digitalRead(re_pinB);

  if (! pin_A && ! pin_B && aFlag)
  { // check that we have both pins at detent (LOW)
    // and that we are expecting detent on this pin's rising edge
    if (re_down)
    {
      event_push(re_DnRLeft);
      re_rotation = true;
    }
    else
    {
      event_push(re_RLeft);
    }
    bFlag = 0; //reset flags for the next turn
    aFlag = 0; //reset flags for the next turn
  }
  else if (! pin_A && pin_B)
  {
    // show we're expecting pinB to signal the transition to detent from free rotation
    bFlag = 1;
  }
}

//----------------------------------------
// Handler for pinB interrupts.
//----------------------------------------

void pinB_isr(void)
{
  // sample the pin values
  byte pin_A = digitalRead(re_pinA);
  byte pin_B = digitalRead(re_pinB);

  if (! pin_A && ! pin_B && bFlag)
  { // check that we have both pins at detent (ACTIVE) and
    // that we are expecting detent on this pin's rising edge
    if (re_down)
    {
      event_push(re_DnRRight);
      re_rotation = true;
    }
    else
    {
      event_push(re_RRight);
    }
    bFlag = 0; //reset flags for the next turn
    aFlag = 0; //reset flags for the next turn
  }
  else if (pin_A && ! pin_B)
  {
    // show we're expecting pinA to signal the transition to detent from free rotation
    aFlag = 1;
  }
}

//##############################################################################
// The EEPROM save/restore routines.
//##############################################################################

//----------------------------------------
// Save DigiPower state to EEPROM.
//----------------------------------------

void save_to_eeprom(void)
{
  EEPROM.put(EepromBright, lcd_brightness);
  EEPROM.put(EepromContrast, lcd_contrast);
  EEPROM.put(EepromLongClickTime, re_long_click_time);
  EEPROM.put(EepromDClickTime, re_double_click_time);
  EEPROM.put(EepromVoltsRef, volts_ref);
}

//----------------------------------------
// Restore DigiPower state from EEPROM.
//----------------------------------------

//int restored_brightness = -1;
//int restored_contrast = -1;

void restore_from_eeprom(void)
{
  EEPROM.get(EepromBright, lcd_brightness);
  EEPROM.get(EepromContrast, lcd_contrast);
  EEPROM.get(EepromLongClickTime, re_long_click_time);
  EEPROM.get(EepromDClickTime, re_double_click_time);
  EEPROM.get(EepromVoltsRef, volts_ref);
}

//##############################################################################
// Code to handle the DigitalVFO menus.
//##############################################################################

// handler for selection of an item (re_Click event)
typedef void (*ItemAction)(struct Menu *, int);

// structure defining a menu item
struct MenuItem
{
  const char *title;          // menu item display text
  struct Menu *menu;          // if not NULL, submenu to pass to menu_show()
  ItemAction action;          // if not NULL, address of action function
};

// A structure defining a menu
struct Menu
{
  const char *title;          // title displayed on menu page
  int num_items;              // number of items in the array below
  struct MenuItem **items;    // array of pointers to MenuItem data
};

//----------------------------------------
// Draw a menu on the screen.
//     menu  pointer to a Menu structure
// Only draws the top row.
//----------------------------------------

void menu_draw(struct Menu *menu)
{
  // clear screen and write menu title on upper row
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(menu->title);
}

//----------------------------------------
// Draw a standard menuitem on the screen.
//     menu      pointer to a Menu structure
//     item_num  the item number to show
// Custom menuitems are drawn by their handler.
//----------------------------------------

void menuitem_draw(struct Menu *menu, int item_num)
{
  // figure out max length of item strings
  int max_len = 0;

  for (int i = 0; i < menu->num_items; ++ i)
  {
    int new_len = strlen(menu->items[i]->title);

    if (new_len > max_len)
        max_len = new_len;
  }

  // write indexed item on lower row, right-justified
  lcd.setCursor(0, 1);
  lcd.print(BlankRow);
  lcd.setCursor(NumCols - max_len, 1);
  lcd.print(menu->items[item_num]->title);
}

//----------------------------------------
// Handle a menu.
//     menu      pointer to a defining Menu structure
//     unused    unused item number (used by action routines)
//               (used to be index to initial item to draw)
// Handle events in the loop here.
// This code doesn't see events handled in any *_action() routine.
//----------------------------------------

void menu_show(struct Menu *menu, int unused)
{
  UNUSED(unused);
  int item_num = 0;     // always show first menuitem

  // draw the entire menu
  menu_draw(menu);
  menuitem_draw(menu, item_num);

  // get rid of any stray events to this point
  event_flush();

  while (true)
  {
    if (event_pending())
    {
      switch (event_pop())
      {
        case re_RLeft:
          if (--item_num < 0)
            item_num = 0;
          break;
        case re_RRight:
          if (++item_num >= menu->num_items)
            item_num = menu->num_items - 1;
          break;
        case re_Click:
          if (menu->items[item_num]->action != NULL)
          {
            // if there's a handler, call it
            // this will destroy the current menu page
            (*menu->items[item_num]->action)(menu, item_num);
          }
          else
          {
            // recurse down to sub-menu
            menu_show(menu->items[item_num]->menu, 0);
          }
          menu_draw(menu);    // redraw the menu header, item redrawn below
          break;
        case re_LongClick:
          event_flush();
          lcd.clear();
          return;             // back to the parent menu or main screen
      }

      // above may have destroyed display of menuitem
      menuitem_draw(menu, item_num);
    }
  }
}

//----------------------------------------
// Show that *something* happened.
// Flash the screen in a possibly eye-catching way.
//----------------------------------------

void display_flash(void)
{
  for (int x = 0; x < 5; ++x)
  {
    fade_out(0);
    delay(20);
    fade_in(0);
    delay(20);
  }
}

//##############################################################################
// Define the menu action routines.
//##############################################################################

//----------------------------------------
// Called when the menuitem performs no action.
//----------------------------------------

void nop_action(struct Menu *menu, int item_num)
{
  UNUSED(menu);
  UNUSED(item_num);
}

//----------------------------------------
// Reset everything in the firmware - Dangerous!
//   menu      address of 'calling' menu
//   item_num  index of MenuItem we were actioned from
//
// All calibration settings lost as well as brightness
// and contrast settings, etc.
//----------------------------------------

void reset_action(struct Menu *menu, int item_num)
{
  UNUSED(menu);
  UNUSED(item_num);

  // reset brightness/contrast to maximum
  lcd_brightness = BrightMaxValue;
  lcd_contrast = lcd_contrast;

  // set RE parameters to defaults
  re_long_click_time = DefaultLongClickTime;
  re_double_click_time = DefaultLongClickTime;

  // voltage reference calibartion back to default
  volts_ref = DefaultVoltsReference;

  // save all reset values
  save_to_eeprom();
  display_flash();
}

//----------------------------------------
// Reboot the device!
//   menu      address of 'calling' menu
//   item_num  index of MenuItem we were actioned from
//
// Starts the process of the bootloader looking for a download.
//----------------------------------------

void reboot_action(struct Menu *menu, int item_num)
{
  UNUSED(menu);
  UNUSED(item_num);

  lcd.clear();
  lcd.print(F("Rebooting..."));
  Serial.println(F("Rebooting..."));
  reboot();
}

//----------------------------------------
// Show the credits.
//   menu      address of 'calling' menu
//   item_num  index of MenuItem we were actioned from
//
// Wait here until any type of RE click.
//----------------------------------------

void credits_action(struct Menu *menu, int item_num)
{
  UNUSED(menu);
  UNUSED(item_num);

  // get rid of any stray events to this point
  event_flush();

  // show the credits
  show_credits();

  // handle events in our own little event loop
  // we want to wait until some sort of click
  while (true)
  {
    REEvent event = event_pop();

    // handle any pending event, ignore all but long-click
    if (event == re_LongClick)
    {
      event_flush();
      return;
    }
  }
}

//----------------------------------------
// Draw a bar of a given length on row 1 of LCD.
//   length  length of the bar [1, 48]
//----------------------------------------

void draw_row1_bar(int length)
{
  int full_chars = length / 3;
  int part_char = length % 3;

  lcd.setCursor(0, 1);
  lcd.print(BlankRow);
  lcd.setCursor(0, 1);

  // draw the number of full chars
  for (int i = 0; i < full_chars; ++i)
  {
    lcd.write((byte) Bar3Char);
  }

  // draw last part character
  if (part_char > 0)
  {
    lcd.write((byte) part_char);
  }
}

//----------------------------------------
// Set the current brightness and save to EEPROM if 'actioned'.
//   menu      address of 'calling' menu
//   item_num  index of MenuItem we were actioned from
// We show immediately what the changes mean, but only update the
// actual brightness if we do a re_Click action.
//----------------------------------------

void brightness_action(struct Menu *menu, int item_num)
{
  // save original brightness in case we don't update
  int old_brightness = lcd_brightness;

  // convert brighness value to a bar value in [1, 48]
  int index = map(lcd_brightness, BrightMinValue, BrightMaxValue, 1, 48);

  // get rid of any stray events to this point
  event_flush();

  // draw row 0 of menu, get heading from MenuItem
  lcd.clear();
  lcd.print(menu->items[item_num]->title);

  // show row slot info on row 1
  draw_row1_bar(index);

  // handle events in our own little event loop
  while (true)
  {
    // handle any pending event
    if (event_pending() > 0)
    {
      REEvent event = event_pop(); // get next event and handle it

      switch (event)
      {
        case re_RLeft:
          if (--index < 1)
            index = 1;
          break;
        case re_RRight:
          if (++index > 48)
            index = 48;
          break;
        case re_Click:
          old_brightness = lcd_brightness = map(index, 1, 48, BrightMinValue, BrightMaxValue);
          save_to_eeprom();
          display_flash();
          break;
        case re_LongClick:
          lcd_brightness = old_brightness;
          analogWrite(LCD_K, lcd_brightness);
          event_flush();
          return;
        default:
          // ignored events we don't handle
          break;
      }

      // adjust display brightness so we can see the results
      lcd_brightness = map(index, 1, 48, BrightMinValue, BrightMaxValue);
      analogWrite(LCD_K, lcd_brightness);

      // show brightness value in row 1
      draw_row1_bar(index);
    }
  }
}

//----------------------------------------
// Set the current contrast and save to EEPROM if 'actioned'.
//   menu      address of 'calling' menu
//   item_num  index of MenuItem we were actioned from
// We show immediately what the changes mean, but only update the
// actual contrast if we do a re_Click action.
//
// We limit the allowed range for lcd_contrast to [0, 255].
//----------------------------------------

void contrast_action(struct Menu *menu, int item_num)
{
  // save old contrast just in case we don't set it here
  int old_contrast = lcd_contrast;

  // convert contrast value to a display value in [1, 48]
  int index = map(lcd_contrast, ContrastMinValue, ContrastMaxValue, 1, 48);

  // get rid of any stray events to this point
  event_flush();

  // draw row 0 of menu, get heading from MenuItem
  lcd.clear();
  lcd.print(menu->items[item_num]->title);

  // show row slot info on row 1
  // Contrast voltage works opposite to brightness
  draw_row1_bar(index);

  // handle events in our own little event loop
  while (true)
  {
    // handle any pending event
    if (event_pending() > 0)
    {
      REEvent event = event_pop(); // get next event and handle it

      switch (event)
      {
        case re_RLeft:
          if (--index < 1)
            index = 1;
          break;
        case re_RRight:
          if (++index > 48)
            index = 48;
          break;
        case re_Click:
          old_contrast = lcd_contrast = map(index, 1, 48, ContrastMinValue, ContrastMaxValue);
          save_to_eeprom();
          display_flash();
          break;
        case re_LongClick:
          // not saving, restore original contrast setting
          lcd_contrast = old_contrast;
          analogWrite(LCD_V0, lcd_contrast);
          event_flush();
          return;
        default:
          // ignored events we don't handle
          break;
      }

      // adjust display contrast so we can see the results
      lcd_contrast = map(index, 1, 48, ContrastMinValue, ContrastMaxValue);
      analogWrite(LCD_V0, lcd_contrast);

      // show brightness value in row 1
      // Contrast voltage works opposite to brightness
      draw_row1_bar(index);
    }
  }
}

//----------------------------------------
// Draw a click hold time in milliseconds on row 1 of LCD.
//     msec      the time to show
//     def_time  the current system time
//----------------------------------------

void draw_row1_time(UINT msec, UINT def_time)
{
  lcd.setCursor(0, 1);
  lcd.print(BlankRow);

  if (msec == def_time)
  {
    lcd.setCursor(0, 1);
    lcd.write((byte) InUseChar);
  }

  lcd.setCursor(8, 1);
  if (msec < 1000)
    lcd.setCursor(9, 1);
  lcd.print(msec);
  lcd.print(F("msec"));
}

//----------------------------------------
// Set the current 'long click' time and save to EEPROM if 'actioned'.
//   menu      address of 'calling' menu
//   item_num  index of MenuItem we were actioned from
// This works differently from brightness/contrast.
//----------------------------------------

void longclick_action(struct Menu *menu, int item_num)
{
  UINT holdtime = re_long_click_time;      // the value we change
  const UINT hold_step = 100;           // step adjustment +/-

  // get rid of any stray events to this point
  event_flush();

  // draw row 0 of menu, get heading from MenuItem
  lcd.clear();
  lcd.print(menu->items[item_num]->title);

  // show row slot info on row 1
  draw_row1_time(holdtime, re_long_click_time);

  // handle events in our own little event loop
  while (true)
  {
    // handle any pending event
    if (event_pending() > 0)
    {
      REEvent event = event_pop(); // get next event and handle it

      switch (event)
      {
        case re_RLeft:
          holdtime -= hold_step;
          if (holdtime < MinLongClickTime)
            holdtime = MinLongClickTime;
          break;
        case re_RRight:
          holdtime += hold_step;
          if (holdtime > MaxLongClickTime)
            holdtime = MaxLongClickTime;
          break;
        case re_Click:
          re_long_click_time = holdtime;
          save_to_eeprom();         // save change to EEPROM
          display_flash();
          break;
        case re_LongClick:
          event_flush();
          return;
        default:
          // ignored events we don't handle
          break;
      }

      // show hold time value in row 1
      draw_row1_time(holdtime, re_long_click_time);
    }
  }
}

//----------------------------------------
// Set the current 'doubleclick' time and save to EEPROM if 'actioned'.
//   menu      address of 'calling' menu
//   item_num  index of MenuItem we were actioned from
// This works differently from brightness/contrast.
// We show menuitems of the time to use.
//----------------------------------------

void doubleclick_action(struct Menu *menu, int item_num)
{
  int dctime = re_double_click_time;      // the value we adjust
  UINT dclick_step = 100;         // step adjustment +/-

  // get rid of any stray events to this point
  event_flush();

  // draw row 0 of menu, get heading from MenuItem
  lcd.clear();
  lcd.print(menu->items[item_num]->title);

  // show row slot info on row 1
  draw_row1_time(dctime, re_double_click_time);

  // handle events in our own little event loop
  while (true)
  {
    // handle any pending event
    if (event_pending() > 0)
    {
      REEvent event = event_pop(); // get next event and handle it

      switch (event)
      {
        case re_RLeft:
          dctime -= dclick_step;
          if (dctime < MinDClickTime)
            dctime = MinDClickTime;
          break;
        case re_RRight:
          dctime += dclick_step;
          if (dctime > MaxDClickTime)
            dctime = MaxDClickTime;
          break;
        case re_Click:
          re_double_click_time = dctime;
          save_to_eeprom();         // save changes to EEPROM
          display_flash();
          break;
        case re_LongClick:
          event_flush();
          return;
        default:
          // ignored events we don't handle
          break;
      }

      // show hold time value in row 1
      draw_row1_time(dctime, re_double_click_time);
    }
  }
}

void draw_volts_ref(float volts)
{
  lcd.setCursor(0, 1);
  lcd.print(BlankRow);
  lcd.setCursor(16 - 6, 1);  // 16-6 digits (x.xxxv)
  lcd.print(volts, 3);
  lcd.print(F("v"));
}

//----------------------------------------
// Set the calibration and save to EEPROM if 'actioned'.
//   menu      address of 'calling' menu
//   item_num  index of MenuItem we were actioned from
//----------------------------------------

void calibrate_action(struct Menu *menu, int item_num)
{
  // save original calibration in case we don't update
  float old_volts_ref = volts_ref;

  // get rid of any stray events to this point
  event_flush();

  // draw row 0 of menu, get heading from MenuItem
  lcd.clear();
  lcd.print(menu->items[item_num]->title);

  // show volts reference on row 1
  draw_volts_ref(volts_ref);

  // handle events in our own little event loop
  while (true)
  {
    // handle any pending event
    if (event_pending() > 0)
    {
      REEvent event = event_pop();

      switch (event)
      {
        case re_RLeft:
          volts_ref -= DeltaVoltsReference;
          if (volts_ref < MinVoltsReference)
            volts_ref = MinVoltsReference;

          break;
        case re_RRight:
          volts_ref += DeltaVoltsReference;
          if (volts_ref > MaxVoltsReference)
            volts_ref = MaxVoltsReference;
          break;
        case re_Click:
          old_volts_ref = volts_ref;
          save_to_eeprom();
          display_flash();
          break;
        case re_LongClick:
          volts_ref = old_volts_ref;
          event_flush();
          return;
        default:
          // ignored events we don't handle
          break;
      }

      // show calibration value in row 1
      Serial.print("calibrate_action: volts_ref=");
      draw_volts_ref(volts_ref);
    }
  }
}

//----------------------------------------
// Reboot menu
//----------------------------------------

struct MenuItem mi_reboot_no = {"No", NULL, &nop_action};
struct MenuItem mi_reboot_yes = {"Yes", NULL, &reboot_action};
struct MenuItem *mia_reboot[] = {&mi_reboot_no, &mi_reboot_yes};
struct Menu reboot_menu = {"Reboot", ALEN(mia_reboot), mia_reboot};

//----------------------------------------
// Reset menu
//----------------------------------------

struct MenuItem mi_reset_no = {"No", NULL, &nop_action};
struct MenuItem mi_reset_yes = {"Yes", NULL, &reset_action};
struct MenuItem *mia_reset[] = {&mi_reset_no, &mi_reset_yes};
struct Menu reset_menu = {"Reset all", ALEN(mia_reset), mia_reset};

//----------------------------------------
// Settings menu
//----------------------------------------

struct MenuItem mi_brightness = {"Brightness", NULL, &brightness_action};
struct MenuItem mi_contrast = {"Contrast", NULL, &contrast_action};
struct MenuItem mi_holdclick = {"Hold Click", NULL, &longclick_action};
struct MenuItem mi_doubleclick = {"Double Click", NULL, &doubleclick_action};
struct MenuItem mi_calibclick = {"Calibrate", NULL, &calibrate_action};
struct MenuItem *mia_settings[] = {&mi_brightness, &mi_contrast,
                                      &mi_holdclick, &mi_doubleclick, &mi_calibclick};
struct Menu settings_menu = {"Settings", ALEN(mia_settings), mia_settings};

//----------------------------------------
// Main menu
//----------------------------------------

struct MenuItem mi_settings = {"Settings", &settings_menu, NULL};
struct MenuItem mi_reset = {"Reset all", &reset_menu, NULL};
struct MenuItem mi_reboot = {"Reboot", &reboot_menu, NULL};
struct MenuItem mi_credits = {"Credits", NULL, &credits_action};
struct MenuItem *mia_main[] = {&mi_settings, &mi_reset, &mi_reboot, &mi_credits};
struct Menu menu_main = {"Menu", ALEN(mia_main), mia_main};

//##############################################################################
// Code for the "external" commands from the serial port.
//##############################################################################

//----------------------------------------
// Get help:
//     H;
//----------------------------------------

void xcmd_help(char *cmd)
{
  UNUSED(cmd);

  Serial.println(F("-----------Interactive Commands-----------------"));
  Serial.println(F("H;       send help text to console"));
  Serial.println(F("ID;      get device identifier string"));
  Serial.println(F("C;       get calibration voltage"));
  Serial.println(F("Cx.xxx;  set calibration voltage to 'x.xxx'"));
  Serial.println(F("P;       read power value"));
  Serial.println(F("D;       print some internal debug information"));
  Serial.println(F("R;       reboot the device"));
  Serial.println(F("------------------------------------------------"));
}

//----------------------------------------
// Get the identifier string:
//     ID
//----------------------------------------

void xcmd_id(char *cmd)
{
  // if not legal, complain
  if (strcmp(cmd, "ID"))
  {
    Serial.println(ErrorMsg);
    return;
  }

  // generate ID string and return
  Serial.print(F(PRODUCT_NAME));
  Serial.print(F(" "));
  Serial.println(F(VERSION));
}

//----------------------------------------
// Set/Get calibration value (Aref voltage)
//     Cx.xxx      set calibration
//     C           get calibration
//----------------------------------------

void xcmd_calib(char *cmd)
{
  // are we setting or getting the voltage calibration?
  if (strlen(cmd) == 1)
  {
    Serial.println(volts_ref, 3);
    return;
  }

  // setting voltage calibration, get new float value
  String value_str = cmd + 1;
  volts_ref = value_str.toFloat();
  EEPROM.put(EepromVoltsRef, volts_ref);
  Serial.println(OKMsg);
}

//----------------------------------------
// Get the measured power value
//     P
//----------------------------------------

void xcmd_read(char *cmd)
{
  // if not legal, complain
  if (strcmp(cmd, "P"))
  {
    Serial.println(ErrorMsg);
    return;
  }

  // generate answer string and return
  Serial.println(Power_dBm, 1);
}

//----------------------------------------
// Perform debug:
//     D
//
// Put any debug code you want here.
// Could expand to "Dx" where the "x" char selects debug required.
//----------------------------------------

void xcmd_debug(char *cmd)
{
  // if not legal, complain
  if (strcmp(cmd, "D"))
  {
    Serial.println(ErrorMsg);
    return;
  }

  int v_bits = analogRead(AD8307_pin);
  float voltage = v_bits * volts_ref / 1024;
  Power_dBm = moving_average(voltage2dbm(voltage));
  float power_w = pow(10, (Power_dBm - 30) / 10);

  Serial.print(F("free_ram="));
  Serial.print(free_ram());

  Serial.print(F(", v_bits="));
  Serial.print(v_bits);
  Serial.print(F(", voltage="));
  Serial.print(voltage, 3);
  Serial.print(F("v, power="));
  Serial.print(Power_dBm, 2);
  Serial.print(F("dBm, "));
  if (power_w < 0.000001)
  {
    Serial.print(power_w * 1000000000, 3);
    Serial.println(F("nW"));
  }
  else if (power_w < 0.001)
  {
    Serial.print(power_w * 1000000, 3);
    Serial.println(F("μW"));
  }
  else
  {
    Serial.print(power_w * 1000, 3);
    Serial.println(F("mW"));
  }
}

//----------------------------------------
// Perform debug:
//     R
//
// Reboot the device.  No need to "push the button".
//----------------------------------------

void xcmd_reboot(char *cmd)
{
  // if not legal, complain
  if (strcmp(cmd, "R"))
  {
    Serial.println(ErrorMsg);
    return;
  }

  // OK, reboot this thing (AtMega32U4 bootloader)
  lcd.clear();
  lcd.print(F("Rebooting..."));
  Serial.println(F("Rebooting..."));
  reboot();
}

//----------------------------------------
// Process an external command.
//     cmd     address of command string buffer
//     index   index of last char in string buffer
// 'cmd' is '\0' terminated.
//----------------------------------------
void do_extern_cmd(char *cmd)
{
  // ensure everything is uppercase
  str2upper(cmd);

  // process the command
  switch (cmd[0])
  {
    case 'H':
      xcmd_help(cmd);
      return;
    case 'I':
      xcmd_id(cmd);
      return;
    case 'C':
      xcmd_calib(cmd);
      return;
    case 'P':
      xcmd_read(cmd);
      return;
    case 'D':
      xcmd_debug(cmd);
      return;
    case 'R':
      xcmd_reboot(cmd);
      return;
  }

  Serial.println(ErrorMsg);
}

void do_external_commands(void)
{
  bool overflow = false;    // "true" if command buffer overflowed

  // gather any commands from the external controller
  while (Serial.available())
  {
    char ch = Serial.read();

    // ignore newlines
    if (ch == '\n')
      continue;

    if (CommandIndex < MAX_COMMAND_LEN)
    {
      // only add if command not over-length
      CommandBuffer[CommandIndex++] = ch;
    }
    else
      overflow = true;

    if (ch == COMMAND_END_CHAR)   // if end of command, execute it
    {
      if (overflow)
      {
        overflow = false;
        Serial.println(F("TOO LONG"));
      }
      else
      {
        CommandBuffer[CommandIndex-1] = '\0'; // remove final ';'
        do_extern_cmd(CommandBuffer);
        CommandIndex = 0;
      }
    }
  }
}

// show main screen
void show_main_screen(float power)
{
  static ULONG last_secs = -1;
  ULONG elapsed = millis() / 1000;

  if (elapsed != last_secs)
  {
    last_secs = elapsed;

    float power_w = pow(10, (power - 30) / 10);
    char *units = (char *) "W";
    int col_start;      // column to start Power/Watts value in

    // display POWER
    lcd.setCursor(0, 0);
    lcd.print(BlankRow);
    col_start = 1;
    if (abs(power) < 10.0)
      col_start = 2;
    else if (abs(power) < 100.0)
      col_start = 1;
    if (power >= 0.0)
      col_start += 1;
    lcd.setCursor(col_start-1, 0);
    lcd.print(power, 2);
    lcd.print(F("dBm"));

    // display WATTS
    lcd.setCursor(0, 1);
    lcd.print(BlankRow);
    lcd.setCursor(0, 1);
    if (power < MinPowerLimit)
      lcd.print(F("-----"));
    else if (power > MaxPowerLimit)
      lcd.print(F("+++++"));
    else
    {
      if (power_w < 0.000001)
      {
        power_w *= 1000000000;
        units = (char *) "nW";
      }
      else if (power_w < 0.001)
      {
        power_w *= 1000000;
        units = (char *) "\xe4W";   // "µW"
      }
      else
      {
        power_w *= 1000;
        units = (char *) "mW";
      }

      // justify power figure so "." in fixed place
      col_start = 0;
      if (power_w < 10.0)
        col_start = 2;
      else if (power_w < 100.0)
        col_start = 1;
        
      lcd.setCursor(col_start, 1);
      lcd.print(power_w, 2);
      lcd.print(units);
    }
  }
}

//----------------------------------------
// Calculate dBm from the voltage.
//   voltage  voltage from the AD8307
// Returns the calibrated dBm.
//
// From the AD8307 datasheet, -60dBm = 0.41v and 0dBm = 2.03v.
//----------------------------------------

#define Vatm60    0.41
#define Vat0      2.03

float voltage2dbm(float voltage)
{
  return -60.0 + (voltage - Vatm60) * 60.0 / (Vat0 - Vatm60);
}

//----------------------------------------
// Average the measured voltage.
//   new_value  latest voltage from the AD8307
//
// Performs a weighted triangle average.
//----------------------------------------

#define Samples  101
float latest[Samples];
int max_weight = Samples / 2;
int avg_div = Samples/2 * (Samples/2+1) + Samples/2 + 1;

float moving_average(float new_value)
{
  // put new value at end after moving all entries down one slot
  memcpy(&latest[0], &latest[1], (Samples - 1) * sizeof(float));
  latest[Samples-1] = new_value;

  // get the weighted average
  float sum = 0.0;
  int weight = 1;
  int offset = 1;
  
  for (int i=0; i < Samples; ++i)
  {
    sum += weight * latest[i];
    if (weight > max_weight)
      offset = -1;
    weight += offset;
  }
  
  return sum / avg_div;
}

//----------------------------------------
// Fill the average buffer with a value.
//   new_value  value to fill with
//----------------------------------------

void avg_init(float new_value)
{
  for (int i=0; i < Samples; ++i)
    latest[i] = new_value;
}

//##############################################################################
// Standard Arduino setup()function.
//##############################################################################

void setup(void)
{
  // initialize the serial console
  Serial.begin(115200);

  // only useful with serial USB connection, hangs without one
#ifdef SERIAL_USB
  while (!Serial)       // wait for Arduino Serial Monitor (native USB boards)
    delay(10);
#endif

  // set mode of pin reading AD8307 output
  pinMode(AD8307_pin, INPUT);

  // set up the 1602 display
  lcd.begin(NumCols, NumRows);

  // use the Aref voltage reference
#if REFERENCE == VREF_EXTERNAL_2_56 || REFERENCE == VREF_EXTERNAL_4_096
  analogReference(EXTERNAL);
#else
  analogReference(INTERNAL);
#endif

  // set up the rotary encoder
  // reset a few things if RE button down during powerup
  if (re_setup())
  {
    Serial.println(F("Resetting various things"));
    Serial.println(F("Click the RE button to continue..."));
    lcd.setCursor(0, 0);
    lcd.print(F("RESET!"));
    lcd.setCursor(0, 1);
    lcd.print(F(" Click button..."));

    // set brightness/contrast to max visibility
    lcd_brightness = DefaultBrightness;
    lcd_contrast = DefaultContrast;
    analogWrite(LCD_K, lcd_brightness);
    analogWrite(LCD_V0, lcd_contrast);

    // voltage reference calibartion back to default

    volts_ref = DefaultVoltsReference;

    save_to_eeprom();   // save default values to EEPROM

    // flush any events in the queue, then wait for re_Click
    event_flush();
    while (event_pop() != re_Click)
      ;
  }

  // create the CGRAM definable characters
  lcd.createChar(InUseChar, (uint8_t *) in_use_char);
  lcd.createChar(Bar1Char, (uint8_t *) bar_1_char);
  lcd.createChar(Bar2Char, (uint8_t *) bar_2_char);
  lcd.createChar(Bar3Char, (uint8_t *) bar_3_char);

  // get saved state from EEPROM and set brightness/contrast
  restore_from_eeprom();
  analogWrite(LCD_K, lcd_brightness);
  analogWrite(LCD_V0, lcd_contrast);

  // show splash screen
  show_splash();

  // Announce that we are ready!
#ifndef SERIAL_USB
  delay(1000);
#endif
  Serial.print("DigiPower ");
  Serial.print(VERSION);
  Serial.println(" - Ready!");

  // read the voltage from the AD8307 & initialize the averaging
  float voltage = analogRead(AD8307_pin) * volts_ref / 1024;
  avg_init(voltage2dbm(voltage));
}

//##############################################################################
// Standard Arduino loop() function.
//##############################################################################

void loop(void)
{
  // if aborted we want loop() to run to allow easy reprogramming
  // previously we tight looped in abort() but programming was a problem
  if (Aborted)
    return;

  // handle any commands from the serial port
  do_external_commands();

  // read the voltage from the AD8307
  float voltage = analogRead(AD8307_pin) * volts_ref / 1024;
  Power_dBm = moving_average(voltage2dbm(voltage));

  // handle RE events
  REEvent event;

  while ((event = event_pop()) != re_None)
  {
    switch (event)
    {
      case re_LongClick:
        // start the menu system, changes may occur
        menu_show(&menu_main, 0);

        // save any new values to EEPROM and update display brightness/contrast
        save_to_eeprom();
        analogWrite(LCD_K, lcd_brightness);
        analogWrite(LCD_V0, lcd_contrast);
        break;
    }
  }

  show_main_screen(Power_dBm);
}
