/*
 * Code to test operation of the 1206 LCD display.
 */

#include <LiquidCrystal.h>

// the builtin LED
const int led = 12;         // D6 - builtin LED

// define the pins connecting to the 1206 LCD
const int LCD_RS = 12;     // D6 on Iota
const int LCD_RW = 11;     // C6 on Iota
const int LCD_E  = 10;     // B6 on Iota
const int LCD_D4 = 5;      // C6 on Iota
const int LCD_D5 = 4;      // D4 on Iota
const int LCD_D6 = 3;      // D0 on Iota
const int LCD_D7 = 2;      // D1 on Iota

LiquidCrystal lcd(LCD_RS, LCD_RW, LCD_E, LCD_D4, LCD_D5, LCD_D6, LCD_D7);


//----------------------------------------
// Flash the builtin LED.
//----------------------------------------

void flash(void)
{
  digitalWrite(led, HIGH);
  delay(2);
  digitalWrite(led, LOW);
  delay(5);
}

//----------------------------------------
// Abort the program.
// Tries to tell the world what went wrong, then just loops.
//     msg  address of error string
// Only first NumRows*NumCols chars of message is displayed on LCD.
//----------------------------------------

void abort(const char *msg)
{
  // print error on console (maybe)
  Serial.print("message=");
  Serial.println(msg);
  Serial.println("Microcontroller is paused!");

  // wait here for a bit
  while (1)
    ;
}

void setup(void)
{
  // initialize the serial console
  Serial.begin(115200);
  while (! Serial) // wait for Serial to go live (Leonardo only)
    ;

  // initialize the LED pin
  pinMode(led, OUTPUT);
  
  // set up the 1206 display
  lcd.begin(16,2); 
  lcd.clear();
  lcd.setCursor(0, 0);  
  lcd.print("hello, world!");

  Serial.println("Ready!");
}

//----------------------------------------
// Standard Arduino loop() function.
//----------------------------------------

void loop(void)
{
  lcd.setCursor(0, 1);
  lcd.print(millis());
}
