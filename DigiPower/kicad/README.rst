This directory holds the KiCad files for the schematic and PCB.

The *gerbers* subdiretory holds the Gerber files used to make the PCB.
