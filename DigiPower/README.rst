Digital Power Meter
===================

This is a realization of the digital power meter described in the May/June
2013 issue of QEX.

The *kicad* directory contains the KiCad files used to produce the
circuit board.

The *Misc* directory holds the original article, datasheets, etc.

The *code* directory holds the DigiPower.ino firmware.

Implementation
--------------

Initial implementation will follow the article closely except that an AtMega32U4
microcontroller will be used, not a Teensy++.  The project will be built on a
custom PCB.

Status
------

*17 April 2020*: Everything assembled and in the case.

*27 March 2020*: Version 1.1 code working.  This just exercises the hardware except
for the actual power meiasurement.  Will add menu system next.

*24 March 2020*: PCBs back from China.  Version 1.1 built and software being written.

The PCB, fresh from China:

.. image:: IMG_6378.png

This is the breadboard version behind the assembled PCB:

.. image:: IMG_6355.png

The PCB assembled and running.
Needs only the shielding over the RF section at bottom left:

.. image:: IMG_6357.png

The finally assembled DigiPower device, measuring the output
from the DigitalVFO:

.. image:: IMG_6366.png

The latest schematic is:

.. image:: DigiPower_Schematic_1.1.png

and the PCB, with minor changes from experience with first PCB:

.. image:: DigiPower_PCB_1.1.png
