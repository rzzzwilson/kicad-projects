What is this?
=============

*PowRef* is a small USB-powered device that is used as a power level reference.

Design
------

The design here is based on the circuit in
*misc/PowerReferenceSource100MHz_SCH.pdf*.

Schematic
---------

.. image:: PowRef_1.0_Schematic.png

PCB
---

.. image:: PowRef_1.0_PCB.png

