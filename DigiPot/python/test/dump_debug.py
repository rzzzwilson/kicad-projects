#!/usr/bin/env python3

"""
A program to do a "d;" command on a DigiPot and print the results.
"""

import os
import sys
import serial


# program version info
Version = 'dump_debug 1.0'

# USB/FTDI board identifiers for an AtMega32U4 DigiPot
VendorID = 0x2341
ProductID = 0x8036
Product = 'DigiPot'

# chose a comports() implementation, depending on os
if os.name == 'nt':
    from serial.tools.list_ports_windows import comports
elif os.name == 'posix':
    from serial.tools.list_ports_posix import comports
else:
    raise ImportError(f"Sorry: no implementation for your platform ('{os.name}') available")

def read_line(ser):
    """Read one line from the 'ser' connection."""

    line = ''
    count = 0
    while count < 3:
        char = _readchar(ser)
        if char:
            if char == '\n':
                return line
            else:
                line += char
                count = 0
        else:
            count += 1

    return ''

######
# Code to find all DigiPot devices that can communicate
######

def find_digivolt():
    """Returns a single DigiPot device."""

    # get *all* DigiPot devices
    result = []
    for usb_dev in sorted(comports()):
        if usb_dev.vid == VendorID and usb_dev.pid == ProductID and usb_dev.product == Product:
            result.append(usb_dev)

    # make sure we have only one, return it
    if len(result) == 0:
        print('Sorry, no DigiPot devices were found.')
        sys.exit(1)

    if len(result) > 1:
        print('Sorry, more than one DigiPot device was found.')
        sys.exit(1)

    return result[0]

def check_connection(ser, device):
    """Ensure we can communicate with the found device.

    ser     the serial connection to the device
    device  the actual device instance
    """

    # flush any pending output
    while _readchar(ser):
        pass

    # ask for an ID string
    id_string = do_cmd(ser, 'id;')
    return id_string.startswith('DigiPot')

def flush_input(ser):
    """Throwaway all input from the device until a timeout."""

    count = 0
    while count < 3:
        line = read_line(ser)
        if line:
            count = 0
        else:
            count += 1

def do_cmd(ser, cmd):
    """Send command to DigiPot, return response."""

    if cmd[-1] != '\n':
        cmd += '\n'
    ser.write(bytes(cmd, encoding='utf-8'))

    # read response
    response = ""
    count = 0
    while count < 5:
        line = read_line(ser)
        if line:
            response += line + '\n'
            count = 0
        else:
            count += 1

    return response

def _readchar(ser):
    """Read one character from the serial device "ser".

    Returns an empty string if no character read.
    """

    return str(bytes(ser.read()), encoding='utf-8')

def main():
    # get all attached DigiPot devices, check we have just one
    device = find_digivolt()

    # open a serial connection to the found DigiPot
    ser = serial.Serial(port=device.device, baudrate=115200)
    ser.timeout = 0.1   # nice short timeout
    if not check_connection(ser, device):
        print()
        print("Sorry, that's not a DigiPot device!?")
        os.exit(1)

    # do 'd;' command and print the results
    print(do_cmd(ser, 'd;'))


# check params, call main()
if len(sys.argv) != 1:
    print('Usage: dump_debug.py')
    sys.exit(1)

main()

