What is this?
=============

*DigiPot* is a small two channel potentiometer device.  It is designed
to interface to a controlling PC through the USB serial port.

Design
------

The AtMega32U4 microcontroller controls both channels of a MCP42xxx digital
potentiometer

The microcontroller will accept commands through the USB serial connection.
This will allow the device to be controlled by a program on a laptop.

Command Language
----------------

The commands for the serial USB connection are:

+------------+----------------------------------------------+
| Command    | Meaning                                      |
+============+==============================================+
| H;         | write help text to the console               |
+------------+----------------------------------------------+
| ID;        | return a device identifier string            |
+------------+----------------------------------------------+
| NC;        | get number of channels                       |
+------------+----------------------------------------------+
| W?;        | get wiper setting for channel '?'            |
+------------+----------------------------------------------+
| W?xxx;     | set wiper setting for channel '?'            |
+------------+----------------------------------------------+
| D;         | return some internal debug information       |
+------------+----------------------------------------------+

Schematic
---------

.. image:: DigiPot_1.0_Schematic.png

PCB
---

.. image:: PCB_1.0a.png

Software
--------

Python software to interface with DigiPot will be held in the *python*
subdirectory.
