//////////////////////////////////////////////////////////////////
// DigiPot
//
// A device with two controllable potentiometers.
//
// Must modify /Applications/Arduino.app/Contents/Java/hardware/arduino/avr/boards.txt
// to include this line "leonardo.build.usb_product="DigiPot" before uploading.  This
// is so the enumeration of USB devices will see "DigiPot".
//////////////////////////////////////////////////////////////////

#include <SPI.h>


// Voltage measurement program name & version
const char *ProgramName = "DigiPot";
const char *Version = "1.2";

// number of channels for this device
const int NumChannels = 2;

// ratio values for the channels
int Wipers[NumChannels] = {};

// set pin 17 as the slave select for the digital pot
// other SPI pins are as assumed on the Iota
const int SSPin = 17;     // B0 on the Iota
const int SCLKPin = 15;   // B1
const int MOSIPin = 16;   // B2

// channel activity pins (LED)
const int Act0 = 3;     // channel 0 activity LED - D0
const int Act1 = 2;     // channel 1 activity LED - D1

// command bytes for the digital pot
// switched since first DigiPot needed 0/1 switch - physical lead length
byte writePot0 = B00010010; // write to pot 0
byte writePot1 = B00010001; // write to pot 1

// buffer, etc, to gather external command strings
#define MAX_COMMAND_LEN   16
#define COMMAND_END_CHAR    ';'
char CommandBuffer[MAX_COMMAND_LEN + 1];
int CommandIndex = 0;

const char *ErrorMsg = "ERROR";
const char *OKMsg = "OK";

/////////////////////////////////////////////////////////////
// Convert a string to an integer.
/////////////////////////////////////////////////////////////

int string_to_int(char *str)
{
  int result = 0;

  while (*str)
  {
    int digit = *str - '0';

    if ((digit < 0) || (digit > 9))
      break;
      
    result *= 10;
    result += *str - '0';
    ++str;
  }

  return result;
}


/////////////////////////////////////////////////////////////
// Convert a string to uppercase in situ.
/////////////////////////////////////////////////////////////

void str2upper(char *str)
{
  while (*str)
  {
    *str = toupper(*str);
    ++str;
  }
}

/////////////////////////////////////////////////////////////
// Set the channel activity LED to a particular state.
/////////////////////////////////////////////////////////////

void setActivity(int channel, bool state)
{
  int pin = Act0;   // assume channel 0

  if (channel != 0)
    pin = Act1;

  if (state)
    digitalWrite(pin, HIGH);
  else
    digitalWrite(pin, LOW);
}

/////////////////////////////////////////////////////////////
// Set 'channel' pot to the given 'offset'.
/////////////////////////////////////////////////////////////

void digitalPotWrite(int channel, int offset)
{
  // handle multiple channels
  byte cmd = writePot0;

  if (channel != 0)
    cmd = writePot1;  // if it's not channel 0, assume channel 1

  setActivity(channel, true);

  // send value to the appropriate channel
  SPI.beginTransaction(SPISettings(10000000, MSBFIRST, SPI_MODE0));
  digitalWrite(SSPin, LOW);
  SPI.transfer(cmd);
  SPI.transfer(offset);
  digitalWrite(SSPin, HIGH);
  SPI.endTransaction();

  delay(5);     // slight delay so we can *see* the LED!
  setActivity(channel, false);
}


//##############################################################################
// External command routines.
//
// External commands are:
//     H;        send help text to console
//     ID;       get device identifier string
//     NC;       get number of channels
//     W?;       get ratio on channel '?'
//     W?xxx;    set ratio on channel '?' - 'xxx' is in range [0, 255]
//     D;        debug command - does something
//##############################################################################

//----------------------------------------
// Get help:
//     H;
//----------------------------------------

void xcmd_help(char *cmd)
{
  // to turn off warning about "unused parameter"
  cmd[0] = '\0';

  Serial.println("-----------Interactive Commands-----------------");
  Serial.println("H;       send help text to console");
  Serial.println("ID;      get device identifier string");
  Serial.println("NC;      get number of channels");
  Serial.println("W?;      get ratio for channel '?'");
  Serial.println("W?xxx;   set ratio on channel '?' - 'xxx' is in range [0, 255]");
  Serial.println("D;       print some internal debug information");
  Serial.println("------------------------------------------------");
}

//----------------------------------------
// Get the identifier string:
//     ID
//----------------------------------------

void xcmd_id(char *cmd)
{
  // if not legal, complain
  if (strcmp(cmd, "ID"))
  {
    Serial.println(ErrorMsg);
    return;
  }

  // generate ID string and return
  Serial.print(ProgramName);
  Serial.print(" ");
  Serial.println(Version);
}

//----------------------------------------
// Get the number of channels:
//     NC
//----------------------------------------

void xcmd_numchan(char *cmd)
{
  // if not legal, complain
  if (strcmp(cmd, "NC"))
  {
    Serial.println(ErrorMsg);
    return;
  }

  // generate answer string and return
  Serial.println(NumChannels);
}

//----------------------------------------
// Get/set wiper ratio for a channel:
//     W?
//     W?xxx
//----------------------------------------

void xcmd_wiper(char *cmd)
{
  // test if it's legal
  if (strlen(cmd) < 2)
  {
    Serial.println(ErrorMsg);
    return;
  }

  // get channel number, check legal
  int channel = cmd[1] - '0';
  if ((channel < 0) or (channel >= NumChannels))
  {
    Serial.println(ErrorMsg);
    return;
  }

  // test if it's a READ
  if (strlen(cmd) == 2)
  {
    // get and return the wiper value
    Serial.println(Wipers[channel]);
    return;
  }
  else
  {
    // must be a WRITE, get the required wiper value
    int new_wiper = string_to_int(cmd+2);

    // check if legal, ie, in [0,255]
    if (new_wiper < 0 || new_wiper > 255)
    {
      Serial.println(ErrorMsg);
      return;
    }

    // set wiper to that value, save in EEPROM
    Wipers[channel] = new_wiper;

    // then actually change the hardware device
    digitalPotWrite(channel, new_wiper);

    Serial.println(OKMsg);
  }
}

//----------------------------------------
// Perform debug:
//     D
//
// Put any debug code you want here.
// Could expand to "Dx" where the "x" char selects debug required.
//----------------------------------------

void xcmd_debug(char *cmd)
{
  // if not legal, complain
  if (strcmp(cmd, "D"))
  {
    Serial.println(ErrorMsg);
    return;
  }

  Serial.println("DigiPot state:");

  // return wiper ratio(s)
  for (int i = 0; i < NumChannels; ++i)
  {
    Serial.print("    Wiper ");
    Serial.print(i);
    Serial.print("=");
    Serial.println(Wipers[i]);
  }
}

//----------------------------------------
// Process an external command.
//     cmd     address of command string buffer
//     index   index of last char in string buffer
// 'cmd' is '\0' terminated.
//----------------------------------------
void do_external_cmd(char *cmd)
{
  // ensure everything is uppercase
  str2upper(cmd);

  // process the command
  switch (cmd[0])
  {
    case 'D':
      xcmd_debug(cmd);
      return;
    case 'H':
      xcmd_help(cmd);
      return;
    case 'I':
      xcmd_id(cmd);
      return;
    case 'N':
      xcmd_numchan(cmd);
      return;
    case 'W':
      xcmd_wiper(cmd);
      return;
  }

  Serial.println(ErrorMsg);
}

void do_external_commands(void)
{
  bool overflow = false;    // "true" if command buffer overflowed
  
  // gather any commands from the external controller
  while (Serial.available())
  {
    char ch = Serial.read();

    // ignore newlines
    if (ch == '\n')
      continue;

    if (CommandIndex < MAX_COMMAND_LEN)
    {
      // only add if command not over-length
      CommandBuffer[CommandIndex++] = ch;
    }
    else
      overflow = true;

    if (ch == COMMAND_END_CHAR)   // if end of command, execute it
    {
      if (overflow)
      {
        overflow = false;
        Serial.println("TOO LONG");
      }
      else
      {
        CommandBuffer[CommandIndex-1] = '\0'; // remove final ';'
        do_external_cmd(CommandBuffer);
        CommandIndex = 0;
      }
    }
  }
}


void setup(void)
{
  // set up the serial comms
  Serial.begin(115200);

  // set pin modes, etc
  pinMode(Act0, OUTPUT);
  pinMode(Act1, OUTPUT);
  pinMode(SSPin, OUTPUT);
  pinMode(SCLKPin, OUTPUT);
  pinMode(MOSIPin, OUTPUT);

  // start SPI
  digitalWrite(SSPin, HIGH);
  SPI.begin();

  // set wiper ratios to 0
  for (int channel = 0; channel < NumChannels; ++channel)
  {
    digitalPotWrite(channel, 0);
  }
  
  // exercise the channel activity LEDs
  setActivity(0, true);
  setActivity(1, true);
  delay(50);            // so we see the LEDs flash
  setActivity(0, false);
  setActivity(1, false);

  // wait for the Serial port to become alive
  // needed for Leonardo only
  while (!Serial)
    ;

  // announce
  Serial.print(ProgramName);
  Serial.print(" ");
  Serial.print(Version);
  Serial.println(" ready.");
  Serial.println("Enter 'H;' and <enter> to get help.");
}


void loop()
{
  do_external_commands();    // do any external commands
}
