This project is a small air quality sensor.

It uses the tiny PMS5003 sensor box and a PCB with USB 5 volts power,
an ESP8266 12F module and an LCD display.  A laser-cut acrylic case
will be used.
