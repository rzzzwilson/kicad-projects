This directory holds KiCad projects.

The "DigiVolt" directory holds the kicad data and arduino source code
for computer-controlled voltage measering device.

The "DigiPot" directory holds the computer controlled potentiometer.
Based on the DigiVolt code.

The "DigiPower" project is an implementation of the digital power meter
described in the May/June 2013 issue of QEX.

The "SquareWaveGenerator" is a simple square wave generator.

DigiOhm - an experiment in measuring resistance

LQFP48_Breakout - a breakout board for LQFP-48 components

PWKeep - a USB "password" device to type in passwords

DigiPotLDR - an experiment in ???

Kappa - a rebuild of Iota using an LQFP chip and 3.3v, with battery provision

PowRef - a "standard" source of known power signals, 0dBm and -10dBm

