# Filament 7 Segment Display

This is a test board to see if the "filament" LED strings can be used to make a
thinner/cheaper 7 segment display rather than the standard LED displays.

Both the standard LED displays and this test board can be driven by the MAX7219.

