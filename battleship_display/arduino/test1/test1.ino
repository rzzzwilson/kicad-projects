#include <SPI.h>
#include <Adafruit_NeoPixel.h>
#include "schedule.h"

///////////////////////////////////////////////////////
// Test code to show fadein/fadeout at various times.
// The times will eventually be at certain hours of the day,
// but initially will be at certain seconds in the minute.
//
// Uses a "schedule" approach, code in files schedule.*.
///////////////////////////////////////////////////////

// Attach a NeoPixel strip Din to the DataPin.
// Change NumLeds to the number of LEDs in the strip
//const int DataPin = 5;    // Uno
const int DataPin = D4;   // D1 Mini
const int NumXLeds = 10;
const int NumYLeds = 10;
const int NumLeds = NumXLeds * NumYLeds;
const int DisplayBrightness = 16; // display brightness

// define the pixels
Adafruit_NeoPixel pixels(NumLeds, DataPin, NEO_GRB + NEO_KHZ800);

uint8_t ships[NumYLeds][NumXLeds];

//*********************************************************
// Utility routines.
//*********************************************************

//------------------------------------
// Convert a Cartesian (x, y) to a strip index.
//------------------------------------

uint8_t xy2lin(uint8_t x, uint8_t y)
{
  uint8_t result;
  
  if (y % 2)
  {
    // "y" is odd
    result = NumXLeds+x + NumXLeds*(y-1);
  }
  else
  {
    // "y" is even
    result = NumXLeds-x-1 + NumXLeds*y;
  }
  
  return result;
}

//------------------------------------
// Show colour "c" on the display by
// scanning along each strip.
//------------------------------------

void show_colour(unsigned long c)
{
  for (int y=0; y < NumYLeds; ++y)
  {
    for (int x=0; x < NumXLeds; ++x)
    {
      pixels.clear();
      pixels.setPixelColor(xy2lin(x, y), c);
      pixels.show();
      delay(1);
    }
  }
}

//-----------------------------------
// Map a display code to a colour.
//------------------------------------

unsigned long map2colour(uint8_t c)
{
  switch (c)
  {
    case 1: // ship
      return pixels.Color(64, 64, 0);
    case 2: // miss
      return pixels.Color(16, 16, 16);
    case 3: // hit
      return pixels.Color(32, 0, 0);
  }

  return pixels.Color(0, 0, 0);
}

#define ShipColour  1
#define MissColour  2
#define HitColour   3

//------------------------------------
// Draw a cross of colour "c" on the display.
//------------------------------------

void draw_cross(unsigned long c)
{
  pixels.clear();

  for (int i = 0; i < NumXLeds; ++i)
  {
    pixels.setPixelColor(xy2lin(i, i), c);
    pixels.setPixelColor(xy2lin(NumXLeds-i-1, i), c);
  }

  pixels.show();
  delay(500);
  pixels.clear();
  pixels.show();
  delay(100);
}

void create_ships(void)
{
  for (int y = 0; y < NumYLeds; ++y)
  {
    for (int x = 0; x < NumXLeds; ++x)
    {
      ships[y][x] = 0;
    }
  }

  // carrier
  ships[8][0] = ShipColour;
  ships[7][0] = ShipColour;
  ships[6][0] = ShipColour;
  ships[5][0] = ShipColour;
  ships[4][0] = ShipColour;

  ships[7][0] = HitColour;

  // battleship
  ships[0][0] = ShipColour;
  ships[0][1] = ShipColour;
  ships[0][2] = ShipColour;
  ships[0][3] = ShipColour;

  ships[0][0] = HitColour;

  // cruiser
  ships[4][3] = ShipColour;
  ships[5][4] = ShipColour;
  ships[6][5] = ShipColour;

  ships[5][4] = HitColour;
  ships[6][5] = HitColour;  

  // destroyer
  ships[8][8] = ShipColour;
  ships[8][7] = ShipColour;

  // submarine
  ships[0][8] = ShipColour;

  ships[0][8] = HitColour;

  // misses
  ships[4][4] = MissColour;
  ships[9][1] = MissColour;
  ships[7][3] = MissColour;
  ships[1][2] = MissColour;
  ships[5][9] = MissColour;
}

void show_ships(void)
{
  pixels.clear();
  for (int y = 0; y < NumYLeds; ++y)
  {
    for (int x = 0; x < NumXLeds; ++x)
    {
      if (ships[y][x])
      {
        unsigned long c = map2colour(ships[y][x]);
        
        pixels.setPixelColor(xy2lin(x, y), c);
      }
    }
  }
  
  pixels.show();
}

//*********************************************************
// Setup
//*********************************************************

void setup()
{
  Serial.begin(115200);
  
 // initialize the NeoPixel strip
  pixels.begin();
  pixels.setBrightness(DisplayBrightness);  // set "static" brightness
  pixels.clear();
  pixels.show();
}

//*********************************************************
// Exercise the display.
//*********************************************************

void loop()
{
  pixels.clear();
  create_ships();
  show_ships();
  delay(2000);
  show_colour(pixels.Color(255, 0, 0));
  show_colour(pixels.Color(0, 255, 0));
  show_colour(pixels.Color(0, 0, 255));

  draw_cross(pixels.Color(31, 0, 0));
  draw_cross(pixels.Color(0, 31, 0));
  draw_cross(pixels.Color(0, 0, 31));
}
