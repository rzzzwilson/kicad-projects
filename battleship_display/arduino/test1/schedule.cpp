/*
 * Schedule
 *
 * Library to handle asynchronous "events" in code without using delay().
 *
 * An attempt has been made to make the system impervious to the millis()
 * rollover every 49+ days.  This should be considered to be untested.
 *
 * Rollover ideas stolen from:
 * https://arduino.stackexchange.com/questions/12587/how-can-i-handle-the-millis-rollover
 */

#include "schedule.h"

// max allowed value for the scheduled delay (half max overflow period)
// it may not be necessary to limit the delay like this, but this allows ~25 days delay
#define SCHED_MAXWAIT  0x7FFFFFFF

// stuff to handle the Event array
Event *EventArray = NULL;    // array of Event structs (dynamically allocated)
int NumEvents = 0;           // number of Event slots in the system
int NextSlot = 0;            // index of first free Event slot in "EventArray" array

// address of the user function to call on abort, if given.
event_handler AbortHandler = NULL;  	// address of user function

// address of user function to call whenever schedule table is empty
event_handler OnEmptyHandler = NULL;	// address of user function

//***************************************************************************
// Debug routine - dump contents of EventArray to Serial.
//     msg  message to print as part of the dump
//***************************************************************************

#ifdef SCHED_DEBUG
void Schedule::dump(const char *msg)
{
  Serial.println("EventArray ------------------------------------");
  Serial.print(msg);
  Serial.print("| NextSlot=");
  Serial.println(NextSlot);
  Serial.println("-----------------------------------------------");
  
  for (int i=0; i < NextSlot; ++i)
  {
    Serial.print("i: ");
    Serial.print(i);
    Serial.print(", .start=");
    Serial.print(EventArray[i].start);
    Serial.print(", .period=");
    Serial.print(EventArray[i].period);
    Serial.print(", handler=");
    Serial.println((int) EventArray[i].handler);
  }
  
  Serial.println("===============================================");
}
#endif


//***************************************************************************
// Initialize the "schedule" system.
//     abort_handler  address of handler function on abort
//     num_events     required slots in the EventArray array
// If "abort_handler" is NULL the handler will not be called on abort.
//***************************************************************************

Schedule::Schedule(event_handler abort_handler, int num_events)
{
  NextSlot = 0;
  AbortHandler = abort_handler;
  NumEvents = num_events;
  EventArray = (Event *) malloc(sizeof(Event) * NumEvents);
  if (EventArray == NULL)  // can't allocate memory?
  {
    abort("Can't allocate memory for scheduler!");
  }
}


//***************************************************************************
// The "destructor" function.
//
// Just free dynamically allocated memory.
//***************************************************************************

Schedule::~Schedule(void)
{
  free(EventArray);
}


//***************************************************************************
// Routine to flush events from the "schedule" system.
//
// All events are removed from the system, abort handler and table size
// remain unchanged.
//***************************************************************************

void Schedule::flush(void)
{
  NextSlot = 0;  
}


//***************************************************************************
// Routine to abort the system.
//     msg  message to print
//
// Call the user abort handler if it was defined.
// If not defined, loop forever.
//***************************************************************************

void Schedule::abort(const char *msg)
{
  Serial.println(msg);

  if (AbortHandler != NULL)
  {
    AbortHandler();
  }

  // if we get here, wait forever
  while (1);
}

//***************************************************************************
// Routine to schedule an Event in the system.
//     period   delay in ms to wait from now before event is triggered
//     handler  address of handler function to call when event is triggered
//***************************************************************************

void Schedule::schedule(unsigned long period, event_handler handler)
{
  // if called incorrectly, abort
  if (period > SCHED_MAXWAIT)
  {
    abort("Schedule period too long.");
  }

  // check if there are any free slots, if not , abort
  if (NextSlot >= NumEvents)
  {
    abort("Too many slots used in the schedule table.");
  }

  // put new at next unused slot
  EventArray[NextSlot].start = millis();
  EventArray[NextSlot].period = period;
  EventArray[NextSlot].handler = handler;

  ++NextSlot;
}

//***************************************************************************
// The "tick" function.  Should be called periodically in user code to check
// if an event needs to be triggered.
//***************************************************************************

void Schedule::tick(void)
{
#ifdef SCHED_DEBUG
  dump("Schedule::tick()");
#endif

  // check if table empty, optinally call handler if set
  if (NextSlot == 0)
  {
    if (OnEmptyHandler)
    {
      OnEmptyHandler();
    }

    return;   // no point in searching, just return
  }
  
  // look for first slot with non-due event
  int slot = 0;                         // start searching at bottom
  
  while (slot < NextSlot)
  {
    // complicated comparison to handle millis() overflow
    if (millis() - EventArray[slot].start > EventArray[slot].period)
    {
      // going to execute this slot, get event handler
      event_handler handler = EventArray[slot].handler;

      // remove event at this slot, move others down
      memmove(&EventArray[slot], &EventArray[slot+1], sizeof(Event) * (NextSlot - slot - 1));
      --NextSlot;           // decrease slot count

      // execute handler
      handler();
    }
    else
    {
      // slot not executed, bump "slot" to search next slot
      ++slot;
    }
  }
}

//***************************************************************************
// Register a handler to be called whenever the schedule table becomes empty.
// 
// handler  the event handler when the table goes empty
//
// The "empty" event handler is called when the "tick()" method finds the
// schedule table is empty.
//***************************************************************************

void Schedule::on_empty(event_handler handler)
{
#ifdef SCHED_DEBUG
  dump("Schedule::on_empty()");
#endif

  OnEmptyHandler = handler;
}
