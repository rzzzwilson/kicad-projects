#!/usr/bin/env python3

"""
A program to measure a DigiPotLDR response curve.

Usage:  measure_DigiPotLDR [-h] <data file>

where -h           prints this help, and
      -l <str>     defines the LDR string (default "LDR")
      <data file>  is the name of the data file to be written
"""

import os
import sys
import time
import getopt
import serial
import usb
from serial.tools.list_ports import comports
import traceback

import digithing
import logger
log = logger.Log('measure_DigiPotLDR.log', logger.Log.DEBUG)

# settling time for DigiPotLDR device
LDRSettleTime = 0.03    # 30 msec

# the known value of the shunt resistor
RShunt = 995


def measure_DigiPotLDR(path, ldr_id, dv, dpl):
    """Make measurements on the DigiPotLDR device.

    path    path to data file to write
    ldr_id  string identifying the test
    dv      the DigiVolt device to use
    dpl     the DigiPotLDR device to use
    """

    # set DigiVolt channels to appropriate range
    dv.do_cmd('r0h;')
    log(f"r0; -> {dv.do_cmd('r0;')}")
    dv.do_cmd('r1l;')
    log(f"r1; -> {dv.do_cmd('r1;')}")

    log(f"measure_DigiPotLDR: opening file '{path}' for writing")
    with open(path, 'w') as fd:
        fd.write(f'{ldr_id}\n')

        for b_val in range(0, 256):
            # set LDR to value
            response = dpl.do_cmd(f'b{b_val};').strip()
            log(f'dpl: set to {b_val}, response={response}')

            time.sleep(LDRSettleTime)   # wait for LDR to settle

            # get voltage across the LDR (Vcc)
            Vcc = dv.do_cmd('v0;').strip()
            Vcc = float(Vcc)

            # get voltage across the shunt resistor (V?)
            Vunknown = dv.do_cmd(f'v1;').strip()
            Vunknown = float(Vunknown)
            print(f'b_val={b_val}, Vcc={Vcc}, Vunknown={Vunknown}, ', end='')
            if Vunknown < 0.001:    # stop "divide by zero" exceptions
                Vunknown = 0.001

            # calculate the LDR resistance
            # R? = (RShunt*Vcc)/V? - RShunt
            r_ldr = (RShunt * Vcc)/Vunknown - RShunt
            print(f'{r_ldr:.2f}')
            r_ldr = r_ldr/1024

            fd.write(f'{b_val},{r_ldr:.2f}\n')
            fd.flush()

def find_required_devices():
    """Find and return the required devices."""

    used_ports = []
    
    print('Looking for DigiPotLDR device ... ', end='')
    sys.stdout.flush()
    dpl = None
    try:
        dpl = digithing.DigiThing('DigiPotLDR', ignore=used_ports)
    except digithing.DigiThingError:
        print('No DigiPotLDR devices found')
    else:
        print(f"found: {dpl.do_cmd('id;').strip()}")
    
        used_ports.append(dpl.port)
    
    # now look for a single DigiVolt device
    print('Looking for DigiVolt device ... ', end='')
    sys.stdout.flush()
    dv = None
    try:
        dv = digithing.DigiThing('DigiVolt', ignore=used_ports)
    except digithing.DigiThingError:
        print('No DigiVolt devices found')
    else:
        print(f"found: {dv.do_cmd('id;').strip()}")
    
        used_ports.append(dv.port)

    if dv is None or dpl is None:
        if dv is None:
            raise Exception("Can't find a DigiVolt device!?")
        raise Exception("Can't find a DigiPotLDR device!?")

    return (dv, dpl)

def usage(msg=None):
    """Print the module docstring plus the optional message."""

    if msg:
        print('-' * 60)
        print(msg)
        print('-' * 60, '\n')
    print(__doc__)

def excepthook(type, value, tback):
    """Capture any unhandled exceptions."""

    msg = '\n' + '=' * 80
    msg += '\nUncaught exception:\n'
    msg += ''.join(traceback.format_exception(type, value, tback))
    msg += '=' * 80 + '\n'
    log(msg)
    print(msg)

# plug our handler into the python system
sys.excepthook = excepthook

# check the command line args
try:
    opts, args = getopt.gnu_getopt(sys.argv, "hl:", ["help", "ldr="])
except getopt.GetoptError:
    usage('Bad option')
    sys.exit(10)

ldr_id = 'LDR'

for opt, arg in opts:
    if opt in ("-h", "--help"):
        usage()
        sys.exit(0)
    elif opt in ("-l", "--ldr"):
        ldr_id = arg

if len(args) != 2:
    usage()
    sys.exit(10)
    
filename = args[1]
if os.path.exists(filename):
    print(f"Sorry, file '{filename}' exists - won't overwrite")
    sys.exit(10)

# check we have the required devices online
(dv, dpl) = find_required_devices()

# start the measurements, save data to given file
measure_DigiPotLDR(filename, ldr_id, dv, dpl)
