import sys
import os.path
import matplotlib.pyplot as plt

try:
    datafile = sys.argv[1]
except IndexError:
    print('usage: plot.py <datafile>')
    sys.exit(1)

(outfile, _) = os.path.splitext(os.path.basename(datafile))
outfile += '.png'

x_data = []
y_data = []
with open(datafile, 'r') as fd:
    ldr = next(fd).strip()

    for line in fd:
        if line.startswith('#'):
            continue
        (x, y) = map(float, line.strip().split(','))
        x_data.append(x)
        y_data.append(y)

plt.plot(x_data, y_data)
plt.xlabel('AnalogWrite() value')
plt.ylabel('DigiPotLDR Resistance (kΩ)')
plt.title('DigiPotLDR Resistance vs Voltage setting')
annotation_str = f'{ldr} LDR'
plt.annotate(annotation_str, xy=(0.99, 0.96), xycoords='axes fraction', ha='right')
plt.savefig(outfile)
plt.show()
