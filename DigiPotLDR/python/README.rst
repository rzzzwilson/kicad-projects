This directory holds the python code used to orchestrate the DigiPotLDR
and DigiVolt devices to measure the Brightness v. Resistance curve
of a test DigiPotLDR device.
