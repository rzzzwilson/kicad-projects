//////////////////////////////////////////////////////////////////
// DigiPotLDR
//
// Code to test the LED+LDR device.
// Circuit is shown in "Schematic.png".
//////////////////////////////////////////////////////////////////


// Voltage measurement program name & version
const char *ProgramName = "DigiPotLDR";
const char *Version = "1.0";

// buffer, etc, to gather external command strings
#define MAX_COMMAND_LEN   16
#define COMMAND_END_CHAR    ';'
char CommandBuffer[MAX_COMMAND_LEN + 1];
int CommandIndex = 0;

const char *ErrorMsg = "ERROR";
const char *OKMsg = "OK";

const int GreenLED = 13; // the C7 pin on Iota

int Brightness = 0;    // the LED brightness value [0,255]

/////////////////////////////////////////////////////////////
// Convert a string to an integer.
/////////////////////////////////////////////////////////////

int string_to_int(char *str)
{
  int result = 0;

  while (*str)
  {
    int digit = *str - '0';

    if ((digit < 0) || (digit > 9))
      break;
      
    result *= 10;
    result += *str - '0';
    ++str;
  }

  return result;
}

/////////////////////////////////////////////////////////////
// Convert a string to uppercase in situ.
/////////////////////////////////////////////////////////////

void str2upper(char *str)
{
  while (*str)
  {
    *str = toupper(*str);
    ++str;
  }
}

//##############################################################################
// External command routines.
//
// External commands are:
//     H;   send help text to console
//     B;   set or read brightness
//     ID;  display ID string
//##############################################################################

//----------------------------------------
// Get help:
//     H;
//----------------------------------------

void xcmd_help(char *cmd)
{
  // to turn off warning about "unused parameter"
  cmd[0] = '\0';

  Serial.println("-----------Interactive Commands-----------------");
  Serial.println("H;       send help text to console");
  Serial.println("ID;      send ID string to console");
  Serial.println("Bxxx;    set LED brightness [0,255]");
  Serial.println("B;       read LED brightness");
  Serial.println("------------------------------------------------");
}

//----------------------------------------
// Perform debug:
//     B
//     Bxxx
//
// Set/read LED brightness.
//----------------------------------------

void xcmd_bright(char *cmd)
{
  // test if it's a READ
  if (strlen(cmd) == 1)
  {
    // just return current brightness
    Serial.println(Brightness);
    return;
  }

  // otherwise it's setting the brightness
  int new_bright = string_to_int(cmd+1);

  if (new_bright < 0 || new_bright > 255)  // check if legal, ie, in [0,255]
  {
    Serial.println(ErrorMsg);
    return;
  }

  Brightness = new_bright;              // set the brightness
  analogWrite(GreenLED, Brightness);

  Serial.print("Brightness set to ");
  Serial.println(Brightness);
//  Serial.println(OKMsg);
}

//----------------------------------------
// Get help:
//     ID;
//----------------------------------------

void xcmd_id(char *cmd)
{
  // test if it's a valid ID command
  if (strlen(cmd) != 2 || strcmp(cmd, "ID") != 0)
  {
    Serial.println(ErrorMsg);
    return;
  }

  // otherwise return the ID string
  Serial.print(ProgramName);
  Serial.print(" ");
  Serial.println(Version);
}

//----------------------------------------
// Process an external command.
//     cmd     address of command string buffer
//     index   index of last char in string buffer
// 'cmd' is '\0' terminated.
//----------------------------------------
void do_external_cmd(char *cmd)
{
  // ensure everything is uppercase
  str2upper(cmd);

  // process the command
  switch (cmd[0])
  {
    case 'B':
      xcmd_bright(cmd);
      return;
    case 'H':
      xcmd_help(cmd);
      return;
    case 'I':
      xcmd_id(cmd);
      return;
  }

  Serial.println(ErrorMsg);
}

void do_external_commands(void)
{
  bool overflow = false;    // "true" if command buffer overflowed
  
  // gather any commands from the external controller
  while (Serial.available())
  {
    char ch = Serial.read();

    // ignore newlines
    if (ch == '\n')
      continue;

    if (CommandIndex < MAX_COMMAND_LEN)
    {
      // only add if command not over-length
      CommandBuffer[CommandIndex++] = ch;
    }
    else
      overflow = true;

    if (ch == COMMAND_END_CHAR)   // if end of command, execute it
    {
      if (overflow)
      {
        overflow = false;
        Serial.println("TOO LONG");
      }
      else
      {
        CommandBuffer[CommandIndex-1] = '\0'; // remove final ';'
        do_external_cmd(CommandBuffer);
        CommandIndex = 0;
      }
    }
  }
}


void setup(void)
{
  // set up the serial comms
  Serial.begin(115200);

  // set pin modes, etc
  pinMode(GreenLED, OUTPUT);

  Brightness = 0;
  analogWrite(GreenLED, Brightness);

  // wait for the Serial port to become alive
  // needed for Leonardo only
  while (!Serial)
    ;

  Serial.print(ProgramName);
  Serial.print(" ");
  Serial.println(Version);
}


void loop()
{
  do_external_commands();    // do any external commands
}
