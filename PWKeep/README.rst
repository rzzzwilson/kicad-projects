UPDATE
======

This project has been moved and renamed.  After finding more uses for the basic
hardware I realized that it should be renamed.  The hardware design is now
called *Ducky* and has been moved to
`its own GitLab repository <https://gitlab.com/rzzzwilson/ducky>`_.
The firmware projects that run on *Ducky* are all there as well.

This repository will remain here until I decide that I haven't forgotten
anything.


PWKeep
======

A simple little "AtMega32U4 on a USB stick" thing that can be used to inject
passwords through the USB Keyboard interface.  Or anything else you want!

Directories
-----------

+---------------+--------------------------------------+
| Directory     | Usage                                |
+===============+======================================+
| PWKeep        | The arduino code for "count" version |
+---------------+--------------------------------------+
| PWMorse       | The arduino code for "morse" version |
+---------------+--------------------------------------+
| kicad         | The kicad project for the PCB.       |
+---------------+--------------------------------------+
| kicad/gerbers | The kicad gerber files for the PCB.  |
+---------------+--------------------------------------+

Circuit
-------

The schematic shows the device to be very simple, hardware-wise.
Just an AtMega32U4 with support components and a pushbutton:

.. image:: PWKeep_2.0_Schematic.png

Most of the complexity is in the firmware.

PCB
---

The final PCB is:

.. image:: PWKeep_2.0_PCB.png

Update
------

1 May 2021: Finally built the v1.1 version with the QFN form chip.
Took a while, but it works!

.. image:: PWKeep.JPG 

Firmware
--------

Update: The firmware has two versions, one that accepts a number of short pushes,
and a version that recognizes morse alphabetic characters.  The doc below is the
PWMorse version that recognizes morse code letters on the pushbutton.


The firmware recognizes these types of events:

+------------------+---------+--------------------------+
| Push type        | Period  | Meaning                  |
+==================+=========+==========================+
| Short push       | *none*  | Morse "dot"              |
+------------------+---------+--------------------------+
| Long push        | >200ms  | Morse "dash"             |
+------------------+---------+--------------------------+
| Timeout          | >1000ms | Resets the state machine |
+------------------+---------+--------------------------+

There are also interactive commands that may be executed through the Arduino
IDE Serial console:

+---------+--------------------------------------+
| Command | Meaning                              |
+=========+======================================+
| H;      | Print help text to console.          |
+---------+--------------------------------------+
| ID;     | Get device identifier string.        |
+---------+--------------------------------------+
| CLEAN;  | Delete saved passwords.              |
+---------+--------------------------------------+
| Da;     | Delete password 'a'.                 |
+---------+--------------------------------------+
| P;      | Display all saved passwords.         |
+---------+--------------------------------------+
| Pa;     | Display password 'a'.                |
+---------+--------------------------------------+
| Paxxx;  | Set password 'a' to 'xxx'.           |
+---------+--------------------------------------+
| R;      | Reboot the device.                   |
+---------+--------------------------------------+

There may be a CLI python program that will allow executing the above serial
commands.
