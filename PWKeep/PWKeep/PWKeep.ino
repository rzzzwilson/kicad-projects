//================================================================
// PWKeep
//
// A small program to store passwords and send them to
// the host as keyboard input.
//
// This version recognizes these events:
//     * a short push (< 300msec),
//     * a long push (< 1000msec), and
//     * a timeout (2500msec of no action).
//
// The program remembers 10 passwords in memory, numbered 0, ..., 9.
// A long push sends the password matching the number of short
// pushes that occur before the long push (0, ..., 9).
// A timeout resets the recognizer.A
//
// Must modify /Applications/Arduino.app/Contents/Java/hardware/arduino/avr/boards.txt
// to include this line "leonardo.build.usb_product="PWKeep" before uploading.  This
// is so the enumeration of USB devices will see "PWKeep".
//================================================================

#include <Arduino.h>
#include <EEPROM.h>
#include <Keyboard.h>
#include <avr/wdt.h>                    // for the soft-boot code

#define DEBUG                 0         // 0 - production, 1 - debugging

#define PRODUCT_NAME          "PWKeep"
#define VERSION               "1.3"

// which pin the button is attached to
#define ButtonPin             3         // Iota D0 pin

// define the debounce counter
#define DebounceCount         20        // number of millis/samples debounce delay

// set the "not set" default password
#define PasswordNotSet        "pw%d"

//definitions of times for long click and timeout (milliseconds)
#define LongClickTime         300
#define TimeoutPeriod         2500

// Finite State Machine events
#define EVENT_TIMEOUT         0         /* no activity for a period */
#define EVENT_SHORTPUSH       1         /* a short push */
#define EVENT_LONGPUSH        2         /* a longer push */
#define EVENT_RLONGPUSH       3         /* a REALLY long push */

// macro to turn off warnings about "unused parameter" in functions
#define UNUSED(v) (void) (v)

// OK and error message
const char *OKMsg = "OK";
const char * ErrorMsg = "ERROR";

// place to save EEPROM data CRC
unsigned long Checksum = 0;

// the password storage structure
#define MaxPasswordLen    64      /* max length of passwords */
#define NumPasswords      10      /* number of saved passwords */

char Passwords[NumPasswords][MaxPasswordLen + 1];

// define start addresses in EEPROM of various fields
const int EepromChecksum = 0;
const int EepromPasswords = EepromChecksum + sizeof(Checksum);

#define EepromPasswordSize  (sizeof(Passwords))

// buffer, etc, to gather external command strings
#define MAX_COMMAND_LEN     80
#define COMMAND_END_CHAR    ';'
char CommandBuffer[MAX_COMMAND_LEN + 1];
int CommandIndex = 0;

// buffer to build dynamic messahes in
char DynamicMsg[128];


//##############################################################################
// Utility routines.
//##############################################################################

//----------------------------------------
// Calculate the CRC of the saved passwords in EEPROM
//----------------------------------------

unsigned long eeprom_crc(void)
{

  const unsigned long crc_table[16] = {0x00000000, 0x1db71064, 0x3b6e20c8, 0x26d930ac,
                                       0x76dc4190, 0x6b6b51f4, 0x4db26158, 0x5005713c,
                                       0xedb88320, 0xf00f9344, 0xd6d6a3e8, 0xcb61b38c,
                                       0x9b64c2b0, 0x86d3d2d4, 0xa00ae278, 0xbdbdf21c
                                      };
  unsigned long crc = ~0L;

  for (unsigned long index = EepromPasswords ; index < EepromPasswordSize; ++index)
  {
    crc = crc_table[(crc ^ EEPROM[index]) & 0x0f] ^ (crc >> 4);
    crc = crc_table[(crc ^ (EEPROM[index] >> 4)) & 0x0f] ^ (crc >> 4);
    crc = ~crc;
  }

  return crc;
}

//----------------------------------------
// Saves the in-memory password data to EEPROM and refreshes the CRC.
//----------------------------------------

void save_eeprom_data(void)
{
  // save in-memory passwords to EEPROM
  EEPROM.put(EepromPasswords, Passwords);

  // calculate CRC of the data
  Checksum = eeprom_crc();
  EEPROM.put(EepromChecksum, Checksum);
}

//----------------------------------------
// Gets the password data in EEPROM and refreshes the RAM copies.
//
// If the EEPROM checksum is invalid, initializes EEPROM and sets
// in-memory copy to initialized values.
//----------------------------------------

void restore_eeprom_data(void)
{
  // get data checksun
  EEPROM.get(EepromChecksum, Checksum);

  // get actual CRC of data and ensure same
  if (Checksum != eeprom_crc())
  {
    Serial.println("Initializing EEPROM");

    // initialize passwords
    for (int pass_num = 0; pass_num < NumPasswords; ++pass_num)
    {
      sprintf(DynamicMsg, "pw%d", pass_num);
      strcpy(Passwords[pass_num], DynamicMsg);
    }

    // and save initialized data
    save_eeprom_data();
  }

  // get data stored in EEPROM
  EEPROM.get(EepromChecksum, Checksum);
  EEPROM.get(EepromPasswords, Passwords);
}

//----------------------------------------
// Code to reboot an AtMega32U4.
//
// From:
// https://blog.fsck.com/2014/08/how-to-reboot-an-arduino-leonardo-micro-into-the-bootloader.html
//----------------------------------------

void reboot(void)
{
  // [...]
  // Set the magic bits to get a Caterina-based device
  // to reboot into the bootloader and stay there, rather
  // than run move onward
  //
  // These values are the same as those defined in
  // Caterina.c

  uint16_t bootKey = 0x7777;
  uint16_t *const bootKeyPtr = (uint16_t *) 0x0800;

  // Stash the magic key
  *bootKeyPtr = bootKey;

  // Set a watchdog timer
  wdt_enable(WDTO_120MS);

  while (1) {} // This infinite loop ensures nothing else
  // happens before the watchdog reboots us
}

//----------------------------------------
// Convert a string to uppercase, in situ.
//----------------------------------------

void str2upper(char *str)
{
  while (*str)
  {
    *str = toupper(*str);
    ++str;
  }
}

//##############################################################################
// The recognizer routines and password processor.
//##############################################################################

//----------------------------------------
// Send a password matching the number.
//
// Do nothing if 'pass_num' out of range.
//----------------------------------------

void send_password(int pass_num)
{
  if (pass_num < NumPasswords)
  {
    Keyboard.print(Passwords[pass_num]);
  }
}

//----------------------------------------
// Decode an event number to a string.
//----------------------------------------

char * decode_event(int event)
{
  switch (event)
  {
    case EVENT_TIMEOUT:   return (char *) "TIMEOUT";
    case EVENT_SHORTPUSH: return (char *) "SHORTPUSH";
    case EVENT_LONGPUSH:  return (char *) "LONGPUSH";
    case EVENT_RLONGPUSH: return (char *) "RLONGPUSH";
  }

  sprintf(DynamicMsg, "Unknown event number %d", event);
  return DynamicMsg;
}

//----------------------------------------
// Accept an event and perform the appropriate action.
//----------------------------------------

void fsm(int event)
{
  static int fsm_push_count = 0;

  // decide what to do
  switch (event)
  {
    case EVENT_SHORTPUSH:
      ++fsm_push_count;
      break;
    case EVENT_LONGPUSH:
      send_password(fsm_push_count);
      fsm_push_count = 0;
      break;
    case EVENT_TIMEOUT:
      fsm_push_count = 0;
      break;
    case EVENT_RLONGPUSH:
      fsm_push_count = 0;
      break;
    default:
      fsm_push_count = 0;
      break;
  }
}

//----------------------------------------
// Handle a button state change.
//
// Decide if we have a SINGLE CLICK, LONG CLICK, REALLY LONG CLICK
// or a TIMEOUT event.
// Call the appropriate handler once we have an event.
//----------------------------------------

void change_button_state(int state)
{
  // state variables to determine if single/long click
  static unsigned long start_click = 0L;    // time of start of click

  unsigned long now = millis();

  if (state)
  {
    // button UP
    if ((now - start_click) <= LongClickTime)
    {
      // a short click
      fsm(EVENT_SHORTPUSH);

      start_click = 0L;
    }
    else
    {
      // a long click
      fsm(EVENT_LONGPUSH);
    }
  }
  else
  {
    // button DOWN
    start_click = now;
  }
}

//----------------------------------------
// Function to handle polling the button - includes debounce.
//
// Passes new (debounced) state to change_button_state() function.
//
// Code here from: https://playground.arduino.cc/Learning/SoftwareDebounce/
//----------------------------------------

unsigned long next_timeout = 0;  // time TIMEOUT event will be raised

void poll_button(void)
{
  // static variables to handle software debounce
  static int current_state = HIGH;        // the debounced input value
  static int counter = 0;                 // how many times we have seen new value
  static unsigned long poll_time = 0;     // the last time the output pin was sampled

  unsigned long now = millis();           // the current time

  if (now > poll_time)
  {
    poll_time = now + 1;                  // so we don't poll again until second millisecond

    // if button now UNPRESSED and long period elapsed, raise TIMEOUT
    if ((current_state == HIGH) && (next_timeout < now))
    {
      fsm(EVENT_TIMEOUT);
      next_timeout = now + TimeoutPeriod;
    }

    int reading = digitalRead(ButtonPin); // current pin state

    if (reading == current_state && counter > 0)
    {
      counter--;
    }
    if (reading != current_state)
    {
      counter++;
    }

    // If the pin has shown the same value for long enough let's switch it
    if (counter >= DebounceCount)
    {
      counter = 0;
      current_state = reading;
      change_button_state(reading);
      next_timeout = now + TimeoutPeriod;   // reset timeout counter
    }
  }
}

//##############################################################################
// Code for the "external" commands from the serial port.
//##############################################################################

//----------------------------------------
// Get help:
//     H;
//----------------------------------------

void xcmd_help(char *cmd)
{
  UNUSED(cmd);

  Serial.println("-----------Interactive Commands-----------------");
  Serial.println("H;       send help text to console");
  Serial.println("ID;      get device identifier string");
  Serial.println("CLEAN;   destroy saved password information");
  Serial.println("Pn;      print password 'n'");
  Serial.println("Pnxxx;   set password 'n' to 'xxx'");
  Serial.println("R;       reboot the device");
  Serial.println("------------------------------------------------");
}

//----------------------------------------
// Get the identifier string:
//     ID
//----------------------------------------

void xcmd_id(char *cmd)
{
  // if not legal, complain
  if (strcmp(cmd, "ID"))
  {
    Serial.println(ErrorMsg);
    return;
  }

  // generate ID string and return
  Serial.print(PRODUCT_NAME);
  Serial.print(" ");
  Serial.println(VERSION);
}

//----------------------------------------
// Destroy any saved password information
//     CLEAN
//----------------------------------------

void xcmd_clean(char *cmd)
{
  // if not legal, complain
  if (strcmp(cmd, "CLEAN"))
  {
    Serial.println(ErrorMsg);
    return;
  }

  // ensure in-memory buffers clean and ALL EEPROM bytes wiped
  for (int pass_num = 0; pass_num < NumPasswords; ++pass_num)
  {
    for (int i = 0; i < MaxPasswordLen; ++i)
    {
      Passwords[pass_num][i] = ' ';
    }
  }
  save_eeprom_data();

  // set in-memory buffers to "not set" state and save to EEPROM
  for (int pass_num = 0; pass_num < NumPasswords; ++pass_num)
  {
    sprintf(DynamicMsg, "pw%d", pass_num);
    strcpy(Passwords[pass_num], DynamicMsg);
  }
  save_eeprom_data();

  Serial.println(OKMsg);
}

//----------------------------------------
// Set/Get password string
//     Pn;      get password 'n'
//     Pnxxxx;  set password 'n'
//
// Because we have a password here, the 'cmd' string has
// not been converted to uppercase.
//----------------------------------------

void xcmd_password(char *cmd)
{
  // if too short, ERROR!
  if (strlen(cmd) < 2)
  {
    Serial.println(ErrorMsg);
    return;
  }

  // get password number
  int pass_num = cmd[1] - '0';

  // are we getting a password?
  if (strlen(cmd) == 2)
  {
    if (pass_num < NumPasswords)
    {
      Serial.print(pass_num);
      Serial.print(": '");
      Serial.print(Passwords[pass_num]);
      Serial.println("'");
    }
    else
    {
      Serial.println(ErrorMsg);
    }

    return;
  }
  else
  {
    // setting a password, get  new password
    char *new_password = &cmd[2];

    if (strlen(new_password) >= MaxPasswordLen)
    {
      Serial.println(ErrorMsg);
      return;
    }

    if (pass_num < NumPasswords)
    {
      strcpy(Passwords[pass_num], new_password);
    }
    else
    {
      Serial.println(ErrorMsg);
      return;
    }

    save_eeprom_data();
  }

  Serial.println(OKMsg);
}

//----------------------------------------
// Perform a hard reboot:
//     R
//
// Reboot the device.  No need to "push the button".
//----------------------------------------

void xcmd_reboot(char *cmd)
{
  // if not legal, complain
  if (strcmp(cmd, "R"))
  {
    Serial.println(ErrorMsg);
    return;
  }

  // OK, reboot this thing (AtMega32U4 bootloader)
  Serial.println("Rebooting...");
  reboot();
}

//----------------------------------------
// Process an external command.
//     cmd     address of command string buffer
//
// 'cmd' is '\0' terminated.
//----------------------------------------

void do_extern_cmd(char *cmd)
{
  char orig_cmd[strlen(cmd) + 1];
  strcpy(orig_cmd, cmd);

  // ensure everything is uppercase
  str2upper(cmd);

  // process the command
  switch (cmd[0])
  {
    case 'H':
      xcmd_help(cmd);
      return;
    case 'I':
      xcmd_id(cmd);
      return;
    case 'C':
      xcmd_clean(cmd);
      return;
    case 'P':
      // must pass original command - don't uppercase password!
      xcmd_password(orig_cmd);
      return;
    case 'R':
      xcmd_reboot(cmd);
      return;
  }

  Serial.println(ErrorMsg);
}

//----------------------------------------
// Handle characters coming in on the Serial port.
//
// Call the command handler when a complete command is received.
//----------------------------------------

void do_external_commands(void)
{
  bool overflow = false;    // "true" if command buffer overflowed

  // gather any commands from the external controller
  while (Serial.available())
  {
    char ch = Serial.read();

    // ignore newlines
    if (ch == '\n')
      continue;

    if (CommandIndex < MAX_COMMAND_LEN)
    {
      // only add if command not over-length
      CommandBuffer[CommandIndex++] = ch;
    }
    else
      overflow = true;

    if (ch == COMMAND_END_CHAR)   // if end of command, execute it
    {
      if (overflow)
      {
        overflow = false;
        Serial.println("Command too long!");
      }
      else
      {
        CommandBuffer[CommandIndex - 1] = '\0'; // remove final ';'
        do_extern_cmd(CommandBuffer);
        CommandIndex = 0;
      }
    }
  }
}

//----------------------------------------
// setup()
//
// Prepare pin mode and set current button state.
//----------------------------------------

void setup(void)
{
  // start Serial and wait for AtMega32U4 chip, Leonardo, etc.
  Serial.begin(115200);
#if defined(__AVR_ATmega32U4__) && DEBUG
  delay(3500);    // required on AtMega32U4 to show start text
#endif

  // set the "timeout" counter
  next_timeout = millis() + TimeoutPeriod;

  // set pin mode for the button
  pinMode(ButtonPin, INPUT_PULLUP);  // may not need this, test

  // prepare keyboard HID
  Keyboard.begin();

  // get data from EEPROM
  restore_eeprom_data();

  // announce the start
#if DEBUG
  Serial.print("PWKeep ");
  Serial.println(VERSION);
#endif
}

//----------------------------------------
// The loop() function.
//
// Just polls the button and handles any Serial communication.
//----------------------------------------

void loop(void)
{
  // check the button, do something if required
  poll_button();

  // handle any commands from the serial port
  do_external_commands();
}
