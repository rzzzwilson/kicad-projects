#!/usr/bin/env python3

"""
A program to configure a PWKeep device running the PWMorse firmware,
v1.6 or later.
"""

import sys
from digithing import DigiThing, DigiThingError

# look for a single PWMorse device
used_ports = []

# looking for a PWMorse device
try:
    pwm = DigiThing('PWMorse', ignore=used_ports)
except DigiThingError:
    print('No PWMorse devices found.')
    print('Attach the PWMorse dongle and try again.')
    sys.exit(1)

print(f"Found: {pwm.do_cmd('id;').strip()}")
print()

# show the allowed commands
print(f"{pwm.do_cmd('h;')}")

# now loop, get and execute internal commands
try:
    while True:
        cmd = input('\ncmd> ')
        print()
        if not cmd.endswith(';'):
            cmd += ';'
        result = pwm.do_cmd(cmd)
        print(result)
except (KeyboardInterrupt, EOFError, DigiThingError):
    pwm.close()
    print()
