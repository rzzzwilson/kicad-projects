//================================================================
// PWMorse
//
// A small program to store passwords and send them to the host as keyboard input.
//
// This version recognizes these events:
//     * a short push (< 200msec),
//     * a long push (> 200msec), and
//     * a timeout (1000msec of no action).
//
// The program remembers 10 passwords in memory, each with a single alphabetic name.
// The **morse** character recognized is the name of the saved password.
// A timeout resets the recognizer.
//
// This version is designed to be configured by the python program "pwmorse"
// Modify /Applications/Teensyduino.app/Contents/Java/hardware/arduino/avr/boards.txt
// to include this line "leonardo.build.usb_product="PWMorse" before uploading.  This
// is so the enumeration of USB devices will see "PWMorse".
//================================================================

#include <Arduino.h>
#include <EEPROM.h>
#include <Keyboard.h>
#include <avr/wdt.h>                    // for the soft-boot code
#include <ctype.h> 

#define DEBUG                 0         // 0 - production, 1 - debugging
#define INIT_EEPROM           0         // if > 0 reinitialize EEPrOm

#define PRODUCT_NAME          "PWMorse"
#define VERSION               "1.6"

// state of the device - LOCKED or UNLOCKED
#define STATE_LOCKED          0
#define STATE_UNLOCKED        1
int device_state = STATE_LOCKED;

// which pin the button is attached to
#define ButtonPin             0         // Iota D2 pin

// define the debounce counter
#define DebounceCount         20        // number of millis/samples debounce delay

//definitions of times for long click and timeout (milliseconds)
#define LongClickTime         200
#define TimeoutPeriod         1000

// Finite State Machine events
#define EVENT_TIMEOUT         0         // no activity for a period
#define EVENT_SHORTPUSH       1         // a short push
#define EVENT_LONGPUSH        2         // a longer push

// number of events in the morse buffer
#define MORSE_BUFF_SIZE       10

// macro to turn off warnings about "unused parameter" in functions
#define UNUSED(v) (void) (v)

// OK and error message
const char *OKMsg = "OK";
const char *ErrorMsg = "ERROR";


// the password storage stuff
#define MaxPasswordLen    64      // max length of passwords
#define NumPasswords      10      // number of saved passwords

typedef struct { char password[MaxPasswordLen+1];
                 char ch;
               } password;

// variables restored from and saved to EEPRON
unsigned long Checksum = 0;       // place to save EEPROM data CRC
password Passwords[NumPasswords]; // all the passwords
char LockPassword;                // password letter, 0 if no password

#define EepromPasswordSize  (sizeof(Passwords))

// define start addresses in EEPROM of various fields
#define EepromChecksum      (0)
#define EepromPasswords     (EepromChecksum + sizeof(Checksum))
#define EepromLockPassword  (EepromPasswords + sizeof(Passwords))

// buffer, etc, to gather external command strings
#define MAX_COMMAND_LEN     40
#define COMMAND_END_CHAR    ';'
char CommandBuffer[MAX_COMMAND_LEN + 1];
int CommandIndex = 0;

// buffer to build dynamic messahes in
char TmpBuffer[128];

// define the Morse Node struct
typedef struct MorseNode { struct MorseNode *dot;
                           struct MorseNode *dash;
                           char ch;
                         } MorseNode;

// and define the morse "decode" tree just for alphabetics
MorseNode m_h = {NULL, NULL, 'H'};
MorseNode m_v = {NULL, NULL, 'V'};
MorseNode m_s = {&m_h, &m_v, 'S'};
MorseNode m_f = {NULL, NULL, 'F'};
MorseNode m_u = {&m_f, NULL, 'U'};
MorseNode m_i = {&m_s, &m_u, 'I'};

MorseNode m_l = {NULL, NULL, 'L'};
MorseNode m_r = {&m_l, NULL, 'R'};
MorseNode m_p = {NULL, NULL, 'P'};
MorseNode m_j = {NULL, NULL, 'J'};
MorseNode m_w = {&m_p, &m_j, 'W'};
MorseNode m_a = {&m_r, &m_w, 'A'};

MorseNode m_e = {&m_i, &m_a, 'E'};

MorseNode m_b = {NULL, NULL, 'B'};
MorseNode m_x = {NULL, NULL, 'X'};
MorseNode m_d = {&m_b, &m_x, 'D'};
MorseNode m_c = {NULL, NULL, 'C'};
MorseNode m_y = {NULL, NULL, 'Y'};
MorseNode m_k = {&m_c, &m_y, 'K'};
MorseNode m_n = {&m_d, &m_k, 'N'};

MorseNode m_z = {NULL, NULL, 'Z'};
MorseNode m_q = {NULL, NULL, 'Q'};
MorseNode m_g = {&m_z, &m_q, 'G'};
MorseNode m_o = {NULL, NULL, 'O'};
MorseNode m_m = {&m_g, &m_o, 'M'};

MorseNode m_t = {&m_n, &m_m, 'T'};

MorseNode morse_head = {&m_e, &m_t, (char) 0};

//##############################################################################
// Utility routines.
//##############################################################################

//----------------------------------------
// Calculate the CRC of the saved passwords in EEPROM
//----------------------------------------

unsigned long eeprom_crc(void)
{

  const unsigned long crc_table[16] = {0x00000000, 0x1db71064, 0x3b6e20c8, 0x26d930ac,
                                       0x76dc4190, 0x6b6b51f4, 0x4db26158, 0x5005713c,
                                       0xedb88320, 0xf00f9344, 0xd6d6a3e8, 0xcb61b38c,
                                       0x9b64c2b0, 0x86d3d2d4, 0xa00ae278, 0xbdbdf21c
                                      };
  unsigned long crc = ~0L;

  for (unsigned long index = EepromPasswords ; index < EepromPasswordSize; ++index)
  {
    crc = crc_table[(crc ^ EEPROM[index]) & 0x0f] ^ (crc >> 4);
    crc = crc_table[(crc ^ (EEPROM[index] >> 4)) & 0x0f] ^ (crc >> 4);
    crc = ~crc;
  }

  return crc;
}

//----------------------------------------
// Saves the in-memory password data to EEPROM and refreshes the CRC.
//----------------------------------------

void save_eeprom_data(void)
{
  // save in-memory passwords to EEPROM
  EEPROM.put(EepromPasswords, Passwords);
  EEPROM.put(EepromLockPassword, LockPassword);

  // calculate CRC of the data
  Checksum = eeprom_crc();
  EEPROM.put(EepromChecksum, Checksum);
}

//----------------------------------------
// Gets the password data in EEPROM and refreshes the RAM copies.
//
// If the EEPROM checksum is invalid, initializes EEPROM and sets
// in-memory copy to initialized values.
//----------------------------------------

void restore_eeprom_data(void)
{
  // get data checksun
  EEPROM.get(EepromChecksum, Checksum);

  // get actual CRC of data and ensure same
  if (Checksum != eeprom_crc() || INIT_EEPROM != 0)
  {
#if DEBUG
    Serial.println("Initializing EEPROM");
#endif

    // initialize passwords to EMPTY
    for (int pass_num = 0; pass_num < NumPasswords; ++pass_num)
    {
      password *pwd = &Passwords[pass_num];

      pwd->ch = (char) 0;
      for (int i=0; i < MaxPasswordLen; ++i)
        pwd->password[i] = (char) 0;
      strcpy(pwd->password, "EMPTY");
    }

    // turn off password lock
    LockPassword = 0;

    // and save initialized data
    save_eeprom_data();
  }

  // get data stored in EEPROM
  EEPROM.get(EepromChecksum, Checksum);
  EEPROM.get(EepromPasswords, Passwords);
  EEPROM.get(EepromLockPassword, LockPassword);
}

//----------------------------------------
// Code to reboot an AtMega32U4.
//
// From:
// https://blog.fsck.com/2014/08/how-to-reboot-an-arduino-leonardo-micro-into-the-bootloader.html
//----------------------------------------

void reboot(void)
{
  // [...]
  // Set the magic bits to get a Caterina-based device
  // to reboot into the bootloader and stay there, rather
  // than run move onward
  //
  // These values are the same as those defined in
  // Caterina.c

  uint16_t bootKey = 0x7777;
  uint16_t *const bootKeyPtr = (uint16_t *) 0x0800;

  // Stash the magic key
  *bootKeyPtr = bootKey;

  // Set a watchdog timer
  wdt_enable(WDTO_120MS);

  while (1) {} // This infinite loop ensures nothing else
  // happens before the watchdog reboots us
}

//----------------------------------------
// Convert a string to uppercase, in situ.
//----------------------------------------

void str2upper(char *str)
{
  while (*str)
  {
    *str = toupper(*str);
    ++str;
  }
}

//##############################################################################
// The recognizer routines and password processor.
//##############################################################################

//----------------------------------------
// Decode an event number to a string.
//----------------------------------------

char * decode_event(int event)
{
  switch (event)
  {
    case EVENT_TIMEOUT:   return (char *) "TIMEOUT";
    case EVENT_SHORTPUSH: return (char *) "SHORTPUSH";
    case EVENT_LONGPUSH:  return (char *) "LONGPUSH";
  }

  sprintf(TmpBuffer, "Unknown event number %d", event);
  return TmpBuffer;
}

//----------------------------------------
// Accept an event and perform the appropriate action.
//----------------------------------------

void send_password(int morse[], int morse_count)
{
  MorseNode *m_walk = &morse_head;

  // walk the morse decode tree until "bad code" or end of morse data
  for (int i=0; i < morse_count; ++i)
  {
    if (morse[i] == EVENT_SHORTPUSH)
    {
      if (!m_walk->dot)
        return;     // error, unrecognized morse code

      m_walk = m_walk->dot;
    }
    else
    {
      if (!m_walk->dash)
        return;     // error, unrecognized morse code
      
      m_walk = m_walk->dash;
    }
  }

  // find password slot for that character
  password *next = NULL;
  password *found = find_password(m_walk->ch, &next);

  if (found)
    Keyboard.print(found->password);
}


//----------------------------------------
// Accept an event and perform the appropriate action.
//----------------------------------------

void fsm(int event)
{
  static int morse[MORSE_BUFF_SIZE]; // the "morse" events
  static int morse_count = 0;        // index into "morse[]" of next event slot
  static bool fsm_abort = false;     // "true" if ignoring everything

  // if aborted or buffer overrun, do nothing
  if (fsm_abort || morse_count >= MORSE_BUFF_SIZE)
  {
    fsm_abort = true;
    return;
  }
  
  // decide what to do, either save morse char or send password
  switch (event)
  {
    case EVENT_SHORTPUSH:
    case EVENT_LONGPUSH:
      morse[morse_count++] = event;
      break;
    case EVENT_TIMEOUT:
      if (!fsm_abort && morse_count > 0)
        send_password(morse, morse_count);
      morse_count = 0;
      fsm_abort = false;
      break;
    default:
      Serial.print("Bad event=");
      Serial.println(event);
      break;
  }
}

//----------------------------------------
// Handle a button state change.
//
// Decide if we have a SINGLE CLICK, LONG CLICK or TIMEOUT
// or a TIMEOUT event.
// Call the appropriate handler once we have an event.
//----------------------------------------

void change_button_state(int state)
{
  // state variables to determine if single/long click
  static unsigned long start_click = 0L;    // time of start of click
  unsigned long now = millis();

  if (state)
  {
    // button UP
    if ((now - start_click) <= LongClickTime)
    {
      // a short click
      fsm(EVENT_SHORTPUSH);
      start_click = 0L;
    }
    else
    {
      // a long click
      fsm(EVENT_LONGPUSH);
    }
  }
  else
  {
    // button DOWN
    start_click = now;
  }
}

//----------------------------------------
// Function to handle polling the button - includes debounce.
//
// Passes new (debounced) state to change_button_state() function.
//
// Code here from: https://playground.arduino.cc/Learning/SoftwareDebounce/
//----------------------------------------

unsigned long next_timeout = 0;  // time TIMEOUT event will be raised

void poll_button(void)
{
  // static variables to handle software debounce
  static int current_state = HIGH;        // the debounced input value
  static int counter = 0;                 // how many times we have seen new value
  static unsigned long poll_time = 0;     // the last time the output pin was sampled

  unsigned long now = millis();           // the current time

  if (now > poll_time)
  {
    poll_time = now + 1;                  // so we don't poll again until second millisecond

    // if button now UNPRESSED and long period elapsed, raise TIMEOUT
    if ((current_state == HIGH) && (next_timeout < now))
    {
      fsm(EVENT_TIMEOUT);
      next_timeout = now + TimeoutPeriod;
    }

    int reading = digitalRead(ButtonPin); // current pin state

    if (reading == current_state && counter > 0)
    {
      counter--;
    }
    if (reading != current_state)
    {
      counter++;
    }

    // If the pin has shown the same value for long enough let's switch it
    if (counter >= DebounceCount)
    {
      counter = 0;
      current_state = reading;
      change_button_state(reading);
      next_timeout = now + TimeoutPeriod;   // reset timeout counter
    }
  }
}

//----------------------------------------
// Find a password for a given character.
//     pass_ch  the character to find
//     next     place to return "first empty" pointer
//              (don't update if 0)
// Returns the address of the slot matching "pass_ch", else NULL.
//----------------------------------------

password *find_password(char pass_ch, password **next)
{
  // set return "next" to NULL (if "next" specified)
  if (*next)
  {
    *next = NULL;
  }

  // look for matching slot
  for (int i=0; i < NumPasswords; ++i)
  {
    password *p = &Passwords[i];

    if (p->ch == (char) 0)
    {
      // empty slot, save in next_p if first
      if (!*next)
        *next = p;
    }
    else if (p->ch == pass_ch)
    {
      // found it
      return p;
    }
  }

  return NULL;
}

//##############################################################################
// Code for the "external" commands from the serial port.
//##############################################################################

//----------------------------------------
// Get help:
//     H;
//----------------------------------------

void xcmd_help(char *cmd)
{
  UNUSED(cmd);

  Serial.println("-----------Interactive Commands-----------------");
  Serial.println("H;       send help text to console");
  Serial.println("ID;      get device identifier string");
  Serial.println("CLEAN;   destroy all saved passwords");
  Serial.println("Da;      delete password 'a'");
  Serial.println("P;       print all saved passwords");
  Serial.println("Pa;      print password 'a'");
  Serial.println("Paxxx;   set password 'a' to 'xxx'");
  Serial.println("LOCKa;   lock the device with password 'a'");
  Serial.println("R;       reboot the device");
  Serial.println("------------------------------------------------");
}

//----------------------------------------
// Get the identifier string:
//     ID
//----------------------------------------

void xcmd_id(char *cmd)
{
  // if not legal, complain
  if (strcmp(cmd, "ID"))
  {
    Serial.println(ErrorMsg);
    return;
  }

  // generate ID string and return
  Serial.print(PRODUCT_NAME);
  Serial.print(" ");
  Serial.println(VERSION);
}

//----------------------------------------
// Destroy any saved password information
//     CLEAN
//----------------------------------------

void xcmd_clean(char *cmd)
{
  // if not legal, complain
  if (strcmp(cmd, "CLEAN"))
  {
    Serial.println(ErrorMsg);
    return;
  }

  // ensure in-memory buffers clean and ALL EEPROM bytes wiped
  for (int pass_num = 0; pass_num < NumPasswords; ++pass_num)
  {
    Passwords[pass_num].ch = (char) 0;
    
    for (int i = 0; i < MaxPasswordLen+1; ++i)
    {
      Passwords[pass_num].password[i] = ' ';
    }
  }

  LockPassword = 0;   // set LOCK off
  
  save_eeprom_data();

  Serial.println(OKMsg);
}

//----------------------------------------
// Destroy any saved password information
//     Da
//----------------------------------------

void xcmd_del(char *cmd)
{
  // if wrong length, ERROR!
  if (strlen(cmd) != 2)
  {
    Serial.println(ErrorMsg);
    return;
  }

  // get password char
  char pass_ch = cmd[1];

  // find password slot or first empty slot
  password *next = NULL;
  password *found = find_password(pass_ch, &next);

  if (!found)
  {
    Serial.println(ErrorMsg);
    return;
  }

  // destroy data in password slot
  found->ch = (char) 0;
  for (int i=0; i < MaxPasswordLen+1; ++i)
  {
    found->password[i] = ' ';
  }

  // unlock device if password was just deleted
  if (LockPassword == pass_ch)
  {
    LockPassword = 0;
    device_state = STATE_UNLOCKED;
  }
  
  save_eeprom_data();

  Serial.println(OKMsg);
}

//----------------------------------------
// Set/Get password string
//     P       display all saved passwords
//     Pa      display password 'a'
//     Paxxxx  set password 'a'
//
// Because we might have a password here, the 'cmd' string has
// not been converted to uppercase.
//----------------------------------------

void xcmd_password(char *cmd)
{
  // if just "P" print all saved passwords
  if (strlen(cmd) == 1)
  {
    Serial.println("Passwords:");
    
    // display all saved passwords
    for (int pass_num = 0; pass_num < NumPasswords; ++pass_num)
    {
      if (Passwords[pass_num].ch != 0)
      {
        Serial.print(Passwords[pass_num].ch);
        Serial.print(" ");
        Serial.println(Passwords[pass_num].password);
      }
    }

    Serial.println("");
    
    return;
  }

  // get UPPERCASE password char
  char pass_ch = toupper(cmd[1]);

  // are we getting a password?
  if (strlen(cmd) == 2)
  {
    // find password slot or first empty slot
    password *next = NULL;
    password *found = find_password(pass_ch, &next);

    if (found)
    {
      Serial.print(pass_ch);
      Serial.print(" ");
      Serial.println(found->password);
    }
    else
      Serial.println(ErrorMsg);

    return;
  }
  else
  {
    // setting a password, get the new password
    char *new_password = &cmd[2];

    // find password slot or first empty slot
    password *next = NULL;
    password *found = find_password(pass_ch, &next);

    // see if we found a slot to use
    if (!found)
      found = next;
      
    if (found)
    {
      // there is an empty slot, save new password
      if (strlen(new_password) >= MaxPasswordLen)
      {
        Serial.println(ErrorMsg);
        return;
      }

      found->ch = pass_ch;
      strcpy(found->password, new_password);
    }
    else
    {
      // no more free slots for passwords
      Serial.println(ErrorMsg);
      return;
    }
    
    save_eeprom_data();
  }

  Serial.println(OKMsg);
}

//----------------------------------------
// Lock the device:
//     LOCKa  set lock password to slot for 'a'
//
// If 'a' is actually '0' remove any lock.
//----------------------------------------

void xcmd_lock(char *cmd)
{
  // valid command?
  if ((strncmp(cmd, "LOCK", 4) != 0) || (strlen(cmd) != 5))    // not "LOCKa"
  {
    Serial.println(ErrorMsg);
    return;
  }

  // get desired password slot
  char pass_ch = toupper(cmd[4]);

  // if password char is '0' remove any lock
  if (pass_ch == '0')
  {
    LockPassword = 0;
    device_state = STATE_UNLOCKED;
  }
  else
  {
    // set lock to password, so find password slot
    password *next = NULL;
    password *found = find_password(pass_ch, &next);
    
    // see if we found a slot to use
    if (found == 0)
    {
      // no slot with that name
      Serial.println(ErrorMsg);
      return;
    }
  
    // OK, lock the device
    LockPassword = pass_ch;
    device_state = STATE_LOCKED;
  }
  
  save_eeprom_data();   // save EEPROM data

  Serial.println(OKMsg);
}

//----------------------------------------
// Perform a hard reboot:
//     R
//
// Reboot the device.  No need to "push the button".
//----------------------------------------

void xcmd_reboot(char *cmd)
{
  // if not legal, complain
  if (strcmp(cmd, "R"))
  {
    Serial.println(ErrorMsg);
    return;
  }

  // OK, reboot this thing (AtMega32U4 bootloader)
  Serial.println("Rebooting...");
  reboot();
}

//----------------------------------------
// Process an external command.
//     cmd     address of command string buffer
//
// 'cmd' is '\0' terminated.
//----------------------------------------

void do_extern_cmd(char *cmd)
{
  char orig_cmd[strlen(cmd) + 1];
  strcpy(orig_cmd, cmd);

  // ensure everything is uppercase
  str2upper(cmd);

  // process the command
  switch (cmd[0])
  {
    case 'H':
      xcmd_help(cmd);
      return;
    case 'I':
      xcmd_id(cmd);
      return;
    case 'C':
      xcmd_clean(cmd);
      return;
    case 'D':
      xcmd_del(cmd);
      return;
    case 'L':
      xcmd_lock(cmd);
      return;
    case 'P':
      // must pass original command - don't uppercase password!
      xcmd_password(orig_cmd);
      return;
    case 'R':
      xcmd_reboot(cmd);
      return;
  }

  Serial.println(ErrorMsg);
}

//----------------------------------------
// Read password into supplied buffer until newline.
//----------------------------------------
void read_password(char *buffer)
{
  *buffer = '\0';
  
  while (Serial.available())
  {
    char ch = Serial.read();
    if (ch == '\n')
      return;
    *buffer = ch;
    ++buffer;
    *(buffer+1) = '\0';
  }
}

//----------------------------------------
// Handle characters coming in on the Serial port.
//
// Call the command handler when a complete command is received.
//----------------------------------------

void do_external_commands(void)
{
  bool overflow = false;    // "true" if command buffer overflowed

  // gather any commands from the external controller
  while (Serial.available())
  {
    char ch = Serial.read();

    // ignore newlines
    if (ch == '\n')
      continue;

    if (CommandIndex < MAX_COMMAND_LEN)
    {
      // only add if command not over-length
      CommandBuffer[CommandIndex++] = ch;
    }
    else
      overflow = true;

    if (ch == COMMAND_END_CHAR)   // if end of command, execute it
    {
      if (overflow)
      {
        overflow = false;
        Serial.println("Command too long!");
      }
      else
      {
        CommandBuffer[CommandIndex - 1] = '\0'; // remove final ';'
        do_extern_cmd(CommandBuffer);
        CommandIndex = 0;
      }
    }
  }
}

//----------------------------------------
// setup()
//
// Prepare pin mode and set current button state.
//----------------------------------------

void setup(void)
{
  Serial.begin(115200);
  
  // start Serial and wait for AtMega32U4 chip, Leonardo, etc.
  while (!Serial)
    ;

  // set the "timeout" counter
  next_timeout = millis() + TimeoutPeriod;

  // set pin mode for the button
  pinMode(ButtonPin, INPUT_PULLUP);

  // prepare keyboard HID
  Keyboard.begin();

  // get data from EEPROM
  restore_eeprom_data();

  // if no password set, start in the UNLOCKED state
  if (LockPassword == 0)
  {
    device_state = STATE_UNLOCKED;
  }

#if DEBUG
  // announce the start
  Serial.print(PRODUCT_NAME);
  Serial.print(" ");
  Serial.println(VERSION);
#endif
}

//----------------------------------------
// The loop() function.
//
// The device is either in the LOCKED mode, where it will only accept a
// password, or it's UNLOCKED and will accept any firmware command.
//----------------------------------------

void loop(void)
{
  // if LOCKED wait for password
  if (device_state == STATE_LOCKED)
  {
    read_password(TmpBuffer);

    password *next = NULL;                                // unused
    password *found = find_password(LockPassword, &next); // password slot
    
    if (strcmp(TmpBuffer, found->password) == 0)
    {
      device_state = STATE_UNLOCKED;
      Serial.println(OKMsg);
    }
  }
  else  // handle external commands
  {
    // handle any commands from the serial port
    do_external_commands();
  }

  // always check the button, do something if required
  poll_button();
}
