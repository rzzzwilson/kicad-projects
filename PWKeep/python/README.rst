pwkeep.py
=========

A CLI program to configure the PWKeep device running PWMorse
firmware, v1.6 or later

Needs these installs on MacOS:

    brew install libusb-compat
    python3 -m pip install pyusb

Usage
-----

Start the program and then plug in the PWMorse firmware device.  The device
will be auto recognized and the user will be prompted to unlock the device
with the password.  After that any command recognized by the firmware may
be entered.
