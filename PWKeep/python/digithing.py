"""
A class to interface to a Digi* device such as a DigiVolt, DigiPot, etc.
In this case, a PWMorse device.
"""

import os
import sys
import time
import serial
import usb
from serial.tools.list_ports import comports
import logger
log = logger.Log('digithing.log', logger.Log.DEBUG)


###############################################################################
# A class to interface with a DigiThing device.
#
#   # create object, optional baudrate, ignore named devices
#   dv = DigiThing(id_string=None, ignore=None, baudrate=115200)
#   # execute command, return results
#   response = dv.do_cmd('ID;')
#   # close device
#   dv.close()
###############################################################################

class DigiThingError(Exception):
    """Exception raised if no DigiThing found."""

    pass

class DigiThing:

    # USB/FTDI board identifiers for an AtMega32U4 DigiThing
    VendorID = 0x2341
    ProductID = 0x8036

    def __init__(self, id_string=None, ignore=None, baudrate=115200):
        """Connect to a single DigiThing device.

        id_string  identifying string of the DigiThing to be used
                   (if None, don't do ID check)
        ignore     list of devices to ignore
        baudrate   speed of serial communication
        """

        # prepare internal state
        self.ser = None
        self.baudrate = baudrate
        self.id_string = id_string
        self.ignore = []
        if ignore:
            self.ignore = ignore
            log.debug(f'.ignore={self.ignore}')

        # get *all* DigiThing devices on the USB bus
        result = []
        for usb_dev in serial.tools.list_ports.comports():
            log(f'Found device: {usb_dev}')
            if (usb_dev.vid == DigiThing.VendorID
                    and usb_dev.pid == DigiThing.ProductID):
                if usb_dev.device not in self.ignore:
                    log(f'Saving DigiThing: {usb_dev}')
                    result.append(usb_dev)

        log.debug(f'found devices:\n{result}')

        if len(result) < 1:
            raise DigiThingError('No DigiThing devices found.')

        # keep only the devices that identify as the desired "id_string"
        things = []
        for usb in result:
            # open a serial connection to the possible DigiThing
            self.ser = serial.Serial(port=usb.device, baudrate=baudrate)
            self.ser.timeout = 0.05                     # nice short timeout

            # check we actually have a real DigiThing
            self._flush_input()                         # flush any pending output
            if self.id_string:                          # if required,
                id_string = self.do_cmd('id;')          #     ask for and check the ID string
                if id_string.startswith(self.id_string):
                    things.append(usb)
            else:
                things.append(usb)
            self.ser.close()
            self.ser = None

        if len(things) < 1:
            raise DigiThingError(f'No DigiThing devices with id_string={self.id_string} found.')

        log.debug(f'found devices:\n{things}')

        # make sure we have only one
        if len(things) > 1:
            raise DigiThingError(f'More than one DigiThing device with id_string={self.id_string}found.')

        self.device = things[0]                     # save possible DigiThing

        # open a serial connection to the possible DigiThing
        self.port = self.device.device
        self.ser = serial.Serial(port=self.port, baudrate=baudrate)
        self.ser.timeout = 0.05                     # nice short timeout

        log.debug(f'Using DigiThing({self.id_string}) on port {self.port}')

    def _readchar(self):
        """Read one character from the serial device.

        Retry on SerialException.
        Returns an empty string if no character read.
        """

        for _ in range(10):
            try:
                return str(bytes(self.ser.read()), encoding='utf-8')
            except (serial.serialutil.SerialException, OSError):
                log.debug(f'Retrying because of exception.')
                time.sleep(0.5)

        raise DigiThingError('Digivolt device disconnected') from None

    def _read_line(self):
        """Read one line from the 'ser' connection."""
    
        line = ''
        count = 0
        while count < 3:
            char = self._readchar()
            if char:
                if char == '\n':
                    log.debug(f"_read_line: returning '{line}'")
                    return line
                elif char != '\r':
                    line += char
                    count = 0
            else:
                count += 1
    
        return ''

    def _flush_input(self):
        """Throwaway all input from the device until a timeout."""

        count = 0
        while count < 3:
            line = self._read_line()
            if line:
                count = 0
            else:
                count += 1

    def do_cmd(self, cmd):
        """Send command to DigiThing, return response."""

        log.debug(f"do_cmd: cmd='{cmd}'")

        if cmd[-1] != '\n':
            cmd += '\n'
        try:
            self.ser.write(bytes(cmd, encoding='utf-8'))
        except (serial.serialutil.SerialException, OSError):
            raise DigiThingError('Digivolt device disconnected') from None

        # read response
        response = []
        count = 0
        while count < 5:
            line = self._read_line()
            if line:
                line = line.strip()
                log.debug(f"do_cmd: line='{line}'")
                response.append(line)
                count = 0
            else:
                count += 1

        response = '\n'.join(response)
        log.debug(f"do_cmd: return response='{response}'")
        return response

    def close(self):
        """Close the DigiThing."""

        if self.ser:
            self.ser.close()

    def __del__(self):
        """Delete the DigiThing device."""

        self.close()

    def __str__(self):
        return f'{self.id_string}'

if __name__ == '__main__':
    import traceback

    # capture any not-handled exceptions
    def excepthook(type, value, tback):
        msg = '\n' + '=' * 80
        msg += '\nUncaught exception:\n'
        msg += ''.join(traceback.format_exception(type, value, tback))
        msg += '=' * 80 + '\n'
        log(msg)
        print(msg)

    # plug our handler into the python system
    sys.excepthook = excepthook

    # look for a single DigiPot device
    used_ports = []

    print('******************** Looking for DigiPot ********************')
    try:
        dp = DigiThing('DigiPot', ignore=used_ports)
    except DigiThingError:
        print('No DigiPot devices found')
    else:
        for attr in dir(dp.device):
            if not attr.startswith('_'):
                value = getattr(dp.device, attr)
                if 'method' in str(value):
                    print(f'\t.{attr}()={value()}')
                else:
                    print(f'\t.{attr}={value}')
        print(f'Found: {dp}')
        print(f"dp.do_cmd('id;') -> '{dp.do_cmd('id;').strip()}'")

        used_ports.append(dp.port)

    print('******************** Looking for DigiPotLDR ********************')
    try:
        dpl = DigiThing('DigiPotLDR', ignore=used_ports)
    except DigiThingError:
        print('No DigiPotLDR devices found')
    else:
        for attr in dir(dpl.device):
            if not attr.startswith('_'):
                value = getattr(dpl.device, attr)
                if 'method' in str(value):
                    print(f'\t.{attr}()={value()}')
                else:
                    print(f'\t.{attr}={value}')
        print(f'Found: {dpl}')
        print(f"dpl.do_cmd('id;') -> '{dpl.do_cmd('id;').strip()}'")

        used_ports.append(dpl.port)

    # now look for a single DigiVolt device
    print('******************** Looking for DigiVolt ********************')
    try:
        dv = DigiThing('DigiVolt', ignore=used_ports)
    except DigiThingError:
        print('No DigiVolt devices found')
    else:
        for attr in dir(dv.device):
            if not attr.startswith('_'):
                value = getattr(dv.device, attr)
                if 'method' in str(value):
                    print(f'\t.{attr}()={value()}')
                else:
                    print(f'\t.{attr}={value}')
        print(f'Found: {dv}')
        print(f"dv.do_cmd('id;') -> '{dv.do_cmd('id;').strip()}'")

        used_ports.append(dv.port)
