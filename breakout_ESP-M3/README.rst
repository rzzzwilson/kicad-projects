Breakout Board for ESP-M3
=========================

This is a simple breakout board for the ESP-M3 module.
Includes a 5 volt power input and a 5v -> 3.3 volt regulator.
