The hardware here is used to program an ESP8266-12E/F module once it is placed
on a PCB.  The programmer contains a USB UART that connects to the Arduino IDE.
The programmer provides the required resistors and buttons.

To program the ESP8266 connect the programmer to your PC and also connect the
6 pins of the programmer to an ICSP connection you built into the PCB.  I
use a 2mm spaced linear set of pads on the PCB and a set of 6 linear pogo
pins to connect temporarily to the PCB.  You could use the more traditional
2x3 pin headers.

When you tell the IDE to compile and download your code press the Flash and
Reset buttons together, release the Reset and then release the Flash button.
This will place the ESP8266 in program mode and the download will succeed.

I don't use the Vcc connection of the ICSP to power the board to program
because the USB UARTs I use have selectable 3.3/5 settings and I don't
want to accidently apply 5 volts to a 3.3 volt component!  That means I
must power the board to program by alternate means, usually from USB.

# Modifications

Move the UART board inward 1.0mm.
